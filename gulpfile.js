var Argv = require('yargs').argv;
var gulp = require('gulp');
var Plugins = require('gulp-load-plugins')();
var clean = require('gulp-clean');

/* ===========================================================
CONTEXT DECLARATION
=========================================================== */
var Paths = {
    assets: 'assets',
    src: 'src',
    debugger: 'src/debugger',
    themes: 'output/themes',
    srcThemes: 'src/themes',
    styles: 'src/styles',
    wall: 'output/themes/wall',
    srcWall: 'src/themes/wall',
    wallgroup: 'output/themes/wallgroup',
    srcWallgroup: 'src/themes/wallgroup',
    admintpl: 'assets/admintpl',
    tpl: 'assets/tpl',
    css: 'assets/css',
    js: 'assets/js',
    javascript: 'assets/javascript',
    labels: 'assets/labels',
    themeOptions: 'assets/theme-options',
    customButtons: 'assets/custom-buttons',
    styleGuides: 'assets/guides/styles',
    themeGuides: 'assets/guides/themes',
    themeThumbs: 'assets/thumbnails'
};
var Environment = Argv.env || 'prod';
Plugins.util.log('*** EXECUTING ON ' + Environment + ' ENVIRONMENT ***');

/* ===========================================================
GULP CONFIGURATION
=========================================================== */

// IMPORT FILES
gulp.task('file-include', require('./build/compile/include_files.js')(gulp, Plugins, Paths, Argv, Environment));

//CLEAN IMPORT TEMPORARY FOLDER
gulp.task('file-include-cleanup', () => gulp.src('./output', { read: false }).pipe(clean()));

// COMPILE WALLS
gulp.task('compile-walls', require('./build/compile/wall.js')(gulp, Plugins, Paths, Argv, Environment));

// COMPILE WALLGROUPS
gulp.task('compile-wallgroups', require('./build/compile/wallgroup.js')(gulp, Plugins, Paths, Argv, Environment));

// THUMBS
gulp.task('thumbs', require('./build/compile/thumbs.js')(gulp, Plugins, Paths, Argv, Environment));

// COMPILE STYLES
gulp.task('compile-styles', require('./build/compile/style.js')(gulp, Plugins, Paths, Argv, Environment));

// CONFIGURE ENVIRONMENT
gulp.task('configure-environment', require('./build/post-compile/environment.js')(gulp, Plugins, Paths, Argv, Environment));

// CONFIGURE LIBRARIES
gulp.task('configure-libraries', require('./build/post-compile/library.js')(gulp, Plugins, Paths, Argv, Environment));

// VALIDATE JS
gulp.task('validate-js', require('./build/post-compile/validate.js')(gulp, Plugins, Paths, Argv, Environment));

// FORMAT ARTIFACTS
gulp.task('format-artifacts', require('./build/post-compile/format.js')(gulp, Plugins, Paths, Argv, Environment));

// COMPILE PROCESS
gulp.task('compile', function() {
    let steps = ['compile-walls', 'compile-wallgroups', 'compile-styles'];
    if (Argv.hasOwnProperty('wall')) {
        steps = ['compile-walls'];
    } else if (Argv.hasOwnProperty('wallgroup')) {
        steps = ['compile-wallgroups'];
    } else if (Argv.hasOwnProperty('style')) {
        steps = ['compile-styles'];
    }

    return gulp.series('file-include', ...steps, 'configure-environment', 'format-artifacts');
}());

// PACKAGING PROCESS
gulp.task('post-compile', gulp.series('validate-js', 'format-artifacts'));

// PUBLISH TO S3
gulp.task('s3-publish', require('./build/deploy/publish.js')(gulp, Plugins, Paths, Argv, Environment));

// BUILD ALL
gulp.task('build', gulp.series('compile', 'post-compile', 'configure-libraries', 'file-include-cleanup'));

// DEPLOY
gulp.task('deploy', gulp.series('thumbs', 'build', 's3-publish'));

// DEPLOY
gulp.task('deploy-fast', gulp.series('build', 's3-publish'));

// WATCH
gulp.task('watch', require('./build/watch.js')(gulp, Plugins, Paths, Argv, Environment));

gulp.task('default', gulp.series('build', 'watch'));

gulp.task('quick', gulp.series('compile', 'configure-environment', 'validate-js', 'configure-libraries'));
