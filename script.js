// Override asset URL with local path, in order to test locally.
LSConfig.partnerAssetURL = '/assets';

// Since production script has different library names, set correct names locally
if (typeof LSHbr != 'undefined') Handlebars = LSHbr;
if (typeof LSpky != 'undefined') packery = LSpky;
if (typeof LSPky != 'undefined') Packery = LSPky;
if (typeof LSitp != 'undefined') isotope = LSitp;
if (typeof LSItp != 'undefined') Isotope = LSItp;
if (typeof LSmmt != 'undefined') moment = LSmmt;