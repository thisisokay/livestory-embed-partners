const fileinclude = require('gulp-file-include');

module.exports = function(gulp, plugins) {
  return function(done) {
    plugins.util.log('Parsing file includes');
    gulp.src(['src/**'])
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(gulp.dest('output/'))
      .on('end', done);
  };
};