var async = require('async');
var fs = require('fs');

module.exports = function(gulp, plugins, paths, params) {

    var _getAvailableStyles = function() {
        var styles = {};
        var types = fs.readdirSync(paths.styles)
            .filter(function(t) {
                return t !== 'common' && t !== '.DS_Store';
            });
        types.forEach(function(t) {
            var subtypes = fs.readdirSync(paths.styles + '/' + t)
                .filter(function(t) {
                    return t !== 'common' && t !== '.DS_Store';
                });
            styles[t] = subtypes;
        });

        console.log('STYLES');
        console.log(styles);

        return styles;
    };

    return function(done) {

        var targets;
        if (params.hasOwnProperty('style')) {
            var selectedStyle = params.style.split('-');
            var type = selectedStyle.shift();
            var name = selectedStyle.join('-');
            targets = {};
            targets[type] = [name];
        } else {
            targets = _getAvailableStyles();
        }

        var q = async.queue(function(task, callback) {
            if (task.input.indexOf('/buttons.json') > 0) {
                plugins.util.log('Running Custom Buttons for ' + task.name);
                gulp.src(task.input, { allowEmpty: true })
                    .pipe(plugins.rename(task.name))
                    .pipe(gulp.dest(paths.customButtons))
                    .on('end', callback);
            } else if (task.input.indexOf('/guide.md') > 0) {
                plugins.util.log('Running Guide for ' + task.name);
                gulp.src(task.input, { allowEmpty: true })
                    .pipe(plugins.rename(task.name))
                    .pipe(gulp.dest(paths.styleGuides))
                    .on('end', callback);
            } else {
                plugins.util.log('Building style ' + task.name);
                gulp.src(task.input, { allowEmpty: true })
                    .pipe(plugins.less())
                    .pipe(plugins.concat(task.name))
                    .pipe(gulp.dest(paths.css))
                    .on('end', callback);
            }
        });

        q.drain = done;

        var tasks = [];
        Object.keys(targets).forEach(function(type) {
            Array.prototype.push.apply(tasks, targets[type].map(function(item) {
                return {
                    input: paths.styles + '/' + type + '/' + item + '/main.less',
                    name: type + '-' + item + '.css'
                };
            }));
            Array.prototype.push.apply(tasks, targets[type].map(function(item) {
                return {
                    input: paths.styles + '/' + type + '/' + item + '/admin.less',
                    name: type + '-' + item + '-admin.css'
                };
            }));
            Array.prototype.push.apply(tasks, targets[type].map(function(item) {
                return {
                    input: paths.styles + '/' + type + '/' + item + '/buttons.json',
                    name: type + '-' + item + '.json'
                };
            }));
            Array.prototype.push.apply(tasks, targets[type].map(function(item) {
                return {
                    input: paths.styles + '/' + type + '/' + item + '/guide.md',
                    name: type + '-' + item + '.md'
                };
            }));
        });

        q.push(tasks);
    };
};
