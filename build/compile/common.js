var async = require('async');
const { exec } = require("child_process");

module.exports = function(gulp, plugins, paths) {
    return {
        handlebars: function(type, subtype, name, done) {

            async.parallel([
                function(cb) {
                    var inPath;
                    var outName;
                    var namespace;
                    if (subtype) {
                        inPath = paths.themes + '/' + type + '/' + subtype + '/' + name + '/handlebars/*.handlebars';
                        outName = type + '-' + subtype + '-' + name;
                        namespace = 'LSTemplates.' + type + '_' + subtype + '_' + name.replace('-', '_').replace('@', '_');
                    } else {
                        inPath = paths.themes + '/' + type + '/' + name + '/handlebars/*.handlebars';
                        outName = type + '-' + name;
                        namespace = 'LSTemplates.' + type + '_' + name.replace('-', '_').replace('@', '_');
                    }
                    var outFileName = outName + '.js';
                    plugins.util.log('Running Handlebars for ' + outName);
                    gulp.src(inPath, { allowEmpty: true })
                        .pipe(plugins.handlebars({
                            handlebars: require('handlebars')
                        }))
                        .pipe(plugins.wrap('Handlebars.template(<%= contents %>)'))
                        .pipe(plugins.declare({
                            namespace: namespace,
                            noRedeclare: true
                        }))
                        .pipe(plugins.concat(outFileName, {newLine: ';'}))
                        .pipe(gulp.dest(paths.tpl + '/'))
                        .on('end', cb);
                },
                function(cb) {
                    var inPath;
                    var outName;
                    var namespace;
                    if (subtype) {
                        inPath = paths.themes + '/' + type + '/' + subtype + '/' + name + '/handlebars/wall.handlebars';
                        outName = type + '-' + subtype + '-' + name;
                        namespace = 'LSTemplates.' + type + '_' + subtype + '_' + name.replace('-', '_').replace('@', '_');
                    } else {
                        inPath = paths.themes + '/' + type + '/' + name + '/handlebars/wall.handlebars';
                        outName = type + '-' + name;
                        namespace = 'LSTemplates.' + type + '_' + name.replace('-', '_').replace('@', '_');
                    }
                    var outFileName = outName + '.js';
                    plugins.util.log('Running Handlebars ADMIN for ' + outName);
                    gulp.src(inPath, { allowEmpty: true })
                        .pipe(plugins.handlebars({
                            handlebars: require('handlebars')
                        }))
                        .pipe(plugins.wrap('Handlebars.template(<%= contents %>)'))
                        .pipe(plugins.declare({
                            namespace: namespace,
                            noRedeclare: true
                        }))
                        .pipe(plugins.concat(outFileName, {newLine: ';'}))
                        .pipe(gulp.dest(paths.admintpl + '/'))
                        .on('end', cb); 
                }
            ], done)
        },
        less: function(type, subtype, name, done) {
            var inPath;
            var inFontPath;
            var outName;
            if (subtype) {
                inPath = [
                    //paths.themes + '/' + type + '/' + subtype + '/default/less/*.less',
                    paths.themes + '/' + type + '/' + subtype + '/' + name + '/less/*.less'
                ];
                inFontPath = [
                    //paths.themes + '/' + type + '/' + subtype + '/default/less/fonts.less',
                    paths.themes + '/' + type + '/' + subtype + '/' + name + '/less/fonts.less'
                ];
                outName = type + '-' + subtype + '-' + name;
            } else {
                inPath = [
                    //paths.themes + '/' + type + '/default/less/*.less',
                    paths.themes + '/' + type + '/' + name + '/less/*.less'
                ];
                inFontPath = [
                    //paths.themes + '/' + type + '/default/less/fonts.less',
                    paths.themes + '/' + type + '/' + name + '/less/fonts.less'
                ];
                outName = type + '-' + name;
            }
            inPath.push('!**/fonts.less');
            var outFileName = outName + '.css';
            var outFontFileName = outName + '-fonts.css';
            plugins.util.log('Running Less for ' + outName);
            async.parallel([
                function(cb) {
                    gulp.src(inPath, { allowEmpty: true })
                        .pipe(plugins.less())
                        .pipe(plugins.concat(outFileName))
                        .pipe(gulp.dest(paths.css))
                        .on('end', cb);
                },
                function(cb) {
                    gulp.src(inFontPath, { allowEmpty: true })
                        .pipe(plugins.less())
                        .pipe(plugins.concat(outFontFileName))
                        .pipe(gulp.dest(paths.css))
                        .on('end', cb);
                }
            ], done);
        },
        concat: function(type, subtype, name, done) {
            var inPath;
            var inPathAdmin;
            var outName;
            var brandCommon = name.split('-')[0];
            console.log('### brand', brandCommon);
            if (subtype) {
                inPath = [
                    paths.themes + '/common/' + brandCommon + '/*.js',
                    paths.themes + '/' + type + '/' + subtype + '/' + name + '/js/*.js'
                ];
                inPathAdmin = paths.themes + '/' + type + '/' + subtype + '/' + name + '/js/admin.js';
                outName = type + '-' + subtype + '-' + name;
            } else {
                inPath = [
                    paths.themes + '/common/' + brandCommon + '/*.js',
                    paths.themes + '/' + type + '/' + name + '/js/*.js'
                ];
                inPathAdmin = paths.themes + '/' + type + '/' + name + '/js/admin.js';
                outName = type + '-' + name;
            }
            var outFileName = outName + '.js';
            var outFileNameAdmin = outName + '-admin.js';
            inPath.push(paths.tpl + '/' + outFileName);
            inPath.push('!**/admin.js');
            plugins.util.log('Running Concat for ' + outName);
            async.parallel([
                function(cb) {
                    gulp.src(inPath, { allowEmpty: true })
                        .pipe(plugins.concat(outFileName, {newLine: ';'}))
                        .pipe(gulp.dest(paths.js))
                        .on('end', cb);
                },
                function(cb) {
                    gulp.src(inPathAdmin, { allowEmpty: true })
                        .pipe(plugins.concat(outFileNameAdmin, {newLine: ';'}))
                        .pipe(gulp.dest(paths.js))
                        .on('end', done);
                }
            ], done);
        },
        labels: function(type, subtype, name, done) {
            var inPath;
            var outName;
            if (subtype) {
                inPath = paths.themes + '/' + type + '/' + subtype + '/' + name + '/labels.json';
                outName = type + '-' + subtype + '-' + name;
            } else {
                inPath = paths.themes + '/' + type + '/' + name + '/labels.json';
                outName = type + '-' + name;
            }
            var outFileName = outName + '.json';
            plugins.util.log('Running Labels for ' + outName);
            gulp.src(inPath, { allowEmpty: true })
                .pipe(plugins.rename(outFileName))
                .pipe(gulp.dest(paths.labels))
                .on('end', done);
        },
        themeOptions: function(type, subtype, name, done) {
            var inPath;
            var outName;
            if (subtype) {
                inPath = paths.themes + '/' + type + '/' + subtype + '/' + name + '/options.json';
                outName = type + '-' + subtype + '-' + name;
            } else {
                inPath = paths.themes + '/' + type + '/' + name + '/options.json';
                outName = type + '-' + name;
            }
            var outFileName = outName + '.json';
            plugins.util.log('Running Theme Options for ' + outName);
            gulp.src(inPath, { allowEmpty: true })
                .pipe(plugins.rename(outFileName))
                .pipe(gulp.dest(paths.themeOptions))
                .on('end', done);
        },
        guides: function(type, subtype, name, done) {
            var inPath;
            var outName;
            if (subtype) {
                inPath = paths.themes + '/' + type + '/' + subtype + '/' + name + '/guide.md';
                outName = type + '-' + subtype + '-' + name;
            } else {
                inPath = paths.themes + '/' + type + '/' + name + '/guide.md';
                outName = type + '-' + name;
            }
            var outFileName = outName + '.md';
            plugins.util.log('Running Guides for ' + outName);
            gulp.src(inPath, { allowEmpty: true })
                .pipe(plugins.rename(outFileName))
                .pipe(gulp.dest(paths.themeGuides))
                .on('end', done);
        },
        thumbs: function(type, subtype, name, done) {
            var inPath;
            var outName;
            if (subtype) {
                inPath = paths.srcThemes + '/' + type + '/' + subtype + '/' + name + '/thumbnail.jpg';
                outName = type + '-' + subtype + '-' + name;
            } else {
                inPath = paths.srcThemes + '/' + type + '/' + name + '/thumbnail.jpg';
                outName = type + '-' + name;
            }
            var outFileName = outName + '.jpg';
            var outPath = paths.themeThumbs + '/' + outFileName;

            exec(`mkdir ${paths.themeThumbs}`, (error, stdout, stderr) => {
                exec(`cp ${inPath} ${outPath}`, (error, stdout, stderr) => {
                    if (error && stderr.indexOf('No such file or directory') < 0) console.error(error);
                    done();
                });
            });
        }
    };
};
