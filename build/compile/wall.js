var async = require('async');
var fs = require('fs');

module.exports = function(gulp, plugins, paths, params) {

    var wallBuild = require('./common')(gulp, plugins, paths);

    var _getAvailableWalls = function() {
        var themes = {};
        var types = fs.readdirSync(paths.wall)
            .filter(function(t) {
                return t !== 'common' && t !== '.DS_Store';
            });

        types.forEach(function(t) {
            var subtypes = fs.readdirSync(paths.wall + '/' + t)
                .filter(function(t) {
                    return t !== 'common' && t !== '.DS_Store';
                });
            themes[t] = subtypes;
        });

        console.log('WALL');
        console.log(themes);

        return themes;
    };


    return function(done) {
        var targets;
        if (params.hasOwnProperty('wall')) {
            var wallParam = params.wall.split('-');
            var subtype = wallParam.shift();
            var name = wallParam.join('-');

            targets = {};
            targets[subtype] = [name];
        } else {
            targets = _getAvailableWalls();
        }

        var q = async.queue(function(task, callback) {
            async.series([
                function(cb) {
                    wallBuild.handlebars('wall', task.subtype, task.wall, cb);
                },
                function(cb) {
                    wallBuild.less('wall', task.subtype, task.wall, cb);
                },
                function(cb) {
                    wallBuild.concat('wall', task.subtype, task.wall, cb);
                },
                function(cb) {
                    wallBuild.labels('wall', task.subtype, task.wall, cb);
                },
                function(cb) {
                    wallBuild.themeOptions('wall', task.subtype, task.wall, cb);
                },
                function(cb) {
                    wallBuild.guides('wall', task.subtype, task.wall, cb);
                }
            ], callback);
        });

        q.drain = done;

        Object.keys(targets).forEach(function(subtype) {

            var walls = targets[subtype];
            for (var i = 0; i < walls.length; ++i) {
                q.push({subtype: subtype, wall: walls[i]});
            }
        });
    };
};
