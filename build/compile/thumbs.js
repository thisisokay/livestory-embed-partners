var async = require('async');
var fs = require('fs');

module.exports = function(gulp, plugins, paths, params) {

    var commonBuild = require('./common')(gulp, plugins, paths);

    var _getAvailableWalls = function() {
        var themes = {};
        var types = fs.readdirSync(paths.srcWall)
            .filter(function(t) {
                return t !== 'common' && t !== '.DS_Store';
            });

        types.forEach(function(t) {
            var subtypes = fs.readdirSync(paths.srcWall + '/' + t)
                .filter(function(t) {
                    return t !== 'common' && t !== '.DS_Store';
                });
            themes[t] = subtypes;
        });

        return themes;
    };

    var _getAvailableWallgroups = function() {
        return fs.readdirSync(paths.srcWallgroup)
            .filter(function(t) {
                return t !== 'common' && t !== '.DS_Store';
            });
    };

    return function(done) {
        var walls = _getAvailableWalls();
        var wallgroups = _getAvailableWallgroups();

        var targets = [ ...Object.keys(walls), ...Object.keys(wallgroups) ];

        var q = async.queue(function(task, callback) {
            async.series([
                function(cb) {
                    commonBuild.thumbs('wall', task.subtype, task.name, cb);
                }
            ], callback);
        });

        q.drain = done;

        Object.keys(walls).forEach(function(subtype) {
            var w = walls[subtype];
            for (var i = 0; i < w.length; ++i) {
                q.push({subtype: subtype, name: w[i]});
            }
        });

        wallgroups.forEach(function(wallgroup) {
            q.push({name: wallgroup});
        });
    };
};
