var async = require('async');
var fs = require('fs');

module.exports = function(gulp, plugins, paths, params) {

    var wallgroupBuild = require('./common')(gulp, plugins, paths);

    var _getAvailableWallgroups = function() {
        return fs.readdirSync(paths.wallgroup)
            .filter(function(t) {
                return t !== 'common' && t !== '.DS_Store';
            });
    };

    return function(done) {
        var targets;
        if (params.hasOwnProperty('wallgroup')) {
            targets = [params.wallgroup];
        } else {
            targets = _getAvailableWallgroups();
            console.log('WALLGROUPS');
            console.log(targets);
        }

        var q = async.queue(function(task, callback) {
            async.series([
                function(cb) {
                    wallgroupBuild.handlebars('wallgroup', null, task.wallgroup, cb);
                },
                function(cb) {
                    wallgroupBuild.less('wallgroup', null, task.wallgroup, cb);
                },
                function(cb) {
                    wallgroupBuild.concat('wallgroup', null, task.wallgroup, cb);
                },
                function(cb) {
                    wallgroupBuild.labels('wallgroup', null, task.wallgroup, cb);
                },
                function(cb) {
                    wallgroupBuild.themeOptions('wallgroup', null, task.wallgroup, cb);
                },
                function(cb) {
                    wallgroupBuild.guides('wallgroup', null, task.wallgroup, cb);
                }
            ], callback);
        });

        q.drain = done;

        targets.forEach(function(wallgroup) {

            q.push({wallgroup: wallgroup});
        });
    };
};
