module.exports = function(gulp, plugins, paths, params, env) {

    return function() {
        plugins.util.log('And now my watch begins!');
        return plugins.watch([
            paths.src + '/**/*.js',
            paths.src + '/**/*.less',
            paths.src + '/**/*.json'
        ], function(changedFile) {

            var relativePath = changedFile.relative;
            var rawMeta = relativePath.split('/');
            var entity = rawMeta.shift();
            var type = rawMeta.shift();
            
            var command = 'gulp';
            
            switch (entity) {
            case 'themes':
                fullName = type;
                if (type === 'wall') {
                    var subtype = rawMeta.shift();
                    if (subtype === 'common') {
                        command += ' compile';
                    } else {
                        var name = rawMeta.shift();
                        fullName += '-' + subtype + '-' + name;
                        command += ' compile --wall=' + subtype + '-' + name;
                    }
                } else if (type === 'wallgroup') {
                    var name = rawMeta.shift();
                    fullName += '-' + subtype + '-' + name;
                    command += ' compile --wallgroup=' + name;
                } else {
                    fullName += rawMeta.join('-');
                    command += ' compile';
                }
                var name = rawMeta.shift();
                fullName += '-' + name;
                plugins.util.log('Theme ' + fullName + ' has been changed!');
                break;
            case 'styles':
                var name = rawMeta.shift();
                var fullName = type + '-' + name;
                plugins.util.log('Style ' + fullName + ' has been changed!');
                command += ' compile --style=' + fullName;
                break;
            default:
                command += ' compile';
            }
            
            command += ' --env=' + env;

            gulp.src('./', {read: false})
                    .pipe(plugins.shell(command))
        });
    }
};
