var CACHE_TIME = 30;

var S3_CONFIG = {
    accessKeyId: 'AKIAXHBF5KDWI54QP5PD',
    secretAccessKey: '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',
    path: 'demopartner',
    region: 'eu-west-1',
    acl: 'private',
    cacheControl: 'max-age=' + CACHE_TIME * 60 + ',public',
    cacheControlDev: 'max-age=' + 0 + ',public',
    expires: new Date(Date.now() + CACHE_TIME * 60000).toUTCString(),
    expiresDev: new Date(Date.now() - 1000).toUTCString()
};

var S3_SHELL_PARAMS = {
    prod: ' --region ' + S3_CONFIG.region + ' --acl ' + S3_CONFIG.acl +
        ' --cache-control "' + S3_CONFIG.cacheControl + '" --expires "' + S3_CONFIG.expires + '"' +
        ' --metadata-directive REPLACE',
    dev: ' --region ' + S3_CONFIG.region + ' --acl ' + S3_CONFIG.acl +
        ' --cache-control "' + S3_CONFIG.cacheControlDev + '" --expires "' + S3_CONFIG.expiresDev + '"' +
        ' --metadata-directive REPLACE'
}

var S3_SHELL_COMMAND = {
    prod: 'aws s3 sync assets/ s3://assets.livestory.io/' + S3_CONFIG.path + '/' + S3_SHELL_PARAMS.prod + ' --delete',
    dev: 'aws s3 sync assets/ s3://assets-dev.livestory.io/' + S3_CONFIG.path + '/' + S3_SHELL_PARAMS.dev + ' --delete'
}

module.exports = function(gulp, plugins, paths, params, env) {

    if (!S3_SHELL_PARAMS.hasOwnProperty(env)) {
        return function() {};
    }

    return plugins.shell.task(S3_SHELL_COMMAND[env], {
        env: {
            AWS_ACCESS_KEY_ID: S3_CONFIG.accessKeyId,
            AWS_SECRET_ACCESS_KEY: S3_CONFIG.secretAccessKey
        }
    });
};
