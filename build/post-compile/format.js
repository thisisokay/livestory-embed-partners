var async = require('async');

module.exports = function(gulp, plugins, paths, params, env) {
    return function(done) {
/*
        if (env !== 'prod') {
            return done();
        }
*/
        async.parallel([
            function(cb) {
                plugins.util.log('Uglifying javascript');
                gulp.src([paths.js + '/*.js', '!' + paths.js + '/*.min.js'], { allowEmpty: true })
                .pipe(plugins.uglify().on('error', console.log))
                .pipe(plugins.extReplace('.min.js'))
                .pipe(gulp.dest(paths.js))
                .on('end', cb);
            },
            function(cb) {
                plugins.util.log('Uglifying javascript');
                gulp.src([paths.javascript + '/**/*.js', '!' + paths.javascript + '/**/*.min.js'], { allowEmpty: true })
                .pipe(plugins.uglify().on('error', console.log))
                .pipe(plugins.extReplace('.min.js'))
                .pipe(gulp.dest(paths.javascript))
                .on('end', cb);
            },
            function(cb) {
                plugins.util.log('Minimizing CSS');
                gulp.src([paths.css + '/*.css', '!' + paths.css + '/*.min.css'], { allowEmpty: true })
                .pipe(plugins.cleanerCss({
                    inline: ['none']
                }))
                .pipe(plugins.extReplace('.min.css'))
                .pipe(gulp.dest(paths.css))
                .on('end', cb);
            }
        ], done);
    };
};
