module.exports = function(gulp, plugins, paths, params, env) {
    return function(done) {
        plugins.util.log('Packaging libraries');
        gulp.src(paths.js + '/*.js', { allowEmpty: true })
            .pipe(plugins.batchReplace([['Handlebars', 'LSHbr']]))
            .pipe(plugins.batchReplace([['packery', 'LSpky']]))
            .pipe(plugins.batchReplace([['Packery', 'LSPky']]))
            .pipe(plugins.batchReplace([['isotope', 'LSitp']]))
            .pipe(plugins.batchReplace([['Isotope', 'LSItp']]))
            .pipe(plugins.batchReplace([['moment', 'LSmmt']]))
            .pipe(gulp.dest(paths.js))
            .on('end', done);
    };
};
