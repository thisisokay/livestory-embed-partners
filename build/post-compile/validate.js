module.exports = function(gulp, plugins, paths, params, env) {
    return function(done) {
        gulp.src(paths.js, { allowEmpty: true })
            .pipe(plugins.jsvalidate())
        done();
    };
};