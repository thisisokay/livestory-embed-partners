var async = require('async');

module.exports = function(gulp, plugins, paths, params, env) {

    var envConfig = require('../configs/env_' + env + '.json');
    var replaceVars = envConfig.map(function(v) {
        return [v.from, v.to];
    });

    return function(done) {

        plugins.util.log('Packaging for ' + env + ' environment');

        async.parallel({
            ls: function (parallelCb) {

                gulp.src(paths.js + '/*.js', { allowEmpty: true })
                    .pipe(plugins.batchReplace(replaceVars))
                    .pipe(gulp.dest(paths.js))
                    .on('end', parallelCb);
            }
        }, done);
    };
};
