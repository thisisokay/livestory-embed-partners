# Index

* [Walls](#walls)
    * [Naming conventions](#walls-naming-conventions)
    * [Folder structure](#walls-folder-structure)
    * [How to define a Theme JS class](#walls-define-theme-js-class)
        * [Customizing single elements](#walls-customizing-single-elements)
    * [Build](#walls-build)

* [Wallgroups](#wallgroups)
    * [Naming conventions](#wallgroups-naming-conventions)
    * [Folder structure](#wallgroups-folder-structure)
    * [How to define a Theme JS class](#wallgroups-define-theme-js-class)
    * [Build](#wallgroups-build)

* [Labels](#labels)
    * [How to add](#how-to-add-labels)
    * [How to use](#how-to-use-labels)

* [Theme Options](#theme-options)
    * [How to add](#how-to-add-theme-options)
    * [How to use](#how-to-use-theme-options)

* [Fonts](#fonts)

* [Color scheme](#color-scheme)
    * [Buttons](#buttons)

* [Partners](#partners)

# <span id="walls"> Walls </span>

Livestory has 4 wall themes, which are:

- wall-strip-default
- wall-grid-default
- wall-freegrid2-default
- wall-customgrid-default

You can also define custom themes, which **must extend one of the above basic themes**, for example

- wall-strip-brand-foo
- wall-strip-brand-bar


## <span id="walls-naming-conventions"> Naming conventions </span>


Theme names are composed by this forms:

- **wall-[WALLSUBTYPE]-default** for default themes
- **wall-[WALLSUBTYPE]-[BRAND]-[NAME]** for custom themes

Within the name you can use underscores instead of white spaces, for example:

- **wall-customgrid-acme-fallwinter2021_hero**
- **wall-customgrid-acme-fallwinter2021_products**
- **wall-customgrid-acme-fallwinter2021_editorial**

**IMPORTANT:** [NAME] **cannnot** contain dashes.

The JS variables must be the same but with **_** instead of **-**, for example **LSTemplates.wall_grid_default**

In particular, for JS Class names, we use the prefix **LSWall_**, for example **LSWall_grid_default**

For CSS class names, we use the prefix **fpls-**, for example **fpls-wall-grid-default**

#### <span id="walls-modules"> Modules </span>

Modules as boxes or popups, if customized, **must end with the [BRAND]-[NAME] suffix**, for example:

**LSBox_post**, if needs to be customized, will become **LSbox_post_acme_1**

## <span id="walls-folder-structure"> Folder structure </span>

The **src** folder contains all the files needed by every template.

A typical theme would be located in a path like this: /src/themes/wall/**THEME-NAME**/

Inside each theme the code is divided in three folders:

- **handlebars**, which contains the template files
- **less**, which contains the CSS sources
- **js**, which contains the main (and accessory) JS files.

### <span id="walls-define-theme-js-class"> How to define a Theme JS class </span>

To use a theme, you must define a JS Class that represents it. Inside the JS Class definition you can also load different handlebars templates if needed.

A sample Theme class would look like this:

    var LSWall_customgrid_acme_1 = function() {
        this.wallTpl = LSTemplates.wall_customgrid_acme_1.wall;
        this.LSBox_post = LSBox_post_customgrid_acme_1;
        this.LSBox_asset = LSBox_asset_customgrid_acme_1;
        this.LSBox_block = LSBox_block_customgrid_acme_1;
        this.LSBox_embed = LSBox_embed_customgrid_acme_1;
        this.LSPopup = LSPopup_customgrid_acme_1;
        this.LSEmbed = LSEmbed_customgrid_acme_1;
    }
    LSHelpers.extend(LSWall_customgrid_acme_1, LSWall_customgrid_default);


In particular, the following line load the custom layout for this wall:

    this.wallTpl = LSTemplates.wall_customgrid_acme_1.wall;

And the following lines specify custom implementations of posts, assets, blocks and embed:

    this.LSBox_post = LSBox_post_customgrid_acme_1;
    this.LSBox_asset = LSBox_asset_customgrid_acme_1;
    this.LSBox_block = LSBox_block_customgrid_acme_1;
    this.LSBox_embed = LSBox_embed_customgrid_acme_1;

Please note that we are also specifying a:

- Custom popup:

    ```
    this.LSPopup = LSPopup_customgrid_acme_1;
    ```

- Custom embed popup:

    ```
    this.LSEmbed = LSEmbed_customgrid_acme_1;
    ```

**IMPORTANT!**: be consistent! If the theme is acme-1, the classes should end with acme_1, and so on.

#### <span id="walls-customizing-single-elements"> Customizing single elements </span>

As you can see in the previous examples, we are customizing the post boxes. To do this, you need to create a JS Class that will load the customized templates.
For example, if we want a different layout for the post box we need to declare a new class:

    var LSBox_post_acme_1 = function() {
        this.photoTpl = LSTemplates.wall_customgrid_acme_1.box_post_photo;
        this.videoTpl = LSTemplates.wall_customgrid_acme_1.box_post_video;
        this.textTpl =  LSTemplates.wall_customgrid_acme_1.box_post_text;
    }
    LSHelpers.extend(LSBox_post_acme_1, LSBox_post);

In this class, we are specifying the differente templates used by this module:

    this.photoTpl = LSTemplates.wall_customgrid_acme_1.box_post_photo;
    this.videoTpl = LSTemplates.wall_customgrid_acme_1.box_post_video;
    this.textTpl =  LSTemplates.wall_customgrid_acme_1.box_post_text;


## <span id="walls-build"> Build </span>

Every theme is composed by a set of files, that are compiled, joined and copied by gulp.

You can find the **wall.js** file contained inside the **build/compile** folder.

---
#  <span id="wallgroups"> Wallgroups </span>

You can define custom themes which **must extend the default theme**, for example

- wallgroup-brand-foo
- wallgroup-brand-bar


## <span id="wallgroups-naming-conventions"> Naming conventions </span>

Theme names are composed by this forms:

- **wallgroup-default** for default themes
- **wallgroup-[BRAND]-[NAME]** for custom themes

Within the name you can use underscores instead of white spaces, for example:

- **wallgroup-acme-fallwinter2021_hero**
- **wallgroup-acme-fallwinter2021_products**
- **wallgroup-acme-fallwinter2021_editorial**

The JS variables must be the same but with **_** instead of **-**, for example **LSTemplates.wallgroup_default**

In particular, for JS Class names, we use the prefix **LSWallgroup_**, for example **LSWallgroup_default**

For CSS class names, we use the prefix **fpls-**, for example **fpls-wallgroup-default**

## <span id="wallgroups-folder-structure"> Folder structure </span>

The **src** folder contains all the files needed by every template.

A typical theme would be located in a path like this: /src/themes/wallgroup/**THEME-NAME**/

Inside each theme the code is divided in three folders:

- **handlebars**, which contains the template files
- **less**, which contains the CSS sources
- **js**, which contains the main JS files.

### <span id="wallgroups-define-theme-js-class"> How to define a Theme JS class </span>

To use a theme, you must define a JS Class that represents it. Inside the JS Class definition you can also load different handlebars templates if needed.

A sample Theme class would look like this:

    var LSWallgroup_acme_1 = function() {
        this.tabContainerTpl = LSTemplates.wallgroup_acme_1.tabs;
        this.wallContainerTpl = LSTemplates.wallgroup_acme_1.tab;
    }
    LSHelpers.extend(LSWallgroup_acme_1, LSWallgroup_default);

In particular, the following line load the custom layout for this wall:

    this.wallContainerTpl = LSTemplates.wallgroup_acme_1.tab;

And the following lines specify custom implementations of tabs:

    this.tabContainerTpl = LSTemplates.wallgroup_acme_1.tabs;


**IMPORTANT!**: be consistent! If the theme is acme-1, the classes should end with acme_1, and so on.

## <span id="wallgroups-build"> Build </span>

Every theme is composed by a set of files, that are compiled, joined and copied by gulp.

You can find the **wallgroup.js** file contained inside the **build/compile** folder.

---
# <span id="labels"> Labels </span>

### <span id="how-to-add-labels"> How to add </span>

To add a custom labels on the console, you will add a **labels.json** file on your custom theme folder, for example:

    [
        {
            "code": "productAction",
            "name": "Product Call to Action",
            "default": "Shop Now"
        }
    ]

- **code** is the is the property name used in API response, must be camelCase
- **name** is the name of the label that appear inside the labels section
- **default** is the default value

### <span id="how-to-use-labels"> How to use </span>

Use labels for customize elements text directly from the console using the assigned **code** and your input value, for example:

    var self = this;

    self.productLabel = 'Shop now';

    if (self.wall.uiLabels && self.wall.uiLabels.productAction) {
        self.productLabel = self.wall.uiLabels.productAction;
    }

---
#  <span id="theme-options"> Theme Options </span>

### <span id="how-to-add-theme-options"> How to add </span>

To add a theme option on the console, you will add a **options.json** file on your custom theme folder, for example:

    [
        {
            "code": "hideTitle",
            "name": "Hide title",
            "tooltip": "Hide the title",
            "input_type": "checkbox",
            "default": false
        }
    ]

- **code** is the is the property name used in API response, must be camelCase
- **name** is the name of the theme option that appear inside the settings > theme settings
- **tooltip** is the text that appear inside the tooltip
- **input_type** checkbox, number, string, color_picker
- **default** is the default value

### <span id="how-to-use-theme-options"> How to use </span>

Use theme options for customize layouts or elements directly from the console using the assigned **code** and your input value, for example:

    var self = this;

    self.container.find('.title').show();

    if (self.wall.themeOptions && self.wall.themeOptions.hideTitle) {
        self.container.find('.title').hide();
    }

---
#  <span id="fonts"> Fonts </span>

A typical brand font would be located in a path like this: assets/fonts/**BRANDNAME**/

Inside each font folder there are:

- **Folder fonts** will contain the files of the fonts you need to integrate
- **Fonts.css** could contain the different @font-face related to the fonts previously added inside the fonts folder, otherwise it could also contain the import of each font that, as per example, Google fonts (in this case, you don't need a @font-face):
- **Fonts.json** will contain the @font-family name (i.e. "name"), also the name of the font that should appear in the console (i.e. “prettyName”), for example:

```
    {
        "fonts": [
            {
                "prettyName": "Montserrat",
                "name": "Montserrat"
            }
        ]
    }
```

**IMPORTANT!**: Live Story inject fonts only inside the console, not inside the web site. Fonts must therefore also be present on the site, and the **@font-face** must match with the font-face added inside the console.

---
#  <span id="color-scheme"> Color Scheme </span>

A typical color scheme would be located in a path like this: src/styles/wall/**BRANDNAME**/

Inside each color scheme there are two mains .less file:

- **admin.less** contains the styles for the admin area, generally used to import the main style
- **main.less** populated with the color-scheme retaled style: in this area you will import the file credits.less (for the credits area), so you can proceed with the style creation within the #WALLID { } code (use to call the destination's or layout's class using its ID)

##  <span id="buttons"> Buttons </span>
#### Buttons classes:

Each button has 3 different CSS classes: **fpls-button**, **ls-button**, and **fpls-box-overlay-button**.

- **ls-button** refers to the standard "SHOP NOW" button once a button is created and / or under the product description
- **fpls-button** refers to the classic button you'll find on a custom template, it also included the product CTA
- **fpls-box-overlay-button** refers to the "SHOP NOW" button that appears as a hover effect on the product

#### Custom buttons:

To add a custom buttons on the console, you will add a **buttons.json** file on your brand color-scheme folder, for example:

    [
        {
            "title": "CUSTOM",
            "inline": "span",
            "classes": "custom-button"
        }
    ]

- **title** is the name of the button that appear inside the text-block formats > buttons
- **inline** is the html button tag
- **classes** is the button class configured inside your color-scheme

---
#  <span id="partners"> For partners </span>

Follow this steps:


1. Clone the partners repo from bitbucket

2. Open the **publish.js** file contained inside the **src/master/build/deploy** folder and edit the **accessKeyId**, **secretAccessKey** and the **path** with personal keys provided by Live Story:

    ```
    var S3_CONFIG = {
        accessKeyId: '12345678',
        secretAccessKey: '12345678',
        path: 'partnername',
    };
    ```

3. If you need to extend Live Story classes and you need original source files for reference, you can access them from S3. With Cyberduck follow these steps:

    **URL**: assets.livestory.io.s3.amazonaws.com

    **Access key id**: your personal accessKeyId

    **Secret access key id**: your personal secretAccessKey

4. Open "other options" and enter your the path "**src**"

5. For the deploy you can use **npm run deploy --env=prod**


**IMPORTANT!**:

For the custom themes, partners need to edit the naming conventions and change **[BRAND]** with **[PARTNER@BRAND]** (**[PARTNER]** is your path value) in this form:

### Walls:

**wall-[WALLSUBTYPE]-[PARTNER@BRAND]-[NAME]**

For example:

- wall-strip-brand-foo -> wall-strip-**partner@brand**-foo

### Wallgroups:

**wallgroup-[PARTNER@BRAND]-[NAME]**

For example:

- wallgroup-brand-bar -> wallgroup-**partner@brand**-bar


#### N.B.:

Theme name used in css and class attributes, will NOT contain the @ symbol, for example:

**wall-strip-partner@brand-foo**

will be converted to

```
<div class="wall-strip-partnerbrand-foo">
```