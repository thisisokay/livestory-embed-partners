var LSWall_customgrid_demopartner_demo_example = function() {
    this.wallTpl = LSTemplates.wall_customgrid_demopartner_demo_example.wall;
}
LSHelpers.extend(LSWall_customgrid_demopartner_demo_example, LSWall_customgrid_default)

LSWall_customgrid_demopartner_demo_example.prototype.renderWall = function(data) {
    var self = this;
    self.parent.renderWall.call(this, data);

    console.log('Customgrid example');

    self.invertBoxesPosition();
}

// Invert boxes position with the theme option
LSWall_customgrid_demopartner_demo_example.prototype.invertBoxesPosition = function() {
    var self = this;

    var wallRow = self.container.find('.fpls-wall-row');

    if (
        self.wall.themeOptions &&
        self.wall.themeOptions.invertBoxesPosition
    ) {
        wallRow.css('flex-direction', 'row-reverse');
    }
}
