/**
 * LSWall_customgrid_default
 * @constructor
 * Class for custom templates. This template is never used as default
 * Each custom template must have its specific class
*/
var LSWall_customgrid_default = function() {
    this.wallTpl = LSTemplates.wall_customgrid_default.wall;
}
LSHelpers.extend(LSWall_customgrid_default, LSWall);

/**
 * Method to load template
 * Added support for context to the template (for example to pass labels, for example "load more", or "click here")
 * Override this method in the actual template class and pass the context like this
 * LSWall_customgrid_demo_example.prototype.loadTemplate = function() {
 *   LSWall_customgrid_default.prototype.loadTemplate.call(this, { myLabel: 'ciao' });
 * } 
 */
LSWall_customgrid_default.prototype.loadTemplate = function(context) {
    var self = this;

    if (self.isSSR) {
        self.container = self.wrapper.find('.fpls-wall-content');
    } else {
        //Hide <!--ADMIN_START ... ADMIN_END--> blocks: these blocks are intended to give instructions to user on the server side
        self.container = $ls(self.wallTpl(context).replace(/<!--ADMIN_START([\s\S]*?)ADMIN_END-->/g, ''));
        self.container.find('[data-layout_id]').css({visibility:'hidden'}).addClass('fpls-empty');
        self.wrapper.html('').append(self.container);
    }
}

/**
 * Method that renders the layout
 * @param {Object} data: wall JSON
 */
LSWall_customgrid_default.prototype.renderWall = function(data, resize) {
    LSWall.prototype.renderWall.call(this, data);

    var self = this;

    self.boxes = [];

    //empty any residual html (from breakpoint switching, for example)
    if (resize) {
        self.container.find('.fpls-placeholder').html('');
    }

    self.current_layout.grid.forEach(function(item) {
        if (item.layout_id) self.appendNode(item);
    });

    // LST-2987 (lazy loading)
    var config = {
        root: null,
        rootMargin: '10px 0px',
        threshold: 0
    }

    self.imgObserver = LSHelpers.createObserver(function(entry) {

        var $img = $ls(entry.target);
        var srcset = $img.attr('lazy-srcset');
        $img.removeAttr('lazy-srcset');
        $img.attr('srcset', srcset);

        var src = $img.attr('lazy-src');

        if (src) {
            $img.removeAttr('lazy-src');
            $img.attr('src', src);
        }

    }, config);

    self.videoObserver = LSHelpers.createObserver(function(entry) {

        var $video = $ls(entry.target);
        // Video poster
        var posterSrc = $video.attr('lazy-poster');
        $video.removeAttr('lazy-poster');
        $video.attr('poster', posterSrc);
        // Video source
        var source = $video.find('source');
        var src = source.attr('lazy-src');
        source.removeAttr('lazy-src');
        source.attr('src', src);

        // load the video:
        // The load() method is used to update the audio/video element after changing the source or other settings.
        $video[0].load();

    }, config);

    //observe imgs
    LSHelpers.initObserver(self.container.find('img[lazy-srcset],img[lazy-src]'), self.imgObserver);

    //observe slides
    LSHelpers.initObserver(self.container.find('.fpls-carousel-item').find('img[lazy-srcset],img[lazy-src]'), self.imgObserver);

    // observe videos
    LSHelpers.initObserver(self.container.find('video[lazy-poster]'), self.videoObserver);

    setTimeout(function() {
        self.detectHash.call(self);
    }, 50);

    try {
        if (self.current_layout.textstyles) LSHelpers.injectTextstyles(self, self.cssBrekapointPrefix + ' .fpls-box', self.current_layout.textstyles, false);
    } catch (error) {}

}

/**
 * Method that appends a single post to the template
 * @param {Object} item: item JSON
 */
LSWall_customgrid_default.prototype.appendNode = function(item) {
    var self = this;

    if (item.layout_id) item.uniqueId = 'ssr' + self.wall.wall_id + '_' + item.layout_id;

    /*
    Check if the layout contains the actual placeholder of the item,
    if not, just skip the item
    */
    var cont = self.container.find('[data-layout_id="' + item.layout_id + '"]');
    if (cont.length < 1) return;

    if (item.type == 'carousel') {
        var element = self.createCarousel(item);
        self.placeElement(element, item.layout_id, null, item);
    } else if (item.type == 'compare') {
        var element = self.handleCompare(item);
        self.placeElement(element, item.layout_id, null, item);
    } else {
        var _box = self.parseNode(item);
        if (!_box.el) return;
        self.boxes.push(_box.el);

        if (!_box.obj.isSSR) _box.el.css({opacity: 0});

        self._loadStackAdd();
        setTimeout(function() {
            _box.obj.onLoad(function(w, h, error) {
                self._loadStackRemove();
                if (error) return;
                 _box.el.css({opacity: 1});

                self.boxLoaded(_box.obj, _box.el);
            }, _box.el);

        }, 100)

        self.placeElement(_box.obj.getHtml(), item.layout_id, _box.obj, item);

    }

    try {
        if (item.type != 'carousel') LSHelpers.injectTextstyles(self, self.cssBrekapointPrefix + ' #' + item.uniqueId, item.textstyles, false);
    } catch (error) {}

}

/**
 * Method that creates a carousel
 * @param {Object} item: item JSON
 * @returns {DOM} carousel html
 */
LSWall_customgrid_default.prototype.createCarousel = function(item) {
    var self = this;

    var element = $ls('<div class="fpls-element"></div>');

    //TODO: manage SSR carousels and existing elements

    //create carousel html
    var elementPrev = $ls('<button class="fpls-element-prev">Previous</button>');
    var elementNext = $ls('<button class="fpls-element-next">Next</button>');
    var wrapper = $ls('<div class="fpls-carousel-inner"></div>');
    wrapper.appendTo(element);

    elementPrev.prependTo(element);
    elementNext.appendTo(element);

    element.addClass('fpls-carousel');

    item.auto_seconds = item.auto_seconds || 3;

    self.fillTrackingCodes(item);

    //create dots html
    if (item.dots) {
        var dotsContainer = $ls('<div class="fpls-dots-container"></div>');
        dotsContainer.appendTo(element);
        for (var i=0; i<=item.subitems.length - 1; i++) {
            var dot = $ls('<span class="fpls-dot"></span>');
            dot.appendTo(dotsContainer);
        }
    }

    //hide arrows
    if (item.hide_arrows) {
        elementPrev.hide();
        elementNext.hide();
    }

    //create child elements and inject them into
    item.subitems && item.subitems.forEach(function(subitem, index) {
        // LST-2987 set offscreen slides
        if (self.wall.themeOptions && self.wall.themeOptions.carouselLazyLoad && (index > (item.slides - 1))) {
            subitem.carouselLazyLoad = true;
        }

        var _box = self.parseNode(subitem, item);
        var subelement = $ls('<div class="fpls-carousel-item" />');
        if (subitem._id) subelement.attr('id', '_' + subitem._id);

        _box.el.appendTo(subelement);

        subelement.appendTo(wrapper);
        self._loadStackAdd();
        _box.obj.onLoad(function(w, h, error) {
            self._loadStackRemove();
            if (error) return;
            if (index == 0) {
                element.css({opacity: 1});
            }
        });

        self.boxes.push(_box.el);
    });

    //function to send analytcs event
    var _sendImpression = function(next) {
        //ANALYTICS: carousel-slide-impression
        var index = next.index();
        var payload = self.getAnalyticsPayload('carousel-slide-impression', item);
        payload.wall_slide_number = index + 1;
        livestory_analytics.pushEvent(payload);
    }

    //caorusel next
    elementNext.on('click', function() {
        var current = element.find('.fpls-current'); 
        var next = current.next('.fpls-carousel-item');
        if (next.length < 1) next = element.find('.fpls-carousel-item').first();

        self.carouselSetCurrent(element, next);
        self.carouselAuto(element, item);

        _sendImpression(next);
    });

    //carousel prev
    elementPrev.on('click', function() {
        var current = element.find('.fpls-current');
        var next = current.prev('.fpls-carousel-item');
        if (next.length < 1) next = element.find('.fpls-carousel-item').last();

        self.carouselSetCurrent(element, next);
        self.carouselAuto(element, item);

        _sendImpression(next);
    });

    //manage dots click
    if (item.dots) dotsContainer.on('click', '.fpls-dot', function() {
        var index = $ls(this).index();
        var next = element.find('.fpls-carousel-item').eq(index);

        self.carouselSetCurrent(element, next);
        self.carouselAuto(element, item);

        _sendImpression(next);
    });

    self.carouselSetCurrent(element, element.find('.fpls-carousel-item').eq(0));
    self.carouselAuto(element, item);

    return element;
}

/**
 * Method that activate current slide of carousel
 * @param {DOM} element: current element
 * @param {DOM} next: next element
 */
LSWall_customgrid_default.prototype.carouselSetCurrent = function (element, next) {
    element.find('.fpls-current').removeClass('fpls-current');
    next.addClass('fpls-current');

    //highlight current dot
    var dotsContainer = element.find('.fpls-dots-container');
    if (dotsContainer) {
        dotsContainer.find('.fpls-dot.fpls-current').removeClass('fpls-current');
        dotsContainer.find('.fpls-dot').eq(next.index()).addClass('fpls-current');
    }
}

/**
 * Enables auto-rotation
 * @param {DOM} element: current element
 * @param {Object} item: item object
 */
LSWall_customgrid_default.prototype.carouselAuto = function(element, item) {
    if (!item.auto) return;
    var next = element.find('.fpls-element-next');
    clearTimeout(item.__autoTimer);
    item.__autoTimer = setTimeout(function() {
        next.trigger('click');
    }, item.auto_seconds * 1000);
}

/**
 * Method that creates a compare
 * @param {Object} item: item JSON
 * @returns {DOM} compare html
 */
LSWall_customgrid_default.prototype.handleCompare = function (item) {
    var self = this;

    var element;

    var found = false;
    if (self.isSSR) {
        element = self.container.find('.fpls-element');
        var slider = element.find('.fpls-compare__inner');
        if (slider.length > 0) found = true;
    }

    if (!found) {
        element = $ls('<div class="fpls-element"></div>');
        element.addClass('fpls-compare');
        var slider = $ls('<div class="fpls-compare__inner" />');
        slider.appendTo(element);
        slider.wrap('<div class="fpls-box-compare" />');
    }

    if (item) self.getAndSetCompareOptions(item, element);

    item.subitems && item.subitems.forEach(function(subitem, index) {
        var _box = self.parseNode(subitem, item);
        var subelement = $ls('<div class="fpls-compare__item" />');
        if (subitem._id) subelement.attr('id', '_' + subitem._id);

        _box.el.appendTo(subelement);

        subelement.appendTo(slider);
        if (!self.isSSR) element.css({opacity: 0});

        self._loadStackAdd();
        _box.obj.onLoad(function(w, h, error) {
            self._loadStackRemove();
            if (error) return;
            if (index == 0) {
                subelement.addClass('fpls-compared-slide');
            }

            if (index == 1) {
                subelement.find('.fpls-box').width(element.width());
                element.css({opacity: 1});
            }
        });

        self.boxes.push(_box.el);
    });

    var slideToCompare = element.find('.fpls-compare__item').eq(1);
    slideToCompare.addClass('fpls-slide-to-compare');

    self.createCompareSlider(item, element);

    return element;
}

/**
 * Method that get and set the compare custom options
 * @param {Object} item: item JSON
 * @param {DOM} element: the compare element
 */
LSWall_customgrid_default.prototype.getAndSetCompareOptions = function (item, element) {
    var self = this;

    if (item.compare_color) element[0].style.setProperty('--compare-slider-color', item.compare_color);
    if (item.compare_opacity) {
        element[0].style.setProperty('--compare-slider-opacity', item.compare_opacity + '%');
        element.addClass('fpls-custom-opacity');
    }
    if (item.compare_reverse_arrows_color) element.addClass('fpls-reverse-arrows-color');
}

/**
 * Method that create compare elements
 * @param {Object} item: item JSON
 * @param {DOM} element: the compare element
 */
LSWall_customgrid_default.prototype.createCompareSlider = function (item, element) {
    var self = this;

    var sliderPosition = item && item.compare_slider_position || 50;

    var slideToCompare = element.find('.fpls-slide-to-compare');
    slideToCompare.addClass('fpls-slide-to-compare__element');
    var slideToCompareWrapper = $ls('<div class="fpls-slide-to-compare__wrapper"> </div>');
    if (!self.isSSR) slideToCompare.wrap(slideToCompareWrapper);
    var compareSlider = $ls('<div class="fpls-slide-to-compare__slider"> </div>');
    if (!self.isSSR) element.find('.fpls-slide-to-compare__wrapper').prepend(compareSlider);

    setTimeout(function(){
        slideToCompare.css({
            width: sliderPosition + '%'
        });

        compareSlider.css({
            'left': sliderPosition + '%'
        });

        self.moveCompareSlider(slideToCompare, element);
    }, 50)

    $ls(window).on('resize', function() {
        self.adjustCompare(element, slideToCompare);
    });

};

/**
 * Method that listen resize event
 * and adjust slide to compare width
 * @param {DOm} compareElement: the compare carousel
 * @param {DOM} slideToCompare: the slide to compare
 */
LSWall_customgrid_default.prototype.adjustCompare = function (compareElement, slideToCompare) {
    var self = this;

    var compareElementWidth = compareElement.width();

    slideToCompare.find('.fpls-box')
        .width(compareElementWidth);
}

/**
 * Method that set the compare slider move
 * @param {DOM} slideToCompare: the slide to compare
 * @param {DOM} compareElement: the compare element
 */
LSWall_customgrid_default.prototype.moveCompareSlider = function (slideToCompare, compareElement) {
    var self = this;

    var clicked = 0;
    var compareSlider = compareElement.find('.fpls-slide-to-compare__slider');

    compareSlider.on("mousedown", slideReady);
    $ls(window).on("mouseup", slideFinish);
    compareSlider.on("touchstart", slideReady);
    $ls(window).on("touchend", slideFinish);

    function slideReady(e) {
        e.preventDefault();
        clicked = 1;
        $ls(window).on("mousemove", slideMove);
        $ls(window).on("touchmove", slideMove);
    };

    function slideFinish() {
        clicked = 0;
    };

    function slideMove(e) {
        var pos;
        if (clicked == 0) return false;
        pos = getCursorPos(e)
        if (pos < 0) pos = 0;
        if (pos > 100) pos = 100;
        slide(pos);
    };

    function getCursorPos(e) {
        var a, w, x = 0;
        e = (e.changedTouches) ? e.changedTouches[0] : e;
        a = slideToCompare.offset().left;
        w = compareElement.width();
        x = e.pageX - a;
        x = (x * 100) / w;
        return x;
    };

    function slide(x) {
        slideToCompare.width(x + '%');
        compareSlider.css({
            'left': x + '%'
        })
    };

}

/**
 * Method that instantiates the box element for an item JSON
 * @param {Object} item: item JSON
 * @param {Function} parent: parent item JSON
 * @returns {Object} mixed object with instance and html DOM
 */
LSWall_customgrid_default.prototype.parseNode = function(item, parent) {
    var self = this;
    var boxObj;
    var empty = {
        el: false,
        obj: false
    };

    switch (item.type) {
        case 'photo':
        case 'video':
        case 'text':
        case 'mixed':
            //UGC or manual upload media (video or photo)
            if (!self.posts[item.id]) return empty;

            var _item = $ls.extend({}, item, self.posts[item.id]);
            _item.default_image_url = item.img;
            _item._id = item._id;

            boxObj = new self.LSBox_post();
            var resolution = self.optimalResolution(item, parent);
            boxObj.init(_item, self, resolution);
            break;
        case 'asset':
            //Assets aka products
            if (!self.products[item.id]) return empty;
            var _item = $ls.extend({}, item, self.products[item.id]);
            var resolution = self.optimalResolution(item, parent);
            boxObj = new self.LSBox_asset();
            boxObj.init(_item, self, resolution);
            break;
        case 'block':
            //Text blocks
            boxObj = new self.LSBox_block();
            boxObj.init(item, self);
            break;
        case 'embed':
            //Iframes and such
            var boxObj = new self.LSBox_embed();
            var resolution = self.optimalResolution(item, parent);
            boxObj.init(item, self, resolution);
            break;
    }

    if (!boxObj) return empty;

    var box = boxObj.getHtml();

    //add custom css class
    if (item.cssclass) box.addClass(item.cssclass);

    //add data-item-class to identify blocks with css class
    if (item.cssclass) box.attr('data-item-class', item.cssclass);

    //add data-item-id to block (to allow specific targeting)
    if (item._id) box.attr('data-item-id', item._id);

    //Add specific attributes for gridster
    box.data('id', item.id);
    box.data('type', item.type);
    box.data('popup_disabled', item.popup_disabled);
    box.data('link', item.link);

    box.addClass('fpls-content');

    return {
        el: box,
        obj: boxObj
    }
}

/**
 * function called after the wall is rendered.
 */
LSWall_customgrid_default.prototype.postRender = function() {
    var self = this;
    LSWall.prototype.postRender.call(this);
    var els = self.container.find('a[href],[data-href]');
    livestory_sfcc.processUrls(els);
}

/**
 * Callback called when box is loaded
 * @param {Object} boxObj: box instance
 * @param {DOM} box: box html
 */
LSWall_customgrid_default.prototype.boxLoaded = function(boxObj, box) {}

/**
 * Place element on the appropriate DIV placeholder
 * @param {DOM} box: box html
 * @param {String} id: id of the placeholder
 * @param {Object} boxObj: box instance
 */
LSWall_customgrid_default.prototype.placeElement = function(box, id, boxObj, item) {
    var self = this;

    //Find the placeholder DIV in the handlebar template
    var cont = self.container.find('[data-layout_id="' + id + '"]');
    var dest = cont.find('[data-placeholder_id]');

    //add custom css class
    if (item && item.cssclass) cont.addClass(item.cssclass);

    //add data-item-class to identify blocks with css class
    if (item && item.cssclass) cont.attr('data-item-class', item.cssclass);

    //add a11y HTML attributes
    if (item && item.accessibility) self.setAccessibilityAttributes(box, item.accessibility);

    //IF in SSR and container is not empty, no need to append the new element
    if (self.isSSR && !cont.hasClass('fpls-empty')) {
        return;
    }

    //Replace placeholder content with actual box
    dest.html('').append(box);

    //show the box and remove the class "empty"
    cont.css({visibility:'visible'}).removeClass('fpls-empty');

    //Add a class to the container, to identify the box type
    //For example, if box has class fpls-box-photo, the placeholder gets the class fpls-wall-content-box-photo
    box.attr('class').split(' ').forEach(function(className) {
        if (className.indexOf('fpls-box-') == 0) cont.addClass(className.replace('fpls-box-', 'fpls-wall-content-box-'));
    });
}

/**
 * Prev method: it is used by the popup to show the prev image when clicking to the popup arrows
 * @param {String} id: ID of the current post open in popup
 */
LSWall_customgrid_default.prototype.prev = function(id) {
    var self = this;
    var found;
    var posts = self.boxes.filter(self._filterPosts);
    posts.forEach(function(post, index) {
        if (post.data('id') == id) found = index;
    });
    found--;
    if (found < 0) found = posts.length - 1;
    if (posts[found])
        posts[found].trigger('click.ls');

}

/**
 * Next method: it is used by the popup to show the next image when clicking to the popup arrows
 * @param {String} id: ID of the current post
 */
LSWall_customgrid_default.prototype.next = function(id) {
    var self = this;
    var found;
    var posts = self.boxes.filter(self._filterPosts);
    posts.forEach(function(post, index) {
        if (post.data('id') == id) found = index;
    });
    found++;
    if (found >= posts.length) found = 0;
    if (posts[found])
        posts[found].trigger('click.ls');
}

/**
 * Method to determine if "prev" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
LSWall_customgrid_default.prototype.prevAvailable = function(id) {
    var self = this;
    return self.boxes.filter(self._filterPosts).length > 1;
}

/**
 * Method to determine if "next" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
LSWall_customgrid_default.prototype.nextAvailable = function(id) {
    var self = this;
    return self.boxes.filter(self._filterPosts).length > 1;
}

/**
 * Method to calculate correct resolution of items in grid
 * @param {Object} item: item JSON
 * @param {Object} parent_item: parent item JSON (used for groups or carousels)
 * @returns size in pixel
 */
LSWall_customgrid_default.prototype.optimalResolution = function(item, parent_item) {
    var self = this;
    var id = item.layout_id;
    if (parent_item) id = parent_item.layout_id;
    var cont = self.container.find('[data-layout_id="' + id + '"]');
    var width = cont.outerWidth() * 1.25;
    if (cont.attr('data-resolution')) width = Math.min(width, parseFloat(cont.attr('data-resolution')));
    return Math.ceil(width / 50) * 50;
}