/**
 * LSWall
 * @constructor
 * Abstract class for all layouts
*/
var LSWall = function() {
    this.posts = {};
    this.products = {};
    this.lang = 'en';
    this.store = 'en';
    this.hashtag = false;
    this.placeholder = false;
    this.jsonData = false;
    this.wallgroup = false;
    this.mainContainer = false;
    this.isSSR = false;
    this.trackingCodes = [];

    // Store references to classes, so we can switch them easily
    this.LSBox_post = LSBox_post;
    this.LSBox_asset = LSBox_asset;
    this.LSBox_block = LSBox_block;
    this.LSBox_embed = LSBox_embed;
    this.LSBox_icon = LSBox_icon;
    this.LSPopup = LSPopup;

    this._loadStack = 0;
}

/**
 * Init method for Wall
 * @param {DOM Element} wrapper - The div containing the wall. If layout, it coincides with mainContainer, if in destination, it's the div containing the layout
 * @param {Object} options - Layout options
 * @param {Object} wall - JSON of the wall content
 * @param {DOM} mainContainer - outermost div of LS, the one with data-store
 */
LSWall.prototype.init = function(wrapper, options, wall, mainContainer, rootContainer) {
    /* 
    wrapper: is the div containing the wall
    mainContainer: is the root div, either wall or destination. The one with data-store
    wall: is the wall plain object
    */
    var self = this;
    self.wall = wall;
    LSHelpers.debug('Wall init ' + self.wall.wall_id);
    self.wrapper = wrapper;
    self.options = options;
    self.jsonData = wall;
    self.mainContainer = mainContainer;
    self.rootContainer = rootContainer || self.mainContainer;
    self.parseDataAttributes();

    if ((self.mainContainer.find('[data-ssr="true"]').length > 0 || //container is ssr
         self.mainContainer.parents('[data-ssr="true"]').length > 0) && //parents are ssr
        self.wrapper.children('div').length > 0 && //has actually some content
        (self.wall.ssr || self.wallgroup && self.wallgroup.ssr) //ssr flag is active in payload (either in wall or wallgroup)
        ) {
          self.isSSR = true;
          if (self.wrapper.attr('data-revision') != self.wall.revision_id) {
              console.warn('REVISION MISMATCH ' + self.wall.wall_id);
              console.warn('SSR:' + self.wrapper.attr('data-revision'));
              console.warn('API:' + self.wall.revision_id);

              livestory_analytics.pushEvent({
                event_type: 'embedcache-needsrefresh',
                wall_id: self.wall.wall_id,
                brand: self.wall.brand
              });

              //trigger refresh if parent layouts are detected
              if (self.options && self.options.parent_wall) {
                var parentSelf = self.options.parent_wall;
                livestory_analytics.pushEvent({
                    event_type: 'embedcache-needsrefresh',
                    wall_id: parentSelf.wall_id,
                    brand: parentSelf.brand
                });

                if (parentSelf.wallgroup && parentSelf.wallgroup._id) {
                    livestory_analytics.pushEvent({
                        event_type: 'embedcache-needsrefresh',
                        wallgroup_id: parentSelf.wallgroup._id,
                        brand: parentSelf.wallgroup.brand
                    });
                  }
              }

              //trigger refresh of the destination as well
              if (self.wallgroup && self.wallgroup._id) {
                livestory_analytics.pushEvent({
                    event_type: 'embedcache-needsrefresh',
                    wallgroup_id: self.wallgroup._id,
                    brand: self.wallgroup.brand
                });
              }

              //if (window.confirm('SSR code is outdated, do you want to replace the html with the online version?')) {
                    self.mainContainer.attr('data-ssr-status', 'not sync');
                    self.isSSR = false;
              //}
          } else if (self.wrapper.attr('data-wall-lang') != self.wall.lang_code) {
            console.warn('LANGUAGE MISMATCH');
            console.warn('SSR:' + self.wrapper.attr('data-wall-lang'));
            console.warn('API:' + self.wall.lang_code);

            // DO NOT INVALIDATE SSR FOR LANGUAGE MISMATCH
            // if (self.wrapper.attr('data-wall-lang')) {
            //     self.isSSR = false;
            //     self.wrapper.html('');
            // }
          }
    } else {
        //we need to clean the wall since SSR is outdated or empty
    }

    self.wrapper.attr('data-wall-id', self.wall.wall_id);
    self.wrapper.attr('data-revision', self.wall.revision_id);
    self.wrapper.attr('data-wall-lang', self.wall.lang_code);

    self.loadWall();

    var _move_timer;

    self.wrapper.on('mousemove.ls', function(e) {
        clearTimeout(_move_timer);

         _move_timer = setTimeout(function() {
            var payload = self.getAnalyticsPayload('mouse-pause', null, e);
            livestory_analytics.pushEvent(payload, {
                wall: self.wall,
                wallgroup: self.wallgroup
            });
        }, 500);
    });
}

/**
 * Helper function to keep count of loaded elements
 */
LSWall.prototype._loadStackAdd = function() {
    this._loadStack++;
}

/**
 * Helper function to keep count of loaded elements.
 * When count is zero, it calls LSWall.onComplete() method
 */
LSWall.prototype._loadStackRemove = function() {
    var self = this;
    this._loadStack--;
    if (this._loadStack < 1 && !this._loadStackNotified) {
        clearTimeout(self.__loadStackTimer);
        self.__loadStackTimer = setTimeout(function() {
            LSHelpers.debug('Wall is now complete: ' + self.wall.wall_id);
            self.wrapper.trigger('ls-wall-complete');
            self._loadStackNotified = true;
            self.onComplete && self.onComplete();
        }, 100);
    }
}

/**
 * Parse attributes of root div, such as data-store, data-lang etc.
 * It stores all these data as porperties
 */
LSWall.prototype.parseDataAttributes = function() {
    var self = this;
    LSHelpers.debug('Wall parseDataAttributes ' + self.wall.wall_id);
    self.lang = self.mainContainer.data('lang');
    self.store = self.mainContainer.data('store');
    self.hashtag = self.mainContainer.data('hashtag');
    self.skus = self.mainContainer.data('sku');
    self.post_type = self.mainContainer.data('type');
    self.revision_id = self.mainContainer.data('revision');
    self.placeholder = self.mainContainer.data('placeholder');

    //if no store or lang passed, take it from the browser
    if (self.store == 'STORE_ID' && !self.lang) {
        self.lang = LSHelpers.defaultLang();
    }
}

/**
 * Main loader of wall
 * it calls all the different methods needed to render the layout
 */
LSWall.prototype.loadWall = function() {
    var self = this;
    LSHelpers.debug('Wall loadWall ' + self.wall.wall_id);

    self.cssBrekapointPrefix = ".fpls-" + self.wall.wall_id;

    self.context = {
        attributes: self.wall.attributes || '',
        uiLabels: self.wall.uiLabels || '',
        themeOptions: self.wall.themeOptions || ''
    }

    self.loadThemeAssets(function() {
        self.initResponsive();
        self.postRender();
        self.hideLoader();
        self.sendAnalytics();
        self.wrapper.trigger('ls-load-wall');
    });
    self.showLoader();
    self.loadTemplate(self.context);
    self.globalSettings();
    self.extractElements(self.wall);
}

/**
 * Listen to resize event in order to switch breakpoints
 */
LSWall.prototype.initResponsive = function() {
    var self = this;
    LSHelpers.debug('Wall initResponsive ' + self.wall.wall_id);
    self.switchResponsive();
    $ls(window).on('ls-delayed-resize', function() {
        self.switchResponsive(true);
    });
}

/**
 * Method called on resize, it determine the most appropriate breakpoint for the resolution
 * It then calls the renderWall() method
 */
LSWall.prototype.switchResponsive = function(resize) {
    var self = this;
    LSHelpers.debug('Wall switchResponsive ' + self.wall.wall_id);
    var wwidth = self.mainContainer.width();
    var found = false;
    var found_breakpoint = false;

    self.previous_layout = self.current_layout;
    self.previous_sections = self.current_sections;

    if (self.wall.subwalls && self.wall.subwalls.length > 0) {
        self.wall.subwalls.forEach(function(subwall) {
            if (wwidth <= subwall.breakpoint) {
                found = subwall.subwall;
                found_breakpoint = subwall.breakpoint;
            }
        });
    }

    if (found) {
        self.current_sections = found.sections;
        self.current_layout = found.layout || {};
        self.current_options = found.options;
    } else {
        self.current_sections = self.wall.sections;
        self.current_layout = self.wall.layout || {};
        self.current_options = self.wall.options;
    }

    //store current breakpoint
    self.current_breakpoint = found_breakpoint;

    //add check to see if SSR breakpoint is correct
    if (self.isSSR) {
        var current_breakpoint = self.current_breakpoint || 'default';
        var ssr_breakpoint = self.wrapper.attr('data-breakpoint');

        if (current_breakpoint != ssr_breakpoint) {
            LSHelpers.warn('BREAKPOINT NON COINCIDONO', current_breakpoint, ssr_breakpoint);
            self.isSSR = false;
        }
    }

    if (self.current_breakpoint) {
        self.container && self.container.addClass('fpls-breakpoint-active');
        self.wrapper.attr('data-breakpoint', self.current_breakpoint);
    } else {
        self.container && self.container.removeClass('fpls-breakpoint-active');
        self.wrapper.attr('data-breakpoint', 'default');
    }

    //prefix to use as scope for css prefix
    self.cssBrekapointPrefix = ".fpls-" + self.wall.wall_id + '[data-breakpoint="' + (self.current_breakpoint || 'default') + '"]';

    //redesign only if resolution has changed...
    if (self.current_layout !== self.previous_layout || self.current_sections !== self.previous_sections) {

        //disable SSR when resizing
        if (resize) {
            //TODO: in caso di resize, dato che ci sono i media query, va switchato il wrapper e lasciato l'ssr acceso
            if (self.isSSR) {
                var ssrWrapper = self.wrapper.parents('[data-ssr-version="3"]').eq(0);
                if (ssrWrapper.length) {
                    //in case of SSR, actually identify the correct container according to media query
                    var matchWidth = self.mainContainer.width();
                    var found = ssrWrapper.find(' > [ssr-breakpoint]').filter(function() {
                        var min = $ls(this).attr('data-min');
                        var max = $ls(this).attr('data-max');
                        if (min && max) {
                            return matchWidth >= min && matchWidth <= max;
                        } else if (min) {
                            return matchWidth >= min;
                        } else if (max) {
                            return matchWidth <= max;
                        }
                    });
    
                    //found actual media query, assign to ssrWrapper
                    if (found.length) {
                        self.wrapper = found.find('[data-wall-id]').eq(0);
                        self.container = found.find('.fpls-wall-content').eq(0);
                    } else {
                        self.isSSR = false;
                    }
                } else {
                    self.isSSR = false;
                }
            }
        }

        LSHelpers.debug('Switch responsive ' + self.wall.wall_id);
        self.previous_layout = self.current_layout;
        self.setMaxWidth();
        self.renderWall(self.wall, resize);
    }
}

/**
 * Assemble API url to get wall payload
 * @returns {String} url of the API to call
 */
LSWall.prototype.getUrl = function(skip) {
    var self = this;
    var url = LSConfig.apiURL + 'wall/' + self.wall.wall_id;

    // filter by hashtag if present
    url = LSHelpers.appendFilters(url, {
        hashtags: self.hashtag,
        skus: self.sku,
        post_type: self.post_type,
        store_code: self.store,
        lang_code: self.lang,
        revisionid: self.revision_id
    });

    if (url.indexOf('?') > 0) url += '&skip=' + skip;
    if (url.indexOf('?') < 0) url += '?skip=' + skip;

    return url;
}

/**
 * Append all the css required by the layout
 * @param {Function} callback: function called when all css are loaded
 */
LSWall.prototype.loadThemeAssets = function(callback) {
    var self = this;
    LSHelpers.debug('Wall loadThemeAssets ' + self.wall.wall_id);
    self.parent_theme = LSHelpers.getDefaultTheme(self.wall.theme);

    var cssToLoad = 2;
    if (self.parent_theme) cssToLoad = 3;

    // Load CSS for the desired theme
    if (self.parent_theme) LSHelpers.appendCss(LSHelpers.getAssetURL(self.parent_theme) + '/css/' + self.parent_theme + '.css', function() {
        cssToLoad--;
        if (cssToLoad == 0) {
            callback();
        }
    }, self.rootContainer);

    LSHelpers.appendCss(LSHelpers.getAssetURL(self.wall.theme) + '/css/' + self.wall.theme + '.css', function() {
        cssToLoad--;
        if (cssToLoad == 0) {
            callback();
        }
    }, self.rootContainer);

    // Inject CSS style, "namespaced" by the wall ID
    LSHelpers.injectCss(LSHelpers.getAssetURL(self.wall.style) + '/css/' + self.wall.style + '.css', self.cssBrekapointPrefix, self.mainContainer, self.wall.style, function() {
        cssToLoad--;
        if (cssToLoad == 0) {
            callback();
        }
    });

    // Add class for the "namespaced" CSS
    self.wrapper.addClass("fpls-" + self.wall.wall_id);
}

/**
 * Abstract method to load templates, must be implemented in specific layout Classes
 * Added support for context to the template (for example to pass labels, for example "load more", or "click here")
 * Override this method in the actual template class and pass the context like this
 * LSWall_customgrid_demo_example.prototype.loadTemplate = function() {
 *   LSWall_customgrid_default.prototype.loadTemplate.call(this, { myLabel: 'ciao' });
 * } 
 */
LSWall.prototype.loadTemplate = function(context) {
    LSHelpers.error('"loadTemplate()" Method must be implemented!');
}

/**
 * Inject global styles for fonts
 */
LSWall.prototype.globalSettings = function() {
    var self = this;

    // Inherit fonts from wallgroup if not defined:
    if (self.wallgroup) {
        self.wall.font_body = self.wall.font_body || self.wallgroup.font_body;
        self.wall.font_heading = self.wall.font_heading || self.wallgroup.font_heading;
    }

    // Inject fonts for default theme (parent)
    if (self.parent_theme) {
        LSHelpers.injectFonts(LSHelpers.getAssetURL(self.parent_theme) + '/css/' + self.parent_theme + '-fonts.css', 
        self.wall.wall_id, 
        self.wall.font_body, 
        self.wall.font_heading, 
        self.mainContainer);
    }

    //Inject font CSS for this wall
    LSHelpers.injectFonts(LSHelpers.getAssetURL(self.wall.theme) + '/css/' + self.wall.theme + '-fonts.css', 
        self.wall.wall_id, 
        self.wall.font_body, 
        self.wall.font_heading, 
        self.mainContainer);

    //Inject custom inline CSS for this wall
    if (self.wall.customcss) LSHelpers.injectInlineCss(self.wall.customcss, self.cssBrekapointPrefix, self.mainContainer, self.wall.theme);
}

/**
 * Sets max width of the layout according to max width: settings
 */
LSWall.prototype.setMaxWidth = function() {
    var self = this;
    LSHelpers.debug('Set max width ' + self.wall.wall_id);
    
    if (!self.current_layout) return;
    if (!self.current_layout.maxwidth) return;

    //if number without measures, add px
    var unit = '';
    if (parseFloat(self.current_layout.maxwidth).toString() == self.current_layout.maxwidth.toString()) unit = 'px';

    if (self.current_layout && self.current_layout.maxwidth) {
        self.container && self.container.css({
            'max-width': self.current_layout.maxwidth + unit,
            'margin-left': 'auto',
            'margin-right': 'auto'
        });
    }
}

/**
 * Convert arrays in an associative arrays, posts: { 'id1': { ...value1 }, 'id2': { ...value2 } }
 * Arrays are stored as class properties
 * @param {Object} data: wall JSON
 */
LSWall.prototype.extractElements = function(data) {
    var self = this;

    // Create associative object for posts, easier for search
    if (data.posts.length) data.posts.forEach(function(item) {
        self.posts[item._id] = item;
    });

    // Create associative object for products, easier for search
    if (data.products.length) data.products.forEach(function(item) {
        self.products[item._id] = item;
    });

    // Create associative object for products, easier for search
    if (data.trackingCodes && data.trackingCodes.length) data.trackingCodes.forEach(function(item) {
        self.trackingCodes[item._id] = item;
    });

    // Set the wall live if active
    self.live = data.live;
}

/**
 * This method find tracking codes belonging to a specific item from the global list
 * it "pushes" the tracking codes into a new variable with "Parsed" suffix
 * @param {Object} item: item with tracking code
 */
LSWall.prototype.fillTrackingCodes = function(item) {
    var self = this;
    if (item.trackingCodes && item.trackingCodes.length > 0) {
        item.trackingCodesParsed = [];
        item.trackingCodes.forEach(function(id) {
            if (typeof self.trackingCodes[id] == 'undefined') return;
            item.trackingCodesParsed.push(self.trackingCodes[id]);
        });
    }
}

/**
 * Abstract method that renders the layout
 * must be implemented in specific classes
 * @param {Object} data: wall JSON
 */
LSWall.prototype.renderWall = function(data) {
    //renderWall() Method must be implemented
}

/**
 * Method called after the layout is rendered
 * Be aware it doesn't mean the layout is fully loaded
 */
LSWall.prototype.postRender = function() {
    var self = this;
    var title = self.wrapper.find('.fpls-wall-title');
    if (self.wall.layout && self.wall.layout.showtitle) {
        title && title.html(self.wall.title);
    } else {
        title && title.remove();
    }
}

/**
 * Hide loader
 */
LSWall.prototype.hideLoader = function() {
    var self = this;
    self.mainContainer.removeClass('fpls-cont-loading');
    self.mainContainer.find('.fpls-cont-loading').removeClass('fpls-cont-loading');
    self.mainContainer.addClass('fpls-cont-loaded');
}

/**
 * Show loader
 * NB: it hides the loading icon anyway after 15 seconds
 */
LSWall.prototype.showLoader = function() {
    var self = this;
    self.mainContainer.addClass('fpls-cont-loading');
    // hide loading aniway if stuck...
    setTimeout(function() {
        self.hideLoader();
    }, 15 * 1000);
}

/**
 * Method to send wall and page impression events
 */
LSWall.prototype.sendAnalytics = function() {
    var self = this;

    //ANALYTICS: wall-impression
    var payload = self.getAnalyticsPayload('wall-impression');
    livestory_analytics.pushEvent(payload, {
        wall: self.wall,
        wallgroup: self.wallgroup
    });

    //ANALYTICS: page-impression (triggered only one time)
    if (window.LS_PageImpressionTriggered !== true) {
        var payload = self.getAnalyticsPayload('page-impression');
        livestory_analytics.pushEvent(payload);

        window.LS_PageImpressionTriggered = true;
    }
}

/**
 * Method to send wall impression event
 * @param {String} url: url to add the utm tags
 * @param {Object} post: post JSON
 * @param {Object} asset: asset JSON
 * @returns {String} url with utm tags appended
 */
LSWall.prototype.addUtmToUrl = function(url, post, asset) {
    var self = this;

    //append clientId and brand (to propagate the information through different domains)
    var brand = self.wall.brand || livestory_analytics.getBrand();
    var client_id = livestory_analytics.getClientId();

    if (typeof url !== 'string') return url;

    var finalQueryString = '';

    if (self.wall.options.custom_utm_string) {

        var customUtmString = self.wall.options.custom_utm_string;

        //remove initial query separator, since we add it manually later
        if (customUtmString[0] == '&' || customUtmString[0] == '?') customUtmString = customUtmString.slice(1);

        finalQueryString += customUtmString;
        finalQueryString = finalQueryString.replace(/\[LAYOUT_NAME\]/g, self.wall.title || "");
        finalQueryString = finalQueryString.replace(/\[LAYOUT_ID\]/g, self.wall.wall_id || "");
        finalQueryString = finalQueryString.replace(/\[STORY_ID\]/g, self.wall.campaign_id || "");
        finalQueryString = finalQueryString.replace(/\[STORY_NAME\]/g, self.wall.campaign_name || "");
        finalQueryString = finalQueryString.replace(/\[STORE_CODE\]/g, self.store || "");
        finalQueryString = finalQueryString.replace(/\[LANG_CODE\]/g, self.lang || "");

        finalQueryString = finalQueryString.replace(/\[DESTINATION_NAME\]/g, self.wallgroup && self.wallgroup.title || "");
        finalQueryString = finalQueryString.replace(/\[DESTINATION_ID\]/g, self.wallgroup && self.wallgroup._id || "");

        //post
        finalQueryString = finalQueryString.replace(/\[POST_ID\]/g, post && post._id || "");
        finalQueryString = finalQueryString.replace(/\[POST_TYPE\]/g, post && post.type || "");
        finalQueryString = finalQueryString.replace(/\[POST_SOURCE\]/g, post && post.source || "");
        finalQueryString = finalQueryString.replace(/\[POST_USERNAME\]/g, post && post.author && post.author.username || "");

        //product
        finalQueryString = finalQueryString.replace(/\[PRODUCT_NAME\]/g, asset && asset.name || "");
        finalQueryString = finalQueryString.replace(/\[PRODUCT_SKU\]/g, asset && asset.sku || "");
        finalQueryString = finalQueryString.replace(/\[PRODUCT_PRICE\]/g, asset && asset.price || "");

    } else if (!self.wall.options.utm_tags === false) {

        finalQueryString += 'utm_source=LS';
        finalQueryString += '&utm_campaign=LS-CAMP-' + self.wall.campaign_id;
        finalQueryString += '&utm_medium=LS-WALL-' + self.wall.wall_id;

        if (self.hashtags) {
            finalQueryString += '&utm_term=LS-HASH-' + self.hashtags;
        }

        if (post) {
            finalQueryString += '&utm_content=' + 'LS-POST-' + post._id;
        }
    }

    if (finalQueryString == '') return url;

    if (url.indexOf('?') < 0) {
        finalQueryString = '?' + finalQueryString;
    } else {
        finalQueryString = '&' + finalQueryString;
    }

    hashCharIndex = url.indexOf('#');
    if (hashCharIndex >= 0) {
        url = url.slice(0, hashCharIndex) + finalQueryString + url.slice(hashCharIndex);
    } else {
        url += finalQueryString;
    }

    return url;

}

/**
 * Method to get all the analytics common values, such as wall id, campaign id etc.
 * @param {String} eventType: type of the event
 * @param {Object} item: optional item to which the event is related
 * @param {Event} e: native event object
 * @returns {Object} analytics payload
 */
LSWall.prototype.getAnalyticsPayload = function(eventType, item, e) {
    var self = this;

    var payload = {
        event_type: eventType,
        brand: self.wall.brand,
        campaign_id: self.wall.campaign_id,
        campaign_name: self.wall.campaign_name,
        wall_id: self.wall.wall_id,
        wall_title: self.wall.title,
        wall_subtype: self.wall.subtype,
        wall_breakpoint: self.getWallBreakpoint()
    }

    if (item) {
        payload.wall_item_id = item._id;
        payload.wall_item_type = item.type;
        if (item.cssclass) payload.wall_item_cssclass = item.cssclass;
    }

    payload = livestory_analytics.addCoordinates(payload, e, self);

    if (self.wallgroup && self.wallgroup._id) payload.wallgroup_id = self.wallgroup._id;
    if (self.wallgroup && self.wallgroup.title) payload.wallgroup_name = self.wallgroup.title;
    if (self && self.lang) payload.lang_code = self.lang;
    if (self && self.store) payload.store_code = self.store;

    return payload;
}

/**
 * Helper method to find if box has a popup (it's a post and popup_disabled is off)
 * @param {DOM} box: box html
 * @returns {Boolean} return true if box has a popup
 */
 LSWall.prototype._filterPosts = function(box) {
    return (
        !box.data('popup_disabled') &&
        !box.data('link') && (
            box.data('type') == 'photo' ||
            box.data('type') == 'video' ||
            box.data('type') == 'mixed' ||
            box.data('type') == 'text'
        )
    );
}

/**
 * Abstract method that refresh the layout after resizing or other events
 * must be implemented in specific classes
 */
LSWall.prototype.refresh = function() {
    /* To be implemented by specific wall types, especially Masonry */
}

/**
 * Method to get the current wall breakpoint
 * @returns {Number} current breakpoint in pixels. Returns -1 in case of default breakpoint
 */
LSWall.prototype.getWallBreakpoint = function() {
    var self = this;

    var wallBreakpoint = self.current_breakpoint;
    if (!wallBreakpoint) wallBreakpoint = -1;

    return wallBreakpoint;
}

/**
 * Tracking codes are passed in a global array in the wall itself
 * This method search for the tracking code given the IDs saved in the item object
 * @param {Object} item: Wall item
 */
LSWall.prototype.fillTrackingCodes = function(item) {
    var self = this;
    if (item.trackingCodes && item.trackingCodes.length > 0) {
        item.trackingCodesParsed = [];
        item.trackingCodes.forEach(function(id) {
            if (typeof self.trackingCodes[id] == 'undefined') {
                LSHelpers.error('Warning! item tracking code is not in the global array of tracking codes!', id);
                return;
            }
            item.trackingCodesParsed.push(self.trackingCodes[id]);
        });
    }
}

LSWall.prototype.detectHash = function() {
    var self = this;
    if (!self.wall.themeOptions) return;
    if (!self.wall.themeOptions.enableAnchorNavigation) return;
    if (!window.location.hash) return;

    var hash = window.location.hash;
    var destination = false;
    var found;
    var offset = self.wall.themeOptions && self.wall.themeOptions.anchorNavigationOffset || 0;

    //step 1: search for cssclass elements
    if (destination === false) {
        found = self.container.find('.' + hash.replace('#', ''));
        if (found.length > 0) {
            destination = found.eq(0).offset().top;
        }
    }

    //step 2: search for ID elements
    if (destination === false) {
        found = self.container.find(hash);
        if (found.length > 0) {
            destination = found.eq(0).offset().top;
        }
    }

    if (destination !== false) {
      $ls('html,body').animate({ scrollTop: destination - offset });
    }
}

/**
 * Handles the internal links (aka anchors) between blocks
 * @param {DOM} element: current element
 * @param {Object} item: item JSON
 */
LSWall.prototype.internalLink = function(element, item) {
    var self = this;

    var target = item.internal_link_target;
    if (typeof target === 'undefined') return;

    //offset is the "margin" from the anchor position and actual scroll position
    var offset = item.internal_link_offset || 0;

    var target_type = 'element';

    if (isNaN(target)) {
        //target is a string, but which type?
        if (target.indexOf('#') >= 0 || target.indexOf('.') >= 0 || target.indexOf('[') >= 0) {
            //target is a css selector, do nothing
        } else {
            //target is just a string, consider it a css class
            target = '.' + target;
        }
    } else {
        //target is a number, then scroll in pixel
        target_type = 'number';
        target = parseInt(target, 10);
    }

    //handle the click
    element.on('click.ls-internal-link', function() {
        var scroll = 0;
        var duration = 1000;
        if (target_type == 'number') {
            var t = target;
            if (self.ls_grid) {
                //if scaled, adjust scroll amount
                t = target * self.ls_grid.options.zoom;
            }
            scroll = element.offset().top + t;
        } else {
            var targetWithoutWallID = target.replace('#WALLID ', '');

            if (targetWithoutWallID.indexOf('.') >= 0 || targetWithoutWallID.indexOf('#') >= 0) {
                //target already contains css selector, just replace #WALLID
                target = target.replace('#WALLID ', '.fpls-' + self.wall.wall_id + ' ');
            } else {
                target = target.replace('#WALLID ', '.fpls-' + self.wall.wall_id + ' .');
            }

            var t = $ls(target);
            if (t.length > 0) scroll = t.offset().top;
        }

        if (scroll > 0) {
            duration = 500 + scroll / 2;
            duration = Math.max(duration, 500);
            duration = Math.min(duration, 2000);
            // Internal scroll based on the HTML scroll-behavior property
            // N.B. The scroll-behavior property, specified on the body element, will not propagate to the viewport.
            var htmlScrollBehavior = $ls('html').css('scroll-behavior');

            if (htmlScrollBehavior && htmlScrollBehavior.toLowerCase() == 'smooth') {
                $ls('html').scrollTop(scroll - offset);
            } else {
                $ls('html,body').animate({
                    scrollTop: scroll - offset
                }, duration);
            }
        }
    });
    element.addClass('fpls-internal-link');
}

/**
 * Method that set a11y HTML attributes
 * @param {DOM} element: the element
 * @param {Object} accessibility: item accessibility properties
 */
LSWall.prototype.setAccessibilityAttributes = function(element, accessibility) {

    if (accessibility.aria_hidden === true) element.attr('aria-hidden', accessibility.aria_hidden);
    if (accessibility.role) element.attr('role', accessibility.role);
    if (accessibility.tabindex) element.attr('tabindex', accessibility.tabindex);

}