var LSEmbedA11Y = function() {
    this.embedTpl = LSTemplates.livestory.embed_a11y;
}

LSEmbedA11Y.prototype.init = function(wallInstance, iframe, transcription, width, height) {
    var self = this;
    self.wallInstance = wallInstance;
    self.container = $ls(self.embedTpl());
    self.container.hide();
    $ls('body').append(self.container);

    self.width = width;
    self.height = height;

    if (iframe) self.setIframe(iframe);
    if (transcription) self.setTranscription(transcription);

    //attach same classes of the wall to inherit fonts, styles etc.
    var wallClasses = self.wallInstance.wrapper.attr('class');
    var captureClasses = self.container.attr('class');
    self.container.attr('class', captureClasses + ' fpls ' + wallClasses);
    self.container.find('.fpls-embed-close').on('click.ls', self.hide.bind(self));
}

LSEmbedA11Y.prototype.show = function(callback, iframe) {
    var self = this;
    self.callback = callback;
    if (iframe) self.setIframe(iframe);
    self.container.find('.fpls-embed-iframe-a11y').html(self.iframeWrapper.html());
    self.container.fadeIn();
    livestory_a11y.open();
}

LSEmbedA11Y.prototype.setTranscription = function(iframe_transcription) {
    var self = this;
    self.container.addClass('has-transcription');
    self.container.find('.fpls-transcriptions').html(iframe_transcription);
}

LSEmbedA11Y.prototype.setTitle = function(iframe_title) {
    var self = this;
    if (self.iframeWrapper) {
        var iframe = self.iframeWrapper.find('iframe');
        if (iframe) iframe.attr('title', iframe_title + " accessible version");
    }
}

LSEmbedA11Y.prototype.fitContent = function(iframeContainer) {
    var self = this;
    //reuse LSEmbed
    LSEmbed.prototype.fitContent.call(self, iframeContainer);
}

LSEmbedA11Y.prototype.setIframe = function(iframe) {
    var self = this;
    var iframe_src;
    self.iframeWrapper = $ls('<div>' + iframe + '</div>');


    //add autoplay at youtube or vimeo videos
    if (self.iframeWrapper.find('iframe')) {
        var _iframe = self.iframeWrapper.find('iframe');
        iframe_src = _iframe.attr('src');
        if (iframe_src && (iframe_src.indexOf('youtube') > 0 || 
            iframe_src.indexOf('vimeo') > 0 ||
            iframe_src.indexOf('thron') > 0)) {
            if (iframe_src.indexOf('?') > 0) {
                iframe_src += '&autoplay=1';
            } else {
                iframe_src += '?autoplay=1';
            }
            _iframe.attr('src', iframe_src);
        }
        if (iframe_src && iframe_src.indexOf('tiktok') > 0) {
            //add specific class for tiktok
            self.container.find('.fpls-embed-iframe').addClass('tiktok');
            self.container.find('.fpls-embed-iframe').attr('style', _iframe.attr('style'));
        }
    }

    if (self.iframeWrapper.find('video')) {
        var _video = self.iframeWrapper.find('video');
        _video.attr('playsinline', true);
        _video.attr('autoplay', true);
        _video.attr('controls', true);
    }
}

LSEmbedA11Y.prototype.hide = function() {
    var self = this;
    self.container.hide();
    self.container.find('.fpls-embed-iframe-a11y').html('');
    livestory_a11y.close();
    if (self.callback)
        self.callback();
}
