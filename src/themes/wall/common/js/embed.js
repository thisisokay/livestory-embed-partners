/**
 * LSEmbed
 * Class for embed popups (i.e. youtube videos opened in a modal)
 * @constructor
*/
var LSEmbed = function() {
    this.embedTpl = LSTemplates.livestory.embed;
}

/**
 * Init method for Embed popup
 * @param {String} wallInstance: Instance of the wall containing the embed
 * @param {String} iframe: HTML content of the embed window
 * @param {Number} width: width of the iframe
 * @param {Number} height: height of the iframe
 */
LSEmbed.prototype.init = function(wallInstance, iframe, width, height) {
    var self = this;
    self.wallInstance = wallInstance;
    self.container = $ls(self.embedTpl());
    self.container.hide();
    $ls('body').append(self.container);

    self.width = width;
    self.height = height;

    if (iframe) self.setIframe(iframe);

    //attach same classes of the wall to inherit fonts, styles etc.
    var wallClasses = self.wallInstance.wrapper.attr('class');
    var captureClasses = self.container.attr('class');
    self.container.attr('class', captureClasses + ' fpls ' + wallClasses);
    self.container.find('.fpls-embed-close').on('click.ls', self.hide.bind(self) );
    self.play_a11y = self.container.find('.fpls-embed-play-a11y').hide();
    self.container.find('.fpls-embed-close').on('click.ls', function(){
        self.sendAnalytics();
    })
}

/**
 * Sets the item property of the embed
 * @param {Object} item: JSON of the embed item
 */
LSEmbed.prototype.setItem = function(item) {
    self.item = item;
}

/**
 * Method to show the popup
 * @param {Function} callback: Function executed when the popup is closed
 * @param {Html} iframe: iframe html
 */
LSEmbed.prototype.show = function(callback, iframe) {
    var self = this;
    self.callback = callback;
    if (iframe) self.setIframe(iframe);
    var iframeContainer = self.container.find('.fpls-embed-iframe');
    iframeContainer.html(self.iframeWrapper.html());

    if (self.height > 0) self.fitContent(iframeContainer);

    self.container.fadeIn();
    livestory_a11y.open();
}

/**
 * Method to scale the iframe according to proportions and window size
 * @param {DOM} iframeContainer: html element that will contain the iframe
 */
LSEmbed.prototype.fitContent = function(iframeContainer) {
    var self = this;
    var w_width = $ls(window).width();
    var w_height = $ls(window).height();

    var windowWideness = w_width / w_height;
    var contentWideness = self.width / self.height;

    //baseSize is 90 to let 5% of margin. in mobile, it's 100 to make it fullscreen
    var baseSize = 90;
    if (w_width < 700) baseSize = 100;

    if (windowWideness > contentWideness) {
        //fit by height
        var height = baseSize;
        var width = height / self.height * self.width;

        iframeContainer.css({
            height: height + 'vh',
            width: width + 'vh',
            paddingTop: 0
        });
    } else {
        //fit by width
        var width = baseSize;
        if (width * w_width / 100 > 1920) {
            width = 1920 / w_width * baseSize;
        }
        var height = width / self.width * self.height;

        iframeContainer.css({
            height: height + 'vw',
            width: width + 'vw',
            paddingTop: 0
        });
    }
}

/**
 * Sets the title attribute on the iframe, for accessibility reasions
 * @param {String} iframe_title: title to set
 */
LSEmbed.prototype.setTitle = function(iframe_title) {
    var self = this;
    if (self.iframeWrapper) {
        var iframe = self.iframeWrapper.find('iframe');
        if (iframe) iframe.attr('title', iframe_title);
    }
}

/**
 * Inject iframe html into iframeWrapper property
 * @param {Html} iframe: iframe html
 */
LSEmbed.prototype.setIframe = function(iframe) {
    var self = this;
    var iframe_src;
    self.iframeWrapper = $ls('<div>' + iframe + '</div>');


    //add autoplay at youtube or vimeo videos
    if (self.iframeWrapper.find('iframe')) {
        var _iframe = self.iframeWrapper.find('iframe');
        iframe_src = _iframe.attr('src');
        if (iframe_src && (iframe_src.indexOf('youtube') > 0 || 
            iframe_src.indexOf('vimeo') > 0 ||
            iframe_src.indexOf('thron') > 0)) {
            if (iframe_src.indexOf('?') > 0) {
                iframe_src += '&autoplay=1';
            } else {
                iframe_src += '?autoplay=1';
            }
            _iframe.attr('src', iframe_src);
        }
        if (iframe_src && iframe_src.indexOf('tiktok') > 0) {
            //add specific class for tiktok
            self.container.find('.fpls-embed-iframe').addClass('tiktok');
            self.container.find('.fpls-embed-iframe').attr('style', _iframe.attr('style'));
        }
    }


    if (self.iframeWrapper.find('video')) {
        var _video = self.iframeWrapper.find('video');
        _video.attr('playsinline', true);
        _video.attr('autoplay', true);
        _video.attr('controls', true);
    }
}

/**
 * Hide popup without callbakcs
 */
LSEmbed.prototype.justHide = function() {
    var self = this;
    self.container.hide();
    self.container.find('.fpls-embed-iframe').html('');
    livestory_a11y.close();
}

/**
 * Hide popup and trigger the callback
 */
LSEmbed.prototype.hide = function() {
    this.justHide();
    if (this.callback)
        this.callback();
}

LSEmbed.prototype.setCallBackAccessible = function(callback) {
    var self = this;
    self.play_a11y.show();
    self.play_a11y.on('click.ls', self.showAccessible.bind(self));
    self.callbackAccessible = callback;
}

LSEmbed.prototype.showAccessible = function() {
    this.justHide();
    this.callbackAccessible();
}

/**
 * Method to send embed-close event
 */
LSEmbed.prototype.sendAnalytics = function() {
    var self = this;

    var payload = self.wallInstance.getAnalyticsPayload('embed-close', self.item);
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });
}

/**
 * Method to get the current wall breakpoint
 * @returns {Number} current breakpoint in pixels. Returns -1 in case of default breakpoint
 */
LSEmbed.prototype.getWallBreakpoint = function() {
    var self = this;

    var wallBreakpoint = self.wallInstance.current_breakpoint;
    if (!wallBreakpoint) wallBreakpoint = -1;

    return wallBreakpoint;
}