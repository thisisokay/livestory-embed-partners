/**
 * LSBox_asset
 * @constructor
 * class for asset boxes (products)
*/
var LSBox_asset = function() {
    this.assetTpl = LSTemplates.livestory.box_asset;
}
LSHelpers.extend(LSBox_asset, LSBox);

/**
 * Init method for Box asset
 * @param {String} item - The item object (node of the grid, or social post)
 * @param {String} resolution - The required resolution
 * @param {String} wallInstance - Instance of the wall containing the box
 */
LSBox_asset.prototype.init = function(item, wallInstance, resolution) {
    var self = this;
    LSBox.prototype.init.call(this, item, wallInstance);
    self.productLabel = 'Shop now';
    self.productLabelOOS = 'Out of stock';

    if (self.wallInstance.wall.uiLabels && self.wallInstance.wall.uiLabels.productAction) {
        self.productLabel = self.wallInstance.wall.uiLabels.productAction;
    }

    if (self.wallInstance.wall.uiLabels && self.wallInstance.wall.uiLabels.productActionOOS) {
        self.productLabelOOS = self.wallInstance.wall.uiLabels.productActionOOS;
    }

    self.renderBox(resolution);
}

/**
 * function to render the box
 * @param {String} resolution - The required resolution
 */
LSBox_asset.prototype.renderBox = function(resolution) {
    var self = this;
    LSBox.prototype.renderBox.call(self);
    self.resolution = resolution || 'medium';

    var asset = self.item;

    asset.deleted = LSHelpers.getList(asset.deleted, self.wallInstance.store, 'deleted');
    if (typeof asset.price === 'object') asset.sale_price = LSHelpers.getList(asset.price, self.wallInstance.store, 'sale_value', true);
    asset.price = LSHelpers.getList(asset.price, self.wallInstance.store, 'list_value');
    asset.name = LSHelpers.getList(asset.name, self.wallInstance.store, 'lang_value');
    asset.description = LSHelpers.getList(asset.description, self.wallInstance.store, 'lang_value');
    asset.url = LSHelpers.getList(asset.url, self.wallInstance.store, 'lang_value');

    // LST-1290
    if (asset.sale_price === asset.price) asset.sale_price = '';

    var context = $ls.extend({}, asset);

    context = self.getWallContext(context);

    context.productLabel = self.productLabel;

    //If there is a product label for OOS products, and product is not in stock, use that label
    if (!context.stock && self.productLabelOOS) context.productLabel = self.productLabelOOS;

    //Transform labels into array
    if (asset.custom_labels) {
        context.custom_labels_array = [];
        Object.keys(asset.custom_labels).forEach(function(key) {
            var label = asset.custom_labels[key];
            if (label) context.custom_labels_array.push(label);
        });
    }

    context.place_outside = self.item.assetSettings && self.item.assetSettings.place_outside;
    context.alignment = self.item.assetSettings && self.item.assetSettings.alignment;

    context.outofstock = !context.stock;

    //set these as default:
    context.show_image = true;
    context.show_price = true;
    context.show_title = true;
    context.show_cta = true;

    if (self.item.assetSettings) {
        context.show_image  = !self.item.assetSettings.hide_image;    
        context.show_price  = !self.item.assetSettings.hide_price;
        context.show_title  = !self.item.assetSettings.hide_title;
        context.show_cta    = !self.item.assetSettings.hide_cta;
    }

    context.showprice = true;

    if (context.price) {
        if (parseFloat(context.price.replace(/[^\d.,-]/g, '')) == 0 || 
            isNaN(parseFloat(context.price.replace(/[^\d.,-]/g, '')))) {
            context.showprice = false;
        }
    }

    //set always in stock if user wants to show out of stock produts.
    if (!self.wallInstance.wall.layout.hideoutofstock) context.stock = true;

    //If product is deleted, consider it as out of stock
    //if (asset.deleted) context.stock = false;

    context.url = self.wallInstance.addUtmToUrl(context.url, null, context);

    //override product URL if block has its own link.
    if (self.item.link) {
        context.url = self.item.link;
    }

    if (context.link) context.link = self.wallInstance.addUtmToUrl(context.link);

    context.showOverlay = context.stock && !context.place_outside;

    //add anchor tag to the flex bottom only if title and/or price are true
    context.show_flex_bottom_url = !context.show_image && (context.stock && context.show_title || context.stock && context.show_price);

    context.image_url = LSHelpers.getImagePath(self.item, self.resolution);
    context.image_responsive_url = LSHelpers.getImageSrcSet(self.item, self.resolution);

    var assetTpl = self.assetTpl(context);

    // LST-2987 (lazy loading)
    if (self.wallInstance.wall.themeOptions && self.wallInstance.wall.themeOptions.lazyLoad && window.IntersectionObserver) {
        assetTpl = LSHelpers.replaceLazyAttr(assetTpl);
    }

    self.box = self.getHtml(assetTpl);

    if (self.item.padding) self.box.find('.fpls-box-flex-bottom').css({ padding: self.item.padding + 'px' });
    if (context.alignment) self.box.find('.fpls-box-flex-bottom').css({ textAlign: context.alignment });
    if (self.item.focalpoint) self.box.addClass('fpls-focal--' + self.item.focalpoint);
    if (self.item.object_fit) self.box.addClass('fpls-object-fit--' + self.item.object_fit);

    //ANALYTICS: product-impression
    var payload = self.getAnalyticsPayload('product-impression');
    payload.product_sku = context.sku;
    payload.product_price = context.price;
    payload.product_name = context.name;
    if (context.custom_labels) {
        if (context.custom_labels.custom_label_0 && context.custom_labels.custom_label_0.length > 0) payload.product_custom_label_0 = context.custom_labels.custom_label_0;
        if (context.custom_labels.custom_label_1 && context.custom_labels.custom_label_1.length > 0) payload.product_custom_label_1 = context.custom_labels.custom_label_1;
        if (context.custom_labels.custom_label_2 && context.custom_labels.custom_label_2.length > 0) payload.product_custom_label_2 = context.custom_labels.custom_label_2;
        if (context.custom_labels.custom_label_3 && context.custom_labels.custom_label_3.length > 0) payload.product_custom_label_3 = context.custom_labels.custom_label_3;
        if (context.custom_labels.custom_label_4 && context.custom_labels.custom_label_4.length > 0) payload.product_custom_label_4 = context.custom_labels.custom_label_4;
    }

    if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });

    //Support enter key
    self.box.on('keydown.ls', function(e) {
        if  (e.which == 32 || e.which == 13) {
            self.box.find('a').trigger('click.ls');
            e.preventDefault();
            e.stopPropagation();
        }
    });

    self.detectClick();
    self.postRender();
}

/**
 * Detect interaction events on product tiles
 */
LSBox_asset.prototype.detectClick = function() {
    var self = this;

    self.box.find('a[data-sku]').each(function() {

        var a = $ls(this);
        var _timer;

        a.on('mouseenter.ls', function(e) {
            clearTimeout(_timer);
            _timer = setTimeout(function() {

                //ANALYTICS: product-hover
                var payload = self.getAnalyticsPayload('product-hover', e);
                payload.product_sku = a.data('sku');
                payload.product_price = a.data('price');
                payload.product_name = a.data('name');
                self.getCustomLabelPayload(a, payload);

                if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
                livestory_analytics.pushEvent(payload, {
                    item: self.item,
                    wall: self.wallInstance.wall,
                    wallgroup: self.wallInstance.wallgroup
                });
            }, 100);
        });

        a.on('mouseleave.ls', function() {
            clearTimeout(_timer);
        });

        a.on('click.ls', function(e) {
            var dest = a.attr('href');
            var target = a.attr('target');

            //ANALYTICS: product-click
            var payload = self.getAnalyticsPayload('product-click', e);
            payload.product_sku = a.data('sku');
            payload.product_price = a.data('price');
            payload.product_name = a.data('name');
            self.getCustomLabelPayload(a, payload);

            if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        });
    });
}

/**
 * Method to load product custom lables for the analytics payload
 * @param {DOM element} product: product element
 * @param {Object} payload: analytics product payload
 */
 LSBox_asset.prototype.getCustomLabelPayload = function(product, payload) {

    if (product.data('custom-label-0')) payload.product_custom_label_0 = product.data('custom-label-0');
    if (product.data('custom-label-1')) payload.product_custom_label_1 = product.data('custom-label-1');
    if (product.data('custom-label-2')) payload.product_custom_label_2 = product.data('custom-label-2');
    if (product.data('custom-label-3')) payload.product_custom_label_3 = product.data('custom-label-3');
    if (product.data('custom-label-4')) payload.product_custom_label_4 = product.data('custom-label-4');
    return payload;

}