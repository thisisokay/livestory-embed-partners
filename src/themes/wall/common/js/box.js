/**
 * LSBox
 * @constructor
 * abstract class for any box of content (image, text, video, product, iframe etc.)
 * it's never used directly, but it is extended by specific classes
*/
var LSBox = function() {
    this.uniqueId = '';
    this.isSSR = false;
}

/**
 * Init method for Box
 * @param {String} item - The item object (node of the grid, or social post)
 * @param {String} wallInstance - Instance of the wall containing the box
 */
LSBox.prototype.init = function(item, wallInstance) {
    var self = this;
    self.item = item;
    self.wallInstance = wallInstance;
    self.uniqueId = item.uniqueId;
    self.isSSR = self.wallInstance.isSSR;

    //fix array link
    if (self.item.link && self.item.link.length == 0) {
        self.item.link = "";
    }

    //fetch link from link_asset if present
    if (!self.item.link && self.item.link_asset) {
        var asset = wallInstance.products[self.item.link_asset];
        if (asset) self.item.link = asset.url;
    }

    self.post_id = self.item.id || self.item._id;
    self.fillTrackingCodes(item);
}

/**
 * function to render the box. It is implemented in the specific classes
 */
LSBox.prototype.renderBox = function() {
    var self = this;
}

/**
 * function called after the box is rendered.
 */
LSBox.prototype.postRender = function() {
    var self = this;

    if (
        self.item &&
        !self.item.link &&
        self.item.internal_link
    ) {
        self.wallInstance.internalLink(self.box, self.item);
    }

    self.wallItemVisible();
    self.getCommonBoxEvents();
}

/**
 * listen to all the generic events of any box, in order to trigger tracking
 */
LSBox.prototype.getCommonBoxEvents = function() {
    var self = this;

    self.box.on('click.ls', function(e){
        var payload = self.getAnalyticsPayload('wall-item-click', e);
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    });

    var _timer;
    self.box.on('mouseenter.ls', function(e){
        clearTimeout(_timer);
        _timer = setTimeout(function() {
            var payload = self.getAnalyticsPayload('wall-item-hover', e);
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        }, 100);
    });

    self.box.on('mouseleave.ls', function() {
        clearTimeout(_timer);
    });

    // var _move_timer;
    // self.box.on('mousemove.ls', function(e){
    //     clearTimeout(_move_timer);
    //     e.stopPropagation();

    //      _move_timer = setTimeout(function() {
    //         var payload = self.getAnalyticsPayload('wall-item-pause', e);
    //         livestory_analytics.pushEvent(payload, {
    //             item: self.item,
    //             wall: self.wallInstance.wall,
    //             wallgroup: self.wallInstance.wallgroup
    //         });
    //     }, 250);
    // });

    self.box.find('a[href]').not('[data-sku]').each(function() {

        var a = $ls(this);

        a.on('click.ls', function(e){
            var payload = self.getAnalyticsPayload('wall-link-click', e);

            if (self.item.type === 'block' && self.item.text) payload.wall_link_text = self.item.text.replace(/(<([^>]+)>)/gi, "")
            payload.wall_link_url = self.getLinkUrl();
            payload.wall_link_type = self.getLinkType();

            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        })

        var _timer;
        a.on('mouseenter.ls', function(e){
            clearTimeout(_timer);
            _timer = setTimeout(function() {
                var payload = self.getAnalyticsPayload('wall-link-hover', e);

                if (self.item.type === 'block' && self.item.text) payload.wall_link_text = self.item.text.replace(/(<([^>]+)>)/gi, "")
                payload.wall_link_url = self.getLinkUrl();
                payload.wall_link_type = self.getLinkType();

                livestory_analytics.pushEvent(payload, {
                    item: self.item,
                    wall: self.wallInstance.wall,
                    wallgroup: self.wallInstance.wallgroup
                });
            }, 100);
        })
    });

    //layout item impression
    var payload = self.getAnalyticsPayload('wall-item-impression');
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });

    // wallItemVisbile
    self.boxObserver.observe(self.box[0]);

}

/**
 * Method to get the link from the box, looking within the text or the item.link property itself
 * @returns {String} link URL
 */
LSBox.prototype.getLinkUrl = function() {
    var self = this;

    var linkUrl;

    if (self.item.type === 'block' && self.item.text && self.item.text.indexOf('href=') !== -1) linkUrl = self.item.text.match(/href="([^"]*)/)[1];
    if (self.item.link) linkUrl = self.item.link.split('?')[0];

    return linkUrl;
}

/**
 * Method to get the type of the link
 * If link is inside the text (added within TinyMCE) it returns html
 * @returns {String} link type (block or html)
 */
LSBox.prototype.getLinkType = function() {
    var self = this;

    var linkType;

    if (self.item.type === 'block' && self.item.text && self.item.text.indexOf('href=') !== -1) linkType = 'html';
    if (self.item.link) linkType = 'block';

    return linkType
}

/**
 * Method to get the current wall breakpoint
 * @returns {Number} current breakpoint in pixels. Returns -1 in case of default breakpoint
 */
LSBox.prototype.getWallBreakpoint = function() {
    var self = this;

    var wallBreakpoint = self.wallInstance.current_breakpoint;
    if (!wallBreakpoint) wallBreakpoint = -1;

    return wallBreakpoint;
}

/**
 * Method called on box load
 * @param {function} callback: function called when the box is loaded
 * callback is called with the width and height of the content (if image or video)
 */
LSBox.prototype.onLoad = function(callback) {

    var self = this;

    //fix srcsets for IE
    if (window.document.documentMode) {
        self.box.find('[data-src]').each(function() {
            var _img = $ls(this);
            var _src = _img.attr('data-src');
            _img.attr('src', _src);
        });
    }

    var el = self.box.find('img').eq(0);

    if (el.length < 1) el = self.box.find('video').eq(0);

    //check 1: box does not contain images
    if (el.length < 1) {
        return setTimeout(function() {
            callback(0, 0, false);
            LSHelpers.phantomTimeout();
        }, 0);
    }

    //attach error anyway
    el.on('error', function() {
        LSHelpers.log('error');
        callback(0,0,true);
    });

    //listen to errors in source as well
    if (el.is('video')) el.find('source').on('error', function() {
        var source = $ls(this);
        if (!source.attr('lazy-src')) {
            LSHelpers.log('error');
            callback(0,0,true);
        }
    });

    var width = 0;
    var height = 0;
    var _el = el[0];
    var prop = 'natural';
    var event = 'load';
    if (el.is('video')) {
        prop = 'video';
        event = 'loadedmetadata';
    }

    //check 2: image is already loaded and have width / height, just call the callback
    width = _el[prop+'Width'];
    height = _el[prop+'Height'];

    if (width > 0 && height > 0) {
        self.w = width;
        self.h = height;
        return setTimeout(function() {
            callback(width, height, false);
            LSHelpers.phantomTimeout();
        }, 0);
    }

    //check 3: image is not loaded but have data width / data height in html, so we can call the callback
    el.one(event, function() {
        width = _el[prop+'Width'];
        height = _el[prop+'Height'];
        self.w = width;
        self.h = height;
        return setTimeout(function() {
            callback(width, height, false);
            LSHelpers.phantomTimeout();
        }, 0);
    });
}

/**
 * Method to get all the analytics common values, such as wall id, campaign id etc.
 * @param {String} eventType: type of the event
 * @param {Event} e: native event object
 * @returns {Object} analytics payload
 */
LSBox.prototype.getAnalyticsPayload = function(eventType, e) {
    var self = this;

    var payload = {
        event_type: eventType,
        brand: self.item.brand,
        campaign_id: self.wallInstance.wall.campaign_id,
        campaign_name: self.wallInstance.wall.campaign_name,
        wall_id: self.wallInstance.wall.wall_id,
        wall_title: self.wallInstance.wall.title,
        wall_subtype: self.wallInstance.wall.subtype,
        wall_item_id: self.item._id,
        wall_item_type: self.item.type,
        wall_breakpoint: self.getWallBreakpoint()
    }

    payload = livestory_analytics.addCoordinates(payload, e, self.wallInstance);

    if (self.wallInstance.wall.subtype == 'strip' || self.wallInstance.wall.subtype == 'grid') payload.wall_slide_number = self.getBoxIndex();
    if (self.wallInstance.wallgroup && self.wallInstance.wallgroup._id) payload.wallgroup_id = self.wallInstance.wallgroup._id;
    if (self.wallInstance.wallgroup && self.wallInstance.wallgroup.title) payload.wallgroup_name = self.wallInstance.wallgroup.title;
    if (self.item && self.item.cssclass) payload.wall_item_cssclass = self.item.cssclass;

    var tree = "";
    if (self.item && self.item.cssclass_tree) tree = self.item.cssclass_tree.join(',');
    if (tree != "") payload.wall_item_cssclass_tree = tree;

    if (self.wallInstance && self.wallInstance.lang) payload.lang_code = self.wallInstance.lang;
    if (self.wallInstance && self.wallInstance.store) payload.store_code = self.wallInstance.store;

    return payload;
}

/**
 * Method to get the html of the box, either to append it of (in case of SSR) to reuse the server version of it
 * @param {html} tpl: html of the template (optional)
 * @returns {jQuery DOM element} html of the box
 */
LSBox.prototype.getHtml = function(tpl) {
    var self = this;
    var found = false;

    if (self.isSSR) {
        //if SSR is active search for box in the server side html
        self.box = self.wallInstance.wrapper.find('#'  + self.uniqueId);

        //Update the html of text elements
        if (tpl) {
            var updatedTemplate = $ls(tpl);
            if (self.item.type == 'photo') {
                var img = updatedTemplate.find('.fpls-img');
                var ssrImg = self.box.find('.fpls-img');
                //update image attributes
                var attributesToReplace = ["src", "lazy-src", "srcset", "lazy-srcset", "sizes"];
                attributesToReplace.forEach(function(attr) {
                    if (ssrImg.attr(attr)) ssrImg.attr(attr,  img.attr(attr));
                });
            }

            if (self.item.type == 'asset') {
                var html1 = updatedTemplate.find('.fpls-box-overlay-content').html();
                self.box.find('.fpls-box-overlay-content').html(html1);

                var html2 = updatedTemplate.find('.fpls-box-caption').html();
                self.box.find('.fpls-box-caption').html(html2);

                var html3 = updatedTemplate.find('.fpls-box-flex-bottom').html();
                self.box.find('.fpls-box-flex-bottom').html(html3);

                var productLink = updatedTemplate.find('.fpls-box-flex-top > a');
                var ssrProductLink = self.box.find('.fpls-box-flex-top > a');

                if (productLink.length > 0 && ssrProductLink.length > 0) {
                    ssrProductLink.attr('href', productLink.attr('href'));
                    ssrProductLink.attr('data-sku', productLink.attr('data-sku'));
                    ssrProductLink.attr('data-name', productLink.attr('data-name'));
                    ssrProductLink.attr('data-price', productLink.attr('data-price'));
                    ssrProductLink.attr('data-custom-label-0', productLink.attr('data-custom-label-0'));
                    ssrProductLink.attr('data-custom-label-1', productLink.attr('data-custom-label-1'));
                    ssrProductLink.attr('data-custom-label-2', productLink.attr('data-custom-label-2'));
                    ssrProductLink.attr('data-custom-label-3', productLink.attr('data-custom-label-3'));
                    ssrProductLink.attr('data-custom-label-4', productLink.attr('data-custom-label-4'));
                }
            }

            if (self.item.type == 'block') {
                //update text anyway
                var html = updatedTemplate.find('.fpls-box-inner').html();
                self.box.find('.fpls-box-inner').html(html);
            }

            //update direct child link
            var a = updatedTemplate.find('> a');
            var ssrA = self.box.find('> a');
            if (a.length > 0 && ssrA.length > 0) ssrA.attr('href', a.attr('href'));

            //update inner box link
            a = updatedTemplate.find('a.fpls-box-inner');
            ssrA = self.box.find('a.fpls-box-inner');
            if (a.length > 0 && ssrA.length > 0) ssrA.attr('href', a.attr('href'));
        }


        if (self.box.length > 0) {
            found = true;
        } else {
            self.isSSR = false;
            LSHelpers.debug('SSR element not found', '#' + self.uniqueId);
        }
    }
    if (tpl && !found) {
        self.box = $ls(tpl);
        self.box.attr('id', self.uniqueId); //assign an unique ID to the element
    }
    
    if (found) self.box.data('ssr-found', true);

    //Add loading lazy to image or videos, but just in customgrids
    // if (self.wallInstance.wall.subtype == 'customgrid') {
    //     self.box.find('img').attr('loading', 'lazy');
    //     self.box.find('video').attr('loading', 'lazy');
    // }

    return self.box;
}

/**
 * Tracking codes are passed in a global array in the wall itself
 * This method search for the tracking code given the IDs saved in the item object
 * @param {Object} item: Wall item
 */
LSBox.prototype.fillTrackingCodes = function(item) {
    var self = this;
    self.wallInstance.fillTrackingCodes(item);
}

/**
 * Method to set the box square css classes (used by strip or grid layouts)
 */
 LSBox.prototype.setSquare = function() {
    var self = this;
    if (self.w && self.h) {
        self.box.addClass('fpls-square');
        if (self.w > self.h) {
            self.box.addClass('fpls-landscape');
        } else {
            self.box.addClass('fpls-portrait');
        }
    }
}

/**
 * Get box index in case of grid or strip layouts
 * @returns {Number} box index
 */
LSBox.prototype.getBoxIndex = function() {

    var self = this;

    var boxIndex;

    switch (self.wallInstance.wall.subtype) {
        case 'strip':
            var stripBoxes = self.wallInstance.intContainer.find('.fpls-box:not(.fpls-box-broken)');
            boxIndex = stripBoxes.index(self.box) + 1;
        break;
        case 'grid':
            var gridBoxes = self.wallInstance.isotopeInstance && self.wallInstance.isotopeInstance.getFilteredItemElements();
            if (gridBoxes) boxIndex = $ls(gridBoxes).index(self.box) + 1;
        break;
    }

    return boxIndex;

}

// /**
//  * Method to get the box visibility for the wall-item-visible event
//  * @param {Event} e: native event object
//  *
LSBox.prototype.wallItemVisible = function() {

    var self = this;

    var config = {
        root: null,
        rootMargin: '0px',
        threshold: 0
    }

    self.boxObserver = LSHelpers.createObserver(function(entry) {

        var payload = self.getAnalyticsPayload('wall-item-visible', entry);
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });

    }, config);

}

/**
 * Method that enable countdown
 */
LSBox.prototype.initCountdown = function() {
    var self = this;

    if (
		self.box.find('[data-ls_countdown], .ls-countdown') &&
		self.box.find('[data-ls_countdown], .ls-countdown').length > 0
	) {
        LSHelpers.appendJs(LSConfig.assetURL + '/js/countdown.js', function() {
            self.box.find('[data-ls_countdown], .ls-countdown').each(function() {
                var el = $ls(this);
                var html = el.html();

                setTimeout(function() {
                    $ls('body').addClass('fpls-countdown-initialized');
                }, 750);

                var endString = el.data('ls_countdown');

                if (self.wallInstance.wallgroup && (self.wallInstance.wallgroup.theme.indexOf('countdown') > 0 || !endString)) {
                    if (self.wallInstance.wallgroup.ds_schedule && self.wallInstance.wallgroup.ds_schedule.length > 0) { //take end date from new schedulation, if present
                        endString = self.findClosestDate(self.wallInstance.wallgroup.ds_schedule).d_to;
                    } else if (self.wallInstance.wall.schedule && self.wallInstance.wall.schedule.schedule_end_date) { //take end date from schedulation, if present
                        endString = self.wallInstance.wall.schedule.schedule_end_date;
                    }
                }

                var end = moment(endString);

                el.countdown(end.toDate(), function(event) {
                    if (self.wallInstance.wall.brand == 'santoni') {
                        setTimeout(function() {
                            el.html(event.strftime(html));
                        }, 1);
                    } else {
                        el.html(event.strftime(html));
                    }
                }).on('finish.countdown', function() {
                    //trigger end of scheduling
                    self.wallInstance.mainContainer.trigger('ls-countdown-end');
                });
            });
        });
    }
}

/**
 * Method that find the closest date
 * @param {Array} schedules: array with schedules data
 * @returns {Object} the schedule with closest date
 */
LSBox.prototype.findClosestDate = function(schedules) {
	var now = new Date();

	var closestObj = null;
	var closestDiff = Infinity;

	for (var i = 0; i < schedules.length; i++) {
	  var date = new Date(schedules[i].d_to);
	  var diff = Math.abs(date - now);

	  if (diff < closestDiff) {
		closestDiff = diff;
		closestObj = schedules[i];
	  }
	}

	return closestObj;
}

/**
 * Method that get custom options and update the context
 * @param {Object} context: additional context will be added here
 * @returns {Object} context
 */
LSBox.prototype.getWallContext = function(context) {
    var self = this;

    context.wall = self.wallInstance.context;

	return context;
}