/**
 * LSBox_icon
 * @constructor
 * class for icon boxes (icon elements)
*/
var LSBox_icon = function() {
    this.iconTpl = LSTemplates.livestory.box_icon;
}
LSHelpers.extend(LSBox_icon, LSBox);

/**
 * Init method for Box icon
 * @param {String} item - The item object
 * @param {String} wallInstance - Instance of the wall containing the box
 */
LSBox_icon.prototype.init = function(item, wallInstance) {
    var self = this;

    LSBox.prototype.init.call(this, item, wallInstance);

    self.renderBox();
}

/**
 * Function to render the box
 */
LSBox_icon.prototype.renderBox = function() {
    var self = this;
    var context = self.item;

    context.link = self.item.link;
    if (context.link) context.link = self.wallInstance.addUtmToUrl(context.link);
    if (self.item.svg) context.svg = self.item.svg;

    self.box = self.getHtml(self.iconTpl(context));

    self.postRender();
}

/**
 * Method called after the box is rendered
 */
LSBox_icon.prototype.postRender = function() {
    var self = this;
    LSBox.prototype.postRender.call(this);

    self.injectSvgStyle(self.item.uniqueId, self.item.style);
}

/**
 * Inject inline CSS and replace placeholders
 * @param {String} itemId: item id for inline css
 * @param {String} svgStyle: the css to inject
 */
LSBox_icon.prototype.injectSvgStyle = function(itemId, svgStyle) {
    var self = this;

    var css = "";

    if (!svgStyle) return;

    css = svgStyle && svgStyle.replace(/ITEMID/g, itemId);

    if (css && css.length > 0) LSHelpers.injectInlineCss(css, self.wallInstance.wall.wall_id, self.wallInstance.container);
}