/**
 * LSBox_embed
 * @constructor
 * class for embed boxes (youtube, videos, iframe)
*/
var LSBox_embed = function() {
    this.embedTpl = LSTemplates.livestory.box_embed;
    this.LSEmbed = LSEmbed;
    this.LSEmbedA11Y = LSEmbedA11Y;
}
LSHelpers.extend(LSBox_embed, LSBox);

/**
 * Init method for Box embed
 * @param {String} item - The item object
 * @param {String} wallInstance - Instance of the wall containing the box
 * @param {String} resolution - The required resolution
 */
LSBox_embed.prototype.init = function(item, wallInstance, resolution) {
    var self = this;
    LSBox.prototype.init.call(this, item, wallInstance);
    self.renderBox(resolution);
}

/**
 * function to render the box
 * @param {String} resolution - The required resolution
 */
LSBox_embed.prototype.renderBox = function(resolution) {
    var self = this;
    var context = self.item;

    // Thron CDN China switch
    context = livestory_thron.fixThronChina(context);

    self.resolution = resolution || 2000;

    context.is_accessible = self.isAccessible(self.item);
    context.poster_url = LSHelpers.getImagePath(self.item, self.resolution);
    context.poster_responsive_url = LSHelpers.getImageSrcSet(self.item, self.resolution);

    if (context.iframeHeight > 0) context.proportions = context.iframeHeight / context.iframeWidth * 100;

    var embedTpl = self.embedTpl(context);

    // LST-2987 (lazy loading)
    if (self.wallInstance.wall.themeOptions && self.wallInstance.wall.themeOptions.lazyLoad && window.IntersectionObserver) {
        embedTpl = LSHelpers.replaceLazyAttr(embedTpl);
    }

    self.box = self.getHtml(embedTpl);

    var boxInner = self.box.find('.fpls-box-inner');
    var poster = self.box.find('img');
    var iframewrapper = self.box.find('.fpls-box-iframe-html');
    var iframediv = $ls('<div>' + self.item.iframe + '</div>');
    var iframetag = iframediv.find('iframe');
    var videotag = iframediv.find('video');
    var iframe_src = iframetag.attr('src');

    //scale iframe to full box
    iframetag.attr('width', '100%').attr('height', '100%');

    //add accessibility title to iframe
    iframediv.find('iframe,video').attr('title', context.accessibility_caption);

    var videoButtons = self.box.find('.fpls-playing-video-buttons').hide();
    var close = self.box.find('.fpls-box-close').hide();
    var play = self.box.find('.fpls-box-play');
    var play_a11y = self.box.find('.fpls-box-play-a11y');
    play_a11y.hide();

    if (context.is_accessible) {
        play_a11y.show();
        self.embedAccessible = new self.LSEmbedA11Y();
        self.embedAccessible.init(self.wallInstance, context.iframe_accessible, context.iframe_transcription, self.item.iframeWidth, self.item.iframeHeight);
        self.embedAccessible.setTitle(context.accessibility_caption);
    }

    self.box.find('img').on('click.ls', function(e) {
        e.preventDefault();
        play.trigger('click.ls');
    });

    if (self.item.playinmodal || ($ls(window).outerWidth() < 768 && !self.item.inline_iframe)) {
        iframewrapper.hide();
        var videoButtons = self.box.find('.fpls-playing-video-buttons').hide();
        self.embed = new self.LSEmbed();
        self.embed.init(self.wallInstance, self.item.iframe, self.item.iframeWidth, self.item.iframeHeight);
        self.embed.setItem(self.item);
        self.embed.setTitle(context.accessibility_caption);
        iframewrapper.html('');
        play.on('click.ls', function(e) {
            if (context.is_accessible) {
                self.embed.setCallBackAccessible(function() {
                    play_a11y.trigger('click.ls');
                });

            }

            self.embed.show();

            //ANALYTICS: wall-video-play
            if (self.isVideo()) {

                var payload = self.getAnalyticsPayload('wall-video-play', e);
                payload.wall_video_source = self.getVideoSource();
                livestory_analytics.pushEvent(payload, {
                    item: self.item,
                    wall: self.wallInstance.wall,
                    wallgroup: self.wallInstance.wallgroup
                });
            }

        });
    } else if (!self.item.inline_iframe) {
        //add autoplay at youtube or vimeo videos

        var iframe_src = iframetag.attr('src');
        if (iframe_src && (iframe_src.indexOf('youtube') > 0 || 
            iframe_src.indexOf('vimeo') > 0 ||
            iframe_src.indexOf('thron') > 0)) {
            if (iframe_src.indexOf('?') > 0) {
                iframe_src += '&autoplay=1';
            } else {
                iframe_src += '?autoplay=1';
            }
            iframetag.attr('src', iframe_src);
        }

        //add wmode to youtube videos
        if (iframe_src && (iframe_src.indexOf('youtube') > 0)) {
            if (iframe_src.indexOf('?') > 0) {
                iframe_src += '&wmode=transparent';
            } else {
                iframe_src += '?wmode=transparent';
            }
            iframe_src += '&rel=0';
            iframetag.attr('src', iframe_src);
            iframetag.attr('wmode', 'transparent');
        }

        //add playsinline to html5 videos, to avoid full-screen on mobile
        if (videotag.length > 0) {
            videotag.attr('playsinline', true);
            videotag.attr('autoplay', true);
        }

        //cache iframe html
        var iframehtml = iframediv.html();
        iframewrapper.hide();
        iframewrapper.html('');

        play.on('click.ls', function(e) {
            if ($ls(e.target).parents('.fpls-box-iframe-html').length <= 0) {
                iframewrapper.html(iframehtml).show();
                boxInner.on('mouseover.ls', function(){
                    close.show();
                })
                boxInner.on('mouseout.ls', function(){
                    close.hide();
                })

                //ANALYTICS: wall-video-play
                if (self.isVideo()) {

                    var payload = self.getAnalyticsPayload('wall-video-play', e);
                    payload.wall_video_source = self.getVideoSource();
                    livestory_analytics.pushEvent(payload, {
                        item: self.item,
                        wall: self.wallInstance.wall,
                        wallgroup: self.wallInstance.wallgroup
                    });

                }
            }
            videoButtons.show();
        });

        close.on('click.ls', function(e) {
            e.preventDefault();
            e.stopPropagation();
            iframewrapper.html('').hide();
            videoButtons.hide();
            boxInner.off('mouseover.ls mouseout.ls');

            //ANALYTICS: embed-close
            var payload = self.getAnalyticsPayload('embed-close', e);
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        });

        //stop videos when switching tabs
        self.wallInstance.mainContainer.on('switch.tab.ls', function() {
            iframewrapper.html('').hide();
            videoButtons.hide();
        });
    } else {
        iframewrapper.css('background', 'none');
        
        if (!self.isSSR) {
            var iframehtml = iframediv.html();
            iframewrapper.html(iframehtml);
        }

        if (context.is_accessible) {
            videoButtons.show();
        }

        //stop videos when switching tabs (re-render them)
        self.wallInstance.mainContainer.on('switch.tab.ls', function() {
           if (iframehtml) {
               iframewrapper.html(''); 
               iframewrapper.html(iframehtml); 
           }
       });
    }

    //ANALYTICS: embed-a11y-click
    if (context.is_accessible && !self.item.inline_iframe) {
        play_a11y.on('click.ls', function(e) {
            iframewrapper.html('').hide();
            videoButtons.hide();
            self.embedAccessible.show();

            var payload = self.getAnalyticsPayload('embed-a11y-click', e);
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        });
    } else if (context.is_accessible) {
        play_a11y.on('click.ls', function(e) {
            var iframehtml = iframediv.html();
            iframewrapper.html(iframehtml);
            self.embedAccessible.show();

            var payload = self.getAnalyticsPayload('embed-a11y-click', e);
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        });
    }

    play.on('keydown.ls', function(e) {
        if ($ls(e.target).is('button,a')) return;
        if (e.which == 32 || e.which == 13) {
            play.trigger('click.ls');
            e.preventDefault();
            e.stopPropagation();
        }
    });

    //ANALYTICS: embed-impression
    var payload = self.getAnalyticsPayload('embed-impression');
    if (self.isVideo()) payload.wall_video_source = self.getVideoSource();
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });

    //ANALYTICS: embed-hover
    var _timer;
    self.box.on('mouseenter.ls', function(e){
        clearTimeout(_timer);
        _timer = setTimeout(function() {
            var payload = self.getAnalyticsPayload('embed-hover', e);
            if (self.isVideo()) payload.wall_video_source = self.getVideoSource();
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        }, 100);
    });

    //ANALYTICS: embed-click
    self.box.on('click.ls', function(e){
        var payload = self.getAnalyticsPayload('embed-click', e);
        if (self.isVideo()) payload.wall_video_source = self.getVideoSource();
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    });

    self.postRender();
}

/**
 * function to get video source (youtube, vimeo, thron)
 * @returns {String} video source
 */
LSBox_embed.prototype.getVideoSource = function() {
    var self = this;

    var source;
    var iframe = self.item.iframe;
    var pattern = /src="([^"]+)"/
    var match = pattern.exec(iframe);
    var videoUrl = match[1];

    if (videoUrl && videoUrl.indexOf('youtube') !== -1) source = 'youtube';
    if (videoUrl && videoUrl.indexOf('vimeo') !== -1) source = 'vimeo';
    if (videoUrl && videoUrl.indexOf('thron') !== -1) source = 'thron';

    return source;
}

/**
 * function to determine if video is accessible
 * @params {Object} item: the layout item
 * @returns {boolean} if video is accessible or not
 */
LSBox_embed.prototype.isAccessible = function(item) {
    if (typeof item == 'undefined') item = this.item;
    if (item.iframe_accessible != undefined && item.iframe_accessible != "") {
        return (item.iframe_transcription != undefined && item.iframe_transcription != "")
        || (item.iframe_accessible != undefined && item.iframe_accessible != "")
        || (item.iframe_transcription != undefined && item.iframe_transcription != "");
    }
    return false;
}

/**
 * function to determine if embed is a video
 * @returns {boolean} if embed is a video
 */
LSBox_embed.prototype.isVideo = function() {
    var self = this;
    return self.item.iframe && (
        self.item.iframe.indexOf('youtube') > 0 || 
        self.item.iframe.indexOf('vimeo') > 0 || 
        self.item.iframe.indexOf('thron') > 0 ||
        self.item.iframe.indexOf('<video') > 0 //added support for html5-native video tags
    )
}