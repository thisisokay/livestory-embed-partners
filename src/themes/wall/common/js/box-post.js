/**
 * LSBox_post
 * @constructor
 * class for post boxes, i.e. UGC videos/photos, manual videos/photos and UGC text
*/
var LSBox_post = function() {
    this.photoTpl = LSTemplates.livestory.box_post_photo; //template for photo elements
    this.videoTpl = LSTemplates.livestory.box_post_video; //template for video elements
    this.textTpl = LSTemplates.livestory.box_post_text; //template for text elements (used only by twitter/tumblr)
}
LSHelpers.extend(LSBox_post, LSBox);

/**
 * Init method for Box post
 * @param {String} item - The item object (node of the grid, or social post)
 * @param {String} resolution - The required resolution, either in pixel or in instagram sizes (small, medium, large)
 * @param {String} wallInstance - Instance of the wall containing the box
 * @param {boolean} restrain_y - Resize by giving the height instead of the width (for strips, basically)
 */
LSBox_post.prototype.init = function(item, wallInstance, resolution, restrain_y) {
    var self = this;
    LSBox.prototype.init.call(self, item, wallInstance);
    if (self.item.text)
        self.boxViewAriaLabel = 'View post ' + self.item.text.substring(0, 10);

    self.boxViewLabel = 'View';

    if (self.wallInstance.wall.layout && self.wallInstance.wall.uiLabels && self.wallInstance.wall.uiLabels.boxView) {
        self.boxViewLabel = self.wallInstance.wall.uiLabels.boxView;
    }

    //if pins in box, enable popup
    if (self.item.show_pins) {
        //self.item.popup_disabled = false;
    }

    self.restrain_y = restrain_y;
    self.renderBox(resolution);
}

/**
 * function to render the box
 * according to post type (photo, video, text), it will call specific render methods
 * @param {String} resolution - The required resolution, either in pixel or in instagram sizes (small, medium, large)
 */
LSBox_post.prototype.renderBox = function(resolution) {
    var self = this;

    LSBox.prototype.renderBox.call(self);
    self.resolution = resolution || 'medium';
    if (self.item.type == 'photo') self.renderPhoto();
    if (self.item.type == 'video') self.renderVideo();
    if (self.item.type == 'text') self.renderText();
    if (self.item.type == 'mixed') {
        if (self.item.medias && self.item.medias[0]) {
            var firstItem = self.item.medias[0];
            if (firstItem.type == 'photo') self.renderPhoto();
            if (firstItem.type == 'video') self.renderVideo();
        } else {
            if (self.item.pictures.length > 0) self.renderPhoto();
            if (self.item.pictures.length == 0) self.renderVideo();
        }
    }

    //Add focal point (to align image inside the box)
    if (self.item && self.item.focalpoint) {
        self.box.addClass('fpls-focal--' + self.item.focalpoint);
    }

    //Add image/video object-fit property
    if (
        self.item &&
        self.item.object_fit
    ) {
        self.box.addClass('fpls-object-fit--' + self.item.object_fit);
    }

    //Notify a broken post (image 404)
    self.checkPostBroken();

    //ANALYTICS: post-hover
    var _timer;
    self.box.on('mouseenter.ls', function(e) {
        clearTimeout(_timer);
        _timer = setTimeout(function() {
            var payload = self.getAnalyticsPayload('post-hover', e);
            if (self.item.tags) payload.post_tags = self.item.tags;
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        }, 100);
    });

    self.box.on('mouseleave.ls', function() {
        clearTimeout(_timer);
    });

    self.box.on('keydown.ls', function(e) {
        if ($ls(e.target).is('button,a')) return;
        if  (e.which == 32 || e.which == 13) {
            self.box.trigger('click.ls');
            e.preventDefault();
            e.stopPropagation();
        }
    });

    //ANALYTICS: post-click
    self.box.on('click.ls', function(e) {
        var payload = self.getAnalyticsPayload('post-click', e);
        if (self.item.tags) payload.post_tags = self.item.tags;
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    });

    //ANALYTICS: post-impression
    var payload = self.getAnalyticsPayload('post-impression');
    if (self.item.tags) payload.post_tags = self.item.tags;
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });

    //add post id to box
    self.box.data('id', self.post_id);

    self.postRender();
}

/**
 * function to render the box of type photo
 */
LSBox_post.prototype.renderPhoto = function() {
    var self = this;
    var context = self.mapContext();
    context = self.mapContextPhoto(context);

    // Thron CDN China switch
    context = livestory_thron.fixThronChina(context);
    var photoTpl = self.photoTpl(context);

    // LST-2987 (lazy loading)
    if (self.wallInstance.wall.themeOptions && self.wallInstance.wall.themeOptions.lazyLoad && window.IntersectionObserver) {
        photoTpl = LSHelpers.replaceLazyAttr(photoTpl);
    }

    if (self.item.carouselLazyLoad && window.IntersectionObserver) {
        photoTpl = LSHelpers.replaceLazyAttr(photoTpl);
    }

    self.box = self.getHtml(photoTpl);

    if (self.item.popup_disabled) self.box.addClass('fpls-no-popup');

    // Load popup (if not direct link)
    if (!self.item.link && !self.item.popup_disabled) {
        var popup = new self.wallInstance.LSPopup(self.item.index);
        popup.init(self.item, self.wallInstance);
        self.box.on('click.ls', function(e) {
            var pin = $ls(e.target).attr('data-pin');
            popup.showPopup(pin);
        });
    }

}

/**
 * function to render the box of type video
 */
LSBox_post.prototype.renderVideo = function() {
    var self = this;
    var context = self.mapContext();
    context = self.mapContextVideo(context);

    // Thron CDN China switch
    context = livestory_thron.fixThronChina(context);

    context.show_play = context.html5video = !self.item.link && self.item.popup_disabled;

    //If poster is a placeholder, load html5 video instead
    if (
        !context.poster_url ||
        (context.poster_url && context.poster_url.indexOf('placeholder') > 0)
    ){
        context.html5video = true;
        context.poster_url = false;
    }

    if (context.autoplay) {
        context.html5video = true;
        context.show_unmuted = false;

        // Enable play/pause for the autoplay only if show_play_autoplay is active
        if (context.show_play_autoplay) {
            context.show_play = true;
        } else {
            context.show_play = false;
        }

        // Show unmuted icon only if unmuted is active and show_play_autoplay is not active
        if (context.unmuted && !context.show_play_autoplay) {
            context.show_unmuted = true;
        }

        //always hide controls if item has a link
        if (self.item.link) {
            context.show_play = false;
            context.show_unmuted = false;
        }
    }

    //enable audio if setting is active and it is not autoplay
    context.muted = true;

    if (!context.autoplay && context.audio) {
        context.muted = false;
    }

    // LST-2987 (lazy loading)
    var videoTpl = self.videoTpl(context);

    if (self.wallInstance.wall.themeOptions && self.wallInstance.wall.themeOptions.lazyLoad && window.IntersectionObserver) {
        videoTpl = LSHelpers.replaceLazyAttr(videoTpl);
    }

    if (self.item.carouselLazyLoad && window.IntersectionObserver) {
        videoTpl = LSHelpers.replaceLazyAttr(videoTpl);
    }

    self.box = self.getHtml(videoTpl);

    // LST-2987 (lazy loading)
    var globalLazyLoad = self.wallInstance.wall.themeOptions && self.wallInstance.wall.themeOptions.lazyLoad;

    //if autoplay and not lazy loading, start it
    if (!(globalLazyLoad || self.item.carouselLazyLoad) && window.IntersectionObserver) {
        if (context.autoplay && !self.wallInstance.playlist) setTimeout(function() {
            self.playVideo();
        }, 100);
    }

    if (self.item.popup_disabled) self.box.addClass('fpls-no-popup');

    //play html5 video if is there and it has a play button
    if (context.show_play && context.html5video) {
        self.box.on('click.ls', function() {
            if (self.isPlaying()) {
                self.pauseVideo();
            } else {
                self.playVideo();
            }
        });
    }

    //interact with unmuted button
    if (context.autoplay && context.html5video && context.show_unmuted) {
        var el = self.box.find('video')[0];
        var unmutedIcon = self.box.find('.fpls-box-unmuted');
        self.box.on('click.ls', function() {
            // If video is muted turn on audio and hide icon
            if (el.muted === true) {
                el.muted = false;
                unmutedIcon.addClass('hide-icon');
            } else {
                // If video is unmuted turn off audio and show icon (on hover
                el.muted = true;
                unmutedIcon.removeClass('hide-icon');
            }
        });
    }

    // Load popup (if not direct link)
    if (!self.item.link && !self.item.popup_disabled) {
        var popup = new self.wallInstance.LSPopup();
        popup.init(self.item, self.wallInstance);
        self.box.on('click.ls', function() {
            popup.showPopup();
        });
    }

    self.wallInstance.mainContainer.on('switch.tab.ls', function() {
        var el = self.box.find('video')[0];
        if (el) {
            if (el.currentTime > 0) {
                self.pauseVideo();
                el.currentTime = 0;
            }
        }
    });

    self.endVideo();
}

/**
 * function to detect if box is playing or not
 * @returns {boolean}
 */
LSBox_post.prototype.isPlaying = function() {
    var self = this;
    var el = self.box.find('video')[0];
    if (el.play && !el.paused) return true;
    return false;
}

/**
 * function to play the video
 */
LSBox_post.prototype.playVideo = function() {
    var self = this;
    var el = self.box.find('video')[0];
    var playIcon = self.box.find('.fpls-box-play');
    if (el.play) {
        el.play();
        //playIcon.css('opacity', '0.0001');
        playIcon.addClass('fpls-box-play--playing');

        //ANALYTICS: wall-video-play
        var payload = self.getAnalyticsPayload('wall-video-play');
        if (self.item.type) payload.wall_video_source = self.item.type;
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    }
}

/**
 * function to pause the video
 */
LSBox_post.prototype.pauseVideo = function() {
    var self = this;
    var el = self.box.find('video')[0];
    var playIcon = self.box.find('.fpls-box-play');
    if (el.play) {
        el.pause();
        //playIcon.css('opacity', '1');
        playIcon.removeClass('fpls-box-play--playing');

        //ANALYTICS: wall-video-pause
        var payload = self.getAnalyticsPayload('wall-video-pause');
        payload.wall_video_time = el.currentTime;
        payload.wall_video_total_time = el.duration;
        if (self.item.type) payload.wall_video_source = self.item.type;
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    }
}

/**
 * function to manage the end of video:
 * restart the video if loop
 * trigger analytics event
 */
LSBox_post.prototype.endVideo = function() {
    var self = this;
    var el = self.box.find('video');
    el.on('ended', function() {
        if (self.item.loop === undefined || self.item.loop) this.play();

        //ANALYTICS: wall-video-completed
        if (!self.item.autoplay) {
            var payload = self.getAnalyticsPayload('wall-video-completed');
            if (self.item.type) payload.wall_video_source = self.item.type;
            livestory_analytics.pushEvent(payload, {
                item: self.item,
                wall: self.wallInstance.wall,
                wallgroup: self.wallInstance.wallgroup
            });
        }
    });

}

/**
 * function to render the box of type text
 */
LSBox_post.prototype.renderText = function() {
    var self = this;
    var context = self.mapContext();
    context = self.mapContextText(context);

    self.box = self.getHtml(self.textTpl(context));

    if (self.item.popup_disabled) self.box.addClass('fpls-no-popup');

    // Load popup (if not direct link)
    if (!self.item.link && !self.item.popup_disabled) {
        var popup = new self.wallInstance.LSPopup();
        popup.init(self.item, self.wallInstance);
        self.box.on('click.ls', function() {
            popup.showPopup();
        });
    }
}

/**
 * Method to get the html of the box, either to append it of (in case of SSR) to reuse the server version of it
 * @param {html} tpl: html of the template (optional)
 * @returns {jQuery DOM element} html of the box
 */
LSBox_post.prototype.getHtml = function(tpl) {
    var self = this;
    self.box = LSBox.prototype.getHtml.call(this, tpl);
    return self.box;
}

/**
 * Method to create the context for the Handlebars template
 * It basically takes the item and remap some fields
 * Specific functions are available for specific types
 * @returns {Object} context
 */
LSBox_post.prototype.mapContext = function() {
    var self = this;
    var context = self.item;
    context.avatar = self.item.author && self.item.author.profile_picture;
    context.full_name = self.item.author && self.item.author.full_name;
    context.username = self.item.author && self.item.author.username;
    context.date = moment(self.item.timestamp).fromNow();
    if (context.link) context.link = self.wallInstance.addUtmToUrl(context.link);
    context.viewOverlay = !self.item.popup_disabled && !context.link;
    context.boxViewLabel = self.boxViewLabel;
    context.boxViewAriaLabel = self.boxViewAriaLabel;
    context = self.getWallContext(context);

    if (!context.accessibility_caption && self.item.title) context.accessibility_caption = self.item.title;
    if (!context.accessibility_caption && self.item.text) context.accessibility_caption = self.item.text.substring(0, 50) + '...';

    if (self.item.show_pins) context.pins = self.mapPins(context.actions);
    context.popup_disabled = self.item.popup_disabled;

    //Role presentation on images without link or interactions
    context.role_presentation = !self.item.link && self.item.popup_disabled && !self.item.show_pins;

    return context;
}

/**
 * Method to add specific context for photo elements
 * @param {Object} context: additional context will be added here
 * @returns {Object} context
 */
LSBox_post.prototype.mapContextPhoto = function(context) {
    var self = this;
    context.image_url = LSHelpers.getImagePath(self.item, self.resolution, self.restrain_y);
    context.image_responsive_url = LSHelpers.getImageSrcSet(self.item, self.resolution, self.restrain_y);
    return context;
}

/**
 * Method to add specific context for video elements
 * @param {Object} context: additional context will be added here
 * @returns {Object} context
 */
LSBox_post.prototype.mapContextVideo = function(context) {
    var self = this;
    var ratio = window.devicePixelRatio || 1;
    context.poster_url = LSHelpers.getImagePath(self.item, self.resolution * ratio, self.restrain_y);
    context.poster_responsive_url = LSHelpers.getImageSrcSet(self.item, self.resolution, self.restrain_y);
    context.video_url = LSHelpers.getVideoPath(self.item);
    context.video_optimized_url = LSHelpers.getOptimizedVideoPath(self.item, self.resolution);

    //if no poster, add timestamp to video url
    if (!context.poster_url || context.poster_url.indexOf('images/placeholder') > -1) {
        context.video_url += '#t=0.2';
        context.video_optimized_url += '#t=0.2';
    }

    return context;
}

/**
 * Method to add specific context for text elements
 * @param {Object} context: additional context will be added here
 * @returns {Object} context
 */
LSBox_post.prototype.mapContextText = function(context) {
    var self = this;
    context.quote = self.item.text;
    return context;
}

/**
 * Method that extends the default analytics payload with post specific values (such as post id, post type etc.)
 * @param {String} eventType: type of the event
 * @param {Event} e: native event object
 * @returns {Object} analytics payload
 */
LSBox_post.prototype.getAnalyticsPayload = function(eventType, e) {
    var self = this;
    var payload = LSBox.prototype.getAnalyticsPayload.call(self, eventType, e);

    //add alternative text to payload
    if (self.item.accessibility_caption) payload.post_alt = self.item.accessibility_caption;

    payload.post_id = self.post_id;
    payload.post_type = self.item.type;
    payload.post_source = self.item.source;
    if (self.item.author && self.item.author.username) payload.post_author = self.item.author.username;

    payload.event_type = eventType;
    return payload;
}

/**
 * Method that notify backend of a broken image
 */
LSBox_post.prototype.checkPostBroken = function() {
    var self = this;
    var payload;
    //ANALYTICS: post-broken
    if (self.wallInstance.wall.type == 'mood') {
        //wall post
        payload = self.getAnalyticsPayload('wallpost-broken');
    } else {
        //wall
        payload = self.getAnalyticsPayload('post-broken');
    }

    self.box.find('img').on('error.ls', function() {
        LSHelpers.debug('Unable to load image: ' + payload.post_id);
        self.box.addClass('fpls-box-broken fpls-photo-broken');
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    });

    //listen to errors on videos
    self.box.find('video, video source').on('error.ls', function() {
        LSHelpers.debug('Unable to load video: ' + payload.post_id);
        self.box.addClass('fpls-box-broken fpls-video-broken');
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    });
}

/**
 * Method that extract pins from the actions array
 * @param {array} actions: array of post call to actions
 * @returns {array} pins: array of actions with coordinates
 */
LSBox_post.prototype.mapPins = function(actions) {
    var self = this;
    var pinIndex = 0;
    var pins = actions.filter(function(action) {
        if (action.asset) {
            //load full asset
            var asset = $ls.extend({}, self.wallInstance.products[action.asset]);
            //if no asset found, return
            if (!asset.name) return false;
            var deleted = LSHelpers.getList(asset.deleted, self.wallInstance.store, 'deleted');
            //if asset is deleted, return
            if (asset.deleted) return false;
            //if asset is OOS and theme-option hideoutofstock is true, return
            if(self.wallInstance.wall.layout && self.wallInstance.wall.layout.hideoutofstock && !asset.stock) return;
        }

        if (action.coordinates && action.coordinates.x && action.coordinates.y) return true;
        return false;
    }).map(function(action) {

        // LST-2170
        var asset = $ls.extend({}, self.wallInstance.products[action.asset]);
        if (asset && asset.url) action.link = asset.url;

        pinIndex++;
        action.pinIndex = pinIndex;
        return action;
    });

    return pins;
}

LSBox_post.prototype.onLoad = function(callback) {
    var self = this;
    LSBox.prototype.onLoad.call(self, function(width, height, error) {

        if (self.w > 0 && self.h > 0) {
            var bh = self.box.height();
            var bw = self.box.width();

            if (self.item.crop) {
                self.box.find('.fpls-box-pins').css({
                    width: self.item.crop.width + '%',
                    paddingTop: self.item.crop.width * self.h / self.w + '%',
                    marginTop: self.item.crop.marginTop + '%',
                    marginLeft: self.item.crop.marginLeft + '%'
                });
            } else {
                var aspectRatio = self.h / self.w;
                var boxAspectRatio = bh / bw;
                if (aspectRatio > boxAspectRatio) { //foto più verticale, devo tagliare
                    var stretchX = 100;
                    var stretchY = aspectRatio / boxAspectRatio * 100;
                } else {
                    var stretchX = boxAspectRatio / aspectRatio * 100;
                    var stretchY = 100;
                }

                self.box.find('.fpls-box-pins').css({
                    width: stretchX + '%',
                    height: stretchY + '%',
                    top: (100 - stretchY) / 2 + '%',
                    left: (100 - stretchX) / 2 + '%'
                });

                if (self.item && self.item.focalpoint) {
                    if (self.item.focalpoint.indexOf('w') >= 0) {
                        self.box.find('.fpls-box-pins').css({
                            left: 0
                        });
                    }

                    if (self.item.focalpoint.indexOf('e') >= 0) {
                        self.box.find('.fpls-box-pins').css({
                            left: (100 - stretchX) + '%'
                        });
                    }

                    if (self.item.focalpoint.indexOf('n') >= 0) {
                        self.box.find('.fpls-box-pins').css({
                            top: 0
                        });
                    }

                    if (self.item.focalpoint.indexOf('s') >= 0) {
                        self.box.find('.fpls-box-pins').css({
                            top: (100 - stretchY) + '%'
                        });
                    }
                }
            }
        }

        callback && callback(width, height, error);
    });
}