/**
 * LSPopup
 * @constructor
 * class for post popup
*/
var LSPopup = function(index) {
    this.popupTpl = LSTemplates.livestory.popup;
    this.LSEmbed = LSEmbed;
    this.index = index;
}

/**
 * Init method for Popup
 * @param {String} item - The item object (node of the grid, or social post)
 * @param {String} wallInstance - Instance of the wall containing the item
 */
LSPopup.prototype.init = function(item, wallInstance) {
    var self = this;
    self.item = item;
    self.wallInstance = wallInstance;
    self.fadeTime = 300;
    self.assetFadeTime = 150;
    self.productLabel = 'Shop now';

    self.assetPixels = 500;

    self.assetGalleryEnabled = true;

    if (self.wallInstance.wall.uiLabels && self.wallInstance.wall.uiLabels.productAction) {
        self.productLabel = self.wallInstance.wall.uiLabels.productAction;
    }

    self.post_id = self.item.id || self.item._id; //if coming from mood layouts, post id is in "id", otherwise it's in _id

}

/**
 * Method to show popup
 * @param {Number} pin - The number of the pin to highlight
 */
LSPopup.prototype.showPopup = function(pin) {
    var self = this;

    $ls('body').addClass('fpls-popup-visible');

    var context = self.mapContext();
    self.popup = $ls(self.popupTpl(context));
    self.detectClick();

    self.unbindKeys();
    self.assetGallery();
    self.pinInteractions(pin);
    //self.unbindArrowKeys();
    //self.bindArrowKeys();
    //self.bindMobileEvents();

    //Social popups
    self.popup.find('.fpls-popup-share a').on('click.ls', function(e) {
        e.preventDefault();
        var url = $ls(this).attr('href');
        LSHelpers.openShare(url);
    });

    //click close button
    self.popup.find('.fpls-popup-close').on('click.ls', function() {
        self.hidePopup();
    });

    //click background overlay
    self.popup.find('.fpls-popup-wrapper').on('click.ls', function(e) {
        if ($ls(e.target).is('.fpls-popup-wrapper')) self.hidePopup();
    });

    // next / prev arrows expect two methods in the wallInstance class
    // If no next / prev functions are available, buttons are hidden

    self.popup.find('.fpls-popup-prev').hide().on('click.ls', function(e) {
        e.preventDefault();
        if (self.wallInstance.prev) {
            if (e.target.style.display != "none") {
                livestory_a11y.close();
                self.wallInstance.prev(self.post_id);
            }
        }
    });

    self.popup.find('.fpls-popup-next').hide().on('click.ls', function(e) {
        e.preventDefault();
        if (self.wallInstance.next) {
            if (e.target.style.display != "none") {
                livestory_a11y.close();
                self.wallInstance.next(self.post_id);
            }
        }
    });

    //next / prev methods available, show buttons
    if (self.wallInstance.prev && self.wallInstance.prevAvailable(self.post_id)) self.popup.find('.fpls-popup-prev').show();
    if (self.wallInstance.next && self.wallInstance.nextAvailable(self.post_id)) self.popup.find('.fpls-popup-next').show();

    // like button
    if (self.wallInstance.wall.options.post_likes) {
        self.popup.find('.fpls-popup-like').show();
    }
    self.popup.find('.fpls-popup-like').on('click.ls', function() {
        self.sendLike(self.post_id, function() {
            self.popup.find('.fpls-popup-like').addClass('liked');
        });
    });

    var wallClasses = self.wallInstance.wrapper.attr('class');
    var popupClasses = self.popup.attr('class');
    self.popup.attr('class', popupClasses + ' fpls ' + wallClasses);

    //transition
    self.popup.find('.fpls-popup-wrapper').hide();
    self.popup.show();
    
    if (self.isPopupVisible()) {
        var old_popup = $ls('.fpls-popup');
        old_popup.find('.fpls-popup-background').hide();
        old_popup.find('.fpls-popup-wrapper').fadeOut(self.fadeTime, function() {
            old_popup.remove();
        });
    } else {
        self.popup.addClass('fpls-popup-loading');
    }

    $ls('body').append(self.popup);

    if (self.popup.find('.fpls-popup-image img').length <= 0) {
        self.popup.find('.fpls-popup-wrapper').fadeIn(self.fadeTime, function() { livestory_a11y.open(); });
        self.popup.removeClass('fpls-popup-loading');
    } else {
        self.popup.find('.fpls-popup-image img').eq(0).on('load error', function() {
            self.popup.find('.fpls-popup-wrapper').fadeIn(self.fadeTime, function() { livestory_a11y.open(); });
            self.popup.removeClass('fpls-popup-loading');
        });
    }

    self.enableCarousel();

    //custom callback on popup view
    if (self.wallInstance.options && self.wallInstance.options.onPopupShow) {
        self.wallInstance.options.onPopupShow(self.popup);
    }

    self.handleVideo(self.popup.find('.fpls-popup-image > .fpls-popup-video-wrapper'), true);

    $ls(window).trigger('ls-popup-open');

    self.checkPostBroken();
    if (!self.wallInstance.wall.ls_watermark.hide_logo_on_popup) {
        self.addCredits(self.wallInstance.wall.ls_watermark.disable_logo_link_on_popup);
    }

    self.bindKeys();
    self.bindMobileEvents();
    self.sendPopupImpressionEvent();
}

/**
 * Method to send vote (click on "heart" icon)
 * @param {String} post_id - ID of the post to like
 * @param {function} callback - callback function to call after vote is registered
 */
LSPopup.prototype.sendLike = function(post_id, callback) {
    var self = this;
    $ls.ajax({
        type: "POST",
        url: LSConfig.apiURL + 'post/' + post_id + '/vote/' + livestory_analytics.getClientId(),
        success: function() {
            callback && callback();
        }
    });

    //ANALYTCS: post-like
    var payload = self.getAnalyticsPayload('post-like');

    if (self.item.tags) payload.post_tags = self.item.tags;
    if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });
}

/**
 * Method to bind mobile events (swipe)
 */
LSPopup.prototype.bindMobileEvents = function() {
    var self = this;
    //Support drag events
    self.popup.find('.fpls-popup-image').on('touchstart.ls', function(e) {
        self.__touchstart(e);
    });
    self.popup.find('.fpls-popup-image').on('touchend.ls', function(e) {
        self.__touchend(e);
    });
    self.popup.find('.fpls-popup-image').on('touchmove.ls', function(e) {
        self.__touchmove(e);
    });
}

/**
 * Touch-start listener
 * @param {Event} e - Event
 */
LSPopup.prototype.__touchstart = function(e) {
    var self = this;
    self.touchEnabled = true;
    var pageX = e.originalEvent.targetTouches[0].pageX;
    var pageY = e.originalEvent.targetTouches[0].pageY;
    self.touchPositionX = pageX;
    self.touchPositionY = pageY;
    self.touchDeltaX = 0;
    self.touchDeltaY = 0;
    self.popup.find('.fpls-popup-wrapper').addClass('fpls-dragging');
}

/**
 * Touch-move listener
 * @param {Event} e - Event
 */
LSPopup.prototype.__touchmove = function(e) {
    var self = this;
    var pageX = e.originalEvent.targetTouches[0].pageX;
    var pageY = e.originalEvent.targetTouches[0].pageY;
    self.touchDeltaX = pageX - self.touchPositionX;
    self.touchDeltaY = pageY - self.touchPositionY;
    if (Math.abs(self.touchDeltaX) > 25 && Math.abs(self.touchDeltaY) < 75) {
        e.preventDefault();
        self.popup.find('.fpls-popup-wrapper').css({
            transform: "translateX(" + self.touchDeltaX / 4 + "px)"
        });
    }
}

/**
 * Touch-end listener
 * @param {Event} e - Event
 */
LSPopup.prototype.__touchend = function(e) {
    var self = this;
    self.touchEnabled = false;
    if (Math.abs(self.touchDeltaX) > 100 && Math.abs(self.touchDeltaY) < 75) {
        if (self.touchDeltaX < 0) {
            self.wallInstance.next && self.wallInstance.next(self.post_id);
        } else {
            self.wallInstance.prev && self.wallInstance.prev(self.post_id);
        }
    }
    self.popup.find('.fpls-popup-wrapper').removeClass('fpls-dragging');
    self.popup.find('.fpls-popup-wrapper').css({
        transform: "translateX(" + 0 + "px)"
    });
}

/**
 * Method to handle videos within the popup
 * @param {html} container - Container element
 * @param {boolean} autoplay - either if video is autoplay or not
 */
LSPopup.prototype.handleVideo = function(container, autoplay) {

    var self = this;
    var video = container.find('video');
    var btn = container.find('.fpls-box-play');

    if (video.length < 1) return;

    video.off('click.ls').on('click.ls', function() {
        if (video[0].paused) {
            video[0].play();
        } else {
            video[0].pause();
        }
    });

    video.on('play.ls', function() {
        btn.css({opacity: 0});
    });
    video.on('pause.ls', function() {
        btn.css({opacity: 1});
    });
    btn.on('click.ls', function() {
        if (video[0].paused) {
            video[0].play();
        } else {
            video[0].pause();
        }
    });

    if (autoplay) {
        // Decrease video volume
        video.prop("volume", 0.25);
        self.playVideo(video);
    }
}

/**
 * NOT USED: Method that returns popup html
 */
LSPopup.prototype.getHtml = function() {
    return this.popup;
}

/**
 * Method to hide the popup
 */
LSPopup.prototype.hidePopup = function() {
    var self = this;
    self.unbindKeys();
    //save reference to this
    var self = this;
    //stop running videos
    self.popup.find("video").each(function() {
        this.pause()
    });
    self.popup.fadeOut(self.fadeTime, function() {
        self.popup.remove();
        $ls(window).trigger('ls-popup-close');
    });

    $ls('body').removeClass('fpls-popup-visible');

    livestory_a11y.close();
}

/**
 * Method to detect if popup is visible or not
 */
LSPopup.prototype.isPopupVisible = function() {
    if ($ls('.fpls-popup').length > 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * Map call to actions and prepare them for template
 * @param {array} actions: array of actions
 * @returns {array} mapped actions
 */
LSPopup.prototype.mapActions = function(actions) {
    var self = this;
    self.pinIndex = 0;
    self.pins = [];

    if (actions) for (var i = 0; i < actions.length; i++) {
        if (actions[i].image) actions[i].image = LSHelpers.resize(actions[i].image, self.assetPixels);
        if (actions[i].type == 'link') actions[i].is_link = true;
        if (actions[i].type == 'video') actions[i].is_video = true;

        actions[i].not_asset = true;
        if (actions[i].type == 'asset') actions[i].not_asset = false;

        actions[i].iframe = btoa(actions[i].embedded_video);
        actions[i].pinned = false;

        //new "asset" action type
        if (actions[i].type == 'asset' && typeof self.wallInstance.products[actions[i].asset] !== 'undefined') {
            actions[i].is_asset = true;
            var asset = $ls.extend({}, self.wallInstance.products[actions[i].asset]);

            var parsedProduct = self.parseProduct(asset);
            if (parsedProduct) {
                actions[i].asset = parsedProduct;    
            }
        }

        if (actions[i].coordinates && actions[i].coordinates.x && actions[i].coordinates.y &&
            !(actions[i].is_asset && typeof actions[i].asset == 'string')) {
            //second condition is to exclude assets that are not mapped
            self.pinIndex++;

            actions[i].pinIndex = self.pinIndex;
            actions[i].pinned = true;
            if (actions[i].asset) {
                actions[i].asset.pinIndex = self.pinIndex;
                actions[i].asset.pinned = true;
                actions[i].asset.pin_id = actions[i]._id;
            }

            self.pins.push(actions[i]);
        }
    }
    return actions;
}

/**
 * Get pins from call to actions (actions that have coordinates)
 * @param {array} actions: array of actions
 * @returns {array} found pins
 */
LSPopup.prototype.mapPins = function(actions) {
    var pins = actions.filter(function(action) {
        if (action.pinned && action.coordinates && action.coordinates.x && action.coordinates.y) return true;
        return false;
    });

    return pins;
}

/**
 * Products are passed in a global array in the wall itself
 * This method search for the product given the assets in the item itself
 * It will also add assets from the actions array
 * @param {Object} actions: additional actions of the items
 * @returns {array} array of assets
 */
LSPopup.prototype.parseProducts = function(actions) {
    var self = this;
    var assets = [];

    self.item.assets.forEach(function(asset) {

        if (typeof asset === 'string') {
            if (typeof self.wallInstance.products[asset] === 'undefined') return;
            asset = $ls.extend({}, self.wallInstance.products[asset]);
        }

        var parsedProduct = self.parseProduct(asset);
        if (parsedProduct) {
            assets.push(parsedProduct);
        }

    });

    actions.forEach(function(action) {
        if (action.asset) assets.push(action.asset);
    });

    return assets;
}

/**
 * Parse single asset and format it for the template
 * @param {Object} asset: original asset
 * @returns {Object} asset context for Handlebars
 */
LSPopup.prototype.parseProduct = function(asset) {
    var self = this;

    asset.deleted = LSHelpers.getList(asset.deleted, self.wallInstance.store, 'deleted');
    //extract sale price only if price is still an object
    if (typeof asset.price === 'object') asset.sale_price = LSHelpers.getList(asset.price, self.wallInstance.store, 'sale_value', true);
    asset.price = LSHelpers.getList(asset.price, self.wallInstance.store, 'list_value');
    asset.name = LSHelpers.getList(asset.name, self.wallInstance.store, 'lang_value');
    asset.description = LSHelpers.getList(asset.description, self.wallInstance.store, 'lang_value');
    asset.url = LSHelpers.getList(asset.url, self.wallInstance.store, 'lang_value');

    // LST-1290
    if (asset.sale_price === asset.price) asset.sale_price = '';

    var context_asset = $ls.extend({}, asset);

    context_asset.image_url = LSHelpers.getImagePath(asset, self.assetPixels);

    if (!context_asset.price) {
        //no price
        context_asset.showprice = false;
    } else if (!context_asset.price.toString().replace(/\D/g, '')) {
        //price not a number
        context_asset.showprice = false;
    } else if (parseFloat(context_asset.price.toString().replace(/\D/g, '')) == 0) {
        //price is a number but it's zero
        context_asset.showprice = false;
    } else {
        context_asset.showprice = true;
    }

    // skip if outofstock and wall settings says to hide outofstock
    if (self.wallInstance.wall.layout && self.wallInstance.wall.layout.hideoutofstock && !context_asset.stock) return;

    // skip if product is deleted
    if (context_asset.deleted) return;

    //add tracking to product URLs
    context_asset.url = self.wallInstance.addUtmToUrl(context_asset.url, self.item, context_asset);
    return context_asset;
}

/**
 * Method to create the context for the Handlebars template
 * It basically takes the item and remap some fields
 * @returns {Object} context
 */
LSPopup.prototype.mapContext = function() {
    var self = this;
    var context = {};

    context.avatar = self.item.author && self.item.author.profile_picture;
    context.full_name = self.item.author && self.item.author.full_name;
    context.username = self.item.author && self.item.author.username;
    context.title = self.item.title;
    context.description = self.item.text && self.item.text.replace(/\n/g, "<br />").replace(/#/g, " #");
    context.url = self.item.url;
    context.productLabel = self.productLabel;

    context.date = moment(self.item.timestamp).fromNow();
    context.source = self.item.source;

    context.accessibility_caption = self.item.accessibility_caption;

    context._id = self.item._id;

    context = self.getWallContext(context);

    //Fallbacks
    if (!context.accessibility_caption && self.item.title) context.accessibility_caption = self.item.title;
    if (!context.accessibility_caption && self.item.text) context.accessibility_caption = self.item.text.substring(0, 50) + '...';

    if (self.wallInstance.current_layout.overlays) {
        context.popup_background = self.wallInstance.current_layout.overlays.popup_background;
    }

    //prepare a video player for video CTA
    self.wallInstance.embed = new self.LSEmbed();
    self.wallInstance.embed.init(self.wallInstance);

    if (context.source == 'manual') context.source = 'Live Story';

    context.link = self.item.link;
    context.link_cta = self.item.link_cta || "More";

    //Specific popup contents
    if (self.item.type == 'photo') {
        context.image_url = LSHelpers.getImagePath(self.item, "high");
    }

    if (self.item.type == 'video') {
        context.poster_url = LSHelpers.getImagePath(self.item, "high");
        context.video_url = LSHelpers.getVideoPath(self.item);
    }

    if (self.item.type == 'text') {
        context.text = self.item.text || '';
    }

    if (self.item.type == 'mixed') {
        context.mixed = true;
        context.medias = [];
        self.item.medias.forEach(function(media, index) {
            var item = {};

            if (media.type == 'photo') {
                item.type = media.type;
                item.image_url = LSHelpers.getMixedImagePath(media, "high"); 
            }

            if (media.type == 'video') {
                item.type = media.type;
                item.video_url = LSHelpers.getMixedVideoPath(media);
            }

            if (media.type == 'video' && self.item.dangling == true) return;

            if (media.type == 'photo' && self.item.dangling == true) {
                item.image_url = LSHelpers.resize('/post/orig/' + self.post_id, self.assetPixels, false) + '/' + index;
            }


            item.accessibility_caption = "";
            if (index == 0) item.accessibility_caption += context.accessibility_caption + ', ';
            item.accessibility_caption += (1 + index) + ' of ' + self.item.medias.length;
            context.medias.push(item);
        });
    }

    if (self.item.image_cta) {
        context.image_cta = LSHelpers.resize(self.item.image_cta, self.assetPixels);
    }

    var shareUrl = encodeURIComponent(window.location.href);
    var shareMedia = encodeURIComponent(context.image_url);

    //Share URLs
    if (context.image_url) {
        context.twitter_share = "https://twitter.com/intent/tweet?text=" + shareUrl + " - Shared with LiveStory";
        context.pinterest_share = "https://pinterest.com/pin/create/button/?url=" + shareUrl + "&media=" + shareMedia + "&description=Shared with LiveStory";
        context.facebook_share = "https://www.facebook.com/sharer/sharer.php?u=" + shareUrl;
    } else {
        context.twitter_share = "https://twitter.com/intent/tweet?text=" + shareUrl + " - Shared with LiveStory";
        context.facebook_share = "https://www.facebook.com/sharer/sharer.php?u=" + shareUrl;
    }

    context.actions = self.mapActions(self.item.actions);
    context.pins = self.mapPins(context.actions);
    context.assets = self.parseProducts(context.actions);

    context.assets = context.assets.filter(function(asset) {
        return typeof asset.name != 'undefined'
    });

    //If sidebar would be empty, set an appropriate class
    if (context.assets.length < 1 && !context.title && !context.description && !context.link) {
        context.extra_class = 'fpls-no-assets';
    }

    return context;
}

/**
 * Method that get custom options and update the context
 * @param {Object} context: additional context will be added here
 * @returns {Object} context
 */
LSPopup.prototype.getWallContext = function(context) {
    var self = this;

    context.wall = self.wallInstance.context;

	return context;
}

/**
 * Method to enable popup carousel (in case of mixed posts)
 */
LSPopup.prototype.enableCarousel = function() {
    var self = this;
    if (self.item.type != 'mixed') return;
    var pinsWrapper = self.popup.find('.fpls-popup-pins');
    self.popup.find('.fpls-popup-carousel').each(function() {
        var carousel = $ls(this);
        var items = carousel.find('.fpls-popup-carousel-item');
        var next = carousel.find('.fpls-popup-carousel-next');
        var prev = carousel.find('.fpls-popup-carousel-prev');
        var dot_wrapper = carousel.find('.fpls-popup-carousel-dots');
        var carousel_slider = carousel.find('.fpls-popup-carousel-slider');

        carousel_slider.width(100 * items.length + '%');
        items.width(100 / items.length + '%');

        var i = 0;

        items.each(function() {
            self.handleVideo($ls(this).find('.fpls-popup-video-wrapper'), false);
            dot_wrapper.append('<a class="fpls-popup-carousel-dot"></a>');
            $ls(this).find('img').on('load.ls', function() {
              //fix height after image load
                _calcHeightPadding();
            });
        });

        //Play first video
        if (items.eq(0).has('video')) {
            var video = items.eq(0).find('video');
            video.on('playing.ls', function() {
                _calcHeightPadding();
            });
            self.playVideo(items.eq(0).find('video'));
        }

        _calcHeightPadding();
        _setActiveDot();

        dot_wrapper.find('a').on('click.ls', function(e) {
            e.preventDefault();
            var index = dot_wrapper.find('a').index(this);
            i = index;
            _moveSlider();
        });

        next.on('click.ls', function() {
            i++;
            if (i >= items.length) i = 0;
            if (items.eq(i).has('video')) self.playVideo(items.eq(i).find('video'));
            _moveSlider();
        });

        prev.on('click.ls', function() {
            i--;
            if (i < 0) i = items.length - 1;
            if (items.eq(i).has('video')) self.playVideo(items.eq(i).find('video'));
            _moveSlider();
        });

        function _setActiveDot() {
            dot_wrapper.find('a.fpls-popup-carousel-dot-active').removeClass('fpls-popup-carousel-dot-active');
            dot_wrapper.find('a').eq(i).addClass('fpls-popup-carousel-dot-active');
            if (i > 0) {
                pinsWrapper && pinsWrapper.hide();
            } else {
                pinsWrapper && pinsWrapper.show();
            }
        }

        function _moveSlider() {
            carousel_slider.css({left: -i * 100 + '%'});
            _calcHeightPadding();
            _setActiveDot();
        }

        function _calcHeightPadding() {
            var h = items.eq(i).height() / carousel.width() * 100;
            if (h < 1) h = 100; //temporary set to 100 if calculated value is zero
            carousel.css({paddingTop: h + '%'});
        }
    });
}

/**
 * Method to enable pins in photo
 * @param {Number} initialPin: pin to highlight at first
 */
LSPopup.prototype.pinInteractions = function(initialPin) {
    var self = this;
    var picturePins = self.popup.find('.fpls-popup-pins .fpls-popup-pin');
    var assetPinned = self.popup.find('.fpls-popup-assets .fpls-popup-pinned');
    var allPinned   = self.popup.find('.fpls-popup-pinned');

    //handle pins click
    if (self.assetGalleryEnabled) picturePins.on('click', function() {
        var dest = $ls(this).attr('data-pin');
        var tmp = assetPinned.filter('[data-pin="' + dest + '"]');

        picturePins.removeClass('fpls-active');
        $ls(this).addClass('fpls-active');

        if (tmp.length > 0) self.currentAsset.fadeOut(self.assetFadeTime, function() {
            tmp.fadeIn(self.assetFadeTime);
            self.currentAsset = tmp;
        });
    });

    if (initialPin) {
        setTimeout(function() {
            var tmp = assetPinned.filter('[data-pin="' + initialPin + '"]');
            if (tmp.length > 0) self.currentAsset.fadeOut(self.assetFadeTime, function() {
                tmp.fadeIn(self.assetFadeTime);
                self.currentAsset = tmp;
            });

            _setPin(initialPin);
        }, 10);
    }

    picturePins.on('mouseenter', function() {
        var dest = $ls(this).attr('data-pin');
        _setPin(dest);
    }).on('mouseleave', function() {
        _setPin(false);
    });

    allPinned.on('mouseenter', function() {
        var dest = $ls(this).attr('data-pin');
        _setPin(dest);
    }).on('mouseleave', function() {
        _setPin(false);
    });

    function _setPin(dest) {
        if (dest === false) {
            allPinned.removeClass('fpls-current');
            picturePins.removeClass('fpls-current');
            return;
        }
        allPinned.not('[data-pin="' + dest + '"]').removeClass('fpls-current');
        allPinned.filter('[data-pin="' + dest + '"]').addClass('fpls-current');

        picturePins.not('[data-pin="' + dest + '"]').removeClass('fpls-current');
        picturePins.filter('[data-pin="' + dest + '"]').addClass('fpls-current');
    }
}

/**
 * Method to create the carousel of assets
 */
LSPopup.prototype.assetGallery = function() {
    var self = this;

    var next = self.popup.find('.fpls-popup-assets-next');
    var prev = self.popup.find('.fpls-popup-assets-prev');

    if (!self.assetGalleryEnabled) {
        next.remove();
        prev.remove();
        return;
    }

    var asset_wrapper = self.popup.find('.fpls-popup-assets');
    var assets = self.popup.find('.fpls-popup-asset');

    if (assets.length < 1) {
        return;
    }

    self.currentAsset = assets.eq(0);
    self.sendImpression(self.currentAsset.find('a[data-sku]'));

    if (assets.length < 2) {
        self.popup.find('.fpls-popup-assets-nav').remove();
        return;
    }

    assets.not(self.currentAsset).hide();

    next.on('click.ls', function(e) {
        e.preventDefault();
        var tmp = self.currentAsset.next('.fpls-popup-asset');
        if (tmp.length < 1) tmp = assets.eq(0);
        self.currentAsset.fadeOut(self.assetFadeTime, function() {
            tmp.fadeIn(self.assetFadeTime);
            self.currentAsset = tmp;
        });
        self.sendImpression(tmp.find('a[data-sku]'));
    });

    prev.on('click.ls', function(e) {
        e.preventDefault();
        var tmp = self.currentAsset.prev('.fpls-popup-asset');
        if (tmp.length < 1) tmp = assets.eq(assets.length - 1);
        self.currentAsset.fadeOut(self.assetFadeTime, function() {
            tmp.fadeIn(self.assetFadeTime);
            self.currentAsset = tmp;
        });
        self.sendImpression(tmp.find('a[data-sku]'));
    });
}

/**
 * Method to send impression of each product
 * @param {html} a: html <a></a> element containing the product
 */
LSPopup.prototype.sendImpression = function(a) {
    var self = this;
    //ANALYTICS: product-impression
    var payload = self.getAnalyticsPayload('product-impression');
    payload.product_sku = a.data('sku');
    payload.product_price = a.data('price');
    payload.product_name = a.data('name');
    self.getCustomLabelPayload(a, payload);

    if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });
}

/** 
 * A11Y: Resume user input interactions
 */
LSPopup.prototype._resumeInteractions = function(){
    this.bindKeys();
}

/** 
 * A11Y: Pause user input interactions 
 */
LSPopup.prototype._pauseInteractions = function(){
    this.unbindKeys();
}

/**
 * Method to open the popup of embedded videos in call to actions
 * @param {Event} e: event object
 * @param {Event} button: button clicked (a or button)
 */
LSPopup.prototype._openVideoEmbed = function(e, button) {
    var self = this;
    e.preventDefault();
    self._pauseInteractions();
    self.embed.setItem(self.item);
    self.wallInstance.embed.show(self._resumeInteractions.bind(this), atob(button.data('iframe')));
    self.sendAnalyticsCta(button.text(), e);
}

/**
 * Method to bind all the interatcions with the popup elements
 */
LSPopup.prototype.detectClick = function() {
    var self = this;

    //external links
    self.popup.find('.fpls-popup-cta-link a').on('click.ls', function(e) {
        var a = $ls(this);
        var dest = $ls(this).attr('href');
        var target = $ls(this).attr('target');
        if (target != '_blank' && !a.data('stop')) e.preventDefault();
        self.sendAnalyticsCta($ls(this).text(), e);
        if (dest && target != '_blank' && !a.data('stop')) livestory_analytics.flushEvents(function() {
            document.location = dest;
        });
    });

    //video call to actions
  self.popup.find('.fpls-popup-cta-video').find('a, button').on('click.ls', function(e){
      self._openVideoEmbed(e, $ls(this));
  });

  //products
  self.popup.find('a[data-sku]').each(function() {

      var a = $ls(this);
      var _timer;

      a.on('mouseenter.ls', function(e) {
          clearTimeout(_timer);
          _timer = setTimeout(function() {
              //ANALYTICS: product-hover
              var payload = self.getAnalyticsPayload('product-hover', e);
              payload.product_sku = a.data('sku');
              payload.product_price = a.data('price');
              payload.product_name = a.data('name');
              self.getCustomLabelPayload(a, payload);

              if (self.item.tags) payload.post_tags = self.item.tags;
              if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
              livestory_analytics.pushEvent(payload, {
                  item: self.item,
                  wall: self.wallInstance.wall,
                  wallgroup: self.wallInstance.wallgroup
              });
          }, 100);
      });

      a.on('mouseleave.ls', function() {
          clearTimeout(_timer);
      });

      a.on('click.ls', function(e) {
          var dest = a.attr('href');
          var target = a.attr('target');
          if (target != '_blank' && !a.data('stop')) e.preventDefault();

          //ANALYTICS: product-click
          var payload = self.getAnalyticsPayload('product-click', e);
          payload.product_sku = a.data('sku');
          payload.product_price = a.data('price');
          payload.product_name = a.data('name');
          self.getCustomLabelPayload(a, payload);

          if (self.item.tags) payload.post_tags = self.item.tags;
          if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
          livestory_analytics.pushEvent(payload, {
              item: self.item,
              wall: self.wallInstance.wall,
              wallgroup: self.wallInstance.wallgroup
          });

          if (target != '_blank' && !a.data('stop')) livestory_analytics.flushEvents(function() {
              document.location = dest;
          });
      });
  });
}

/**
 * Method to send analytics when buttons are clicked (either links or videos)
 * @param {String} cta: Text of the button
 * @param {Event} e: event object
 */
LSPopup.prototype.sendAnalyticsCta = function(cta, e) {
    var self = this;

    //ANALYTICS: postlink-click
    var payload = self.getAnalyticsPayload('postlink-click', e);
    payload.post_cta = cta || 'n.a.';

    if (self.item.tags) payload.post_tags = self.item.tags;
    if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });
}

/**
 * Method to bind popup next/prev custom events
 */
LSPopup.prototype.bindKeys = function() {
    var self = this;

    $ls(document).on('ls-popup-next', function() {
        self.wallInstance.next && self.wallInstance.next(self.post_id);
    });

    $ls(document).on('ls-popup-prev', function() {
        self.wallInstance.prev && self.wallInstance.prev(self.post_id);
    });
}

/**
 * Method to unbind popup next/prev custom events
 */
LSPopup.prototype.unbindKeys = function() {
    $ls(document).off('ls-popup-prev');
    $ls(document).off('ls-popup-next');
}

/**
 * Method to play video
 * param {html} video: Html video jQuery element
 */
LSPopup.prototype.playVideo = function(video) {
    var self = this;
    self.popup.find('video').each(function() {
        this.pause();
    });
    if (video.length >= 1) video[0].play();
}

/**
 * Method that notify backend of a broken image
 */
LSPopup.prototype.checkPostBroken = function() {
    var self = this;
    var payload;

    //ANALYTCS: post-broken
    if (self.wallInstance.wall.type == 'mood') {
        //wall post
        payload = self.getAnalyticsPayload('wallpost-broken');
    } else {
        //wall
        payload = self.getAnalyticsPayload('post-broken');
    }

    //listen to errors on images
    self.popup.find('.fpls-popup-image img').on('error.ls', function() {
        LSHelpers.debug('Unable to load image: ' + payload.post_id);
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    });

    //listen to errors on videos
    self.popup.find('.fpls-popup-image video source').on('error.ls', function() {
        LSHelpers.debug('Unable to load video: ' + payload.post_id);
        livestory_analytics.pushEvent(payload, {
            item: self.item,
            wall: self.wallInstance.wall,
            wallgroup: self.wallInstance.wallgroup
        });
    });
}

/**
 * Method to get all the analytics values, such as wall id, campaign id etc.
 * @param {String} eventType: type of the event
 * @param {Event} e: native event object
 * @returns {Object} analytics payload
 */
LSPopup.prototype.getAnalyticsPayload = function(eventType, e) {
    var self = this;

    var payload = {
        event_type: eventType,
        brand: self.item.brand,
        campaign_id: self.wallInstance.wall.campaign_id,
        campaign_name: self.wallInstance.wall.campaign_name,
        wall_id: self.wallInstance.wall.wall_id,
        post_id: self.post_id,
        wall_title: self.wallInstance.wall.title,
        wall_subtype: self.wallInstance.wall.subtype,
        wall_item_id: self.item._id,
        wall_item_type: self.item.type,
        wall_breakpoint: self.wallInstance.getWallBreakpoint(),
        post_type: self.item.type,
        post_source: self.item.source,
    }

    //add index
    if (self.item.index >= 0) payload.popup_index = self.item.index;

    if (self.item.author && self.item.author.username) payload.post_author = self.item.author.username;

    if (self.wallInstance.wallgroup && self.wallInstance.wallgroup._id) payload.wallgroup_id = self.wallInstance.wallgroup._id;
    if (self.wallInstance.wallgroup && self.wallInstance.wallgroup.title) payload.wallgroup_name = self.wallInstance.wallgroup.title;
    if (self.wallInstance && self.wallInstance.lang) payload.lang_code = self.wallInstance.lang;
    if (self.wallInstance && self.wallInstance.store) payload.store_code = self.wallInstance.store;
    return payload;
}

/**
 * Method to add Live Story logo
 * @param {boolean} disable_link: if true, link to watermark is disabled
 */
LSPopup.prototype.addCredits = function(disable_link) {
    //do not append credits if SSR
    if (typeof window.callPhantom == 'undefined') return;
    
    var self = this;
    //append to wall
    if (self.popup.find('.' + livestory_analytics.getCreditsId('popup')).length < 1) {
        if (disable_link) {
            self.popup.find('.fpls-popup-content').append('<span class="' + livestory_analytics.getCreditsId('popup') + '" style="display: block !important;"></span>');
        } else {
            self.popup.find('.fpls-popup-content').append('<a href="https://www.livestory.nyc" target="_blank" class="' + livestory_analytics.getCreditsId('popup') + '" style="display: block !important;"></a>');
        }
    }
}

/**
 * Method to load product custom lables for the analytics payload
 * @param {DOM element} product: product element
 * @param {Object} payload: analytics product payload
 */
LSPopup.prototype.getCustomLabelPayload = function(product, payload) {

    if (product.data('custom-label-0')) payload.product_custom_label_0 = product.data('custom-label-0');
    if (product.data('custom-label-1')) payload.product_custom_label_1 = product.data('custom-label-1');
    if (product.data('custom-label-2')) payload.product_custom_label_2 = product.data('custom-label-2');
    if (product.data('custom-label-3')) payload.product_custom_label_3 = product.data('custom-label-3');
    if (product.data('custom-label-4')) payload.product_custom_label_4 = product.data('custom-label-4');
    return payload;

}

/**
 * Method that send popup impression event
 */
LSPopup.prototype.sendPopupImpressionEvent = function() {
    var self = this;

    var payload = self.getAnalyticsPayload('popup-impression');

    if (self.wallInstance.wallgroup) payload.wallgroup_id = self.wallInstance.wallgroup.wallgroup_id;
    livestory_analytics.pushEvent(payload, {
        item: self.item,
        wall: self.wallInstance.wall,
        wallgroup: self.wallInstance.wallgroup
    });
}
