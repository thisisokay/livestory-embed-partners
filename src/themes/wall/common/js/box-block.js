/**
 * LSBox_block
 * @constructor
 * class for block boxes (text elements)
*/
var LSBox_block = function() {
    this.blockTpl = LSTemplates.livestory.box_block;
}
LSHelpers.extend(LSBox_block, LSBox);

/**
 * Init method for Box block
 * @param {String} item - The item object
 * @param {String} wallInstance - Instance of the wall containing the box
 */
LSBox_block.prototype.init = function(item, wallInstance) {
    var self = this;
    LSBox.prototype.init.call(this, item, wallInstance);
    self.renderBox();
}

/**
 * function to render the box
 */
LSBox_block.prototype.renderBox = function() {
    var self = this;
    var context = self.item;
    context = self.getWallContext(context);

    context.link = self.item.link;
    if (context.link) context.link = self.wallInstance.addUtmToUrl(context.link);
    context.text = self.item.text;

    if (context.link) {
        //remove links in the text to avoid html issues
        var content = $ls('<div>' + context.text + '</div>');
        content.find('a').each(function() {
            var a = $ls(this);
            var html = a.html();
            a.replaceWith('<span>' + html + '</span>');
        });
        context.text = content.html();
    }

    if (self.wallInstance.lang && self.wallInstance.lang != 'default') {
        context.lang = self.wallInstance.lang.replace('_', '-');
    }

    self.box = self.getHtml(self.blockTpl(context));

    if (self.item.padding) self.box.find('.fpls-box-text').css({ padding: self.item.padding + 'px' });

    if (self.item.text && self.item.textscroll && self.item.textscroll.active) {
        setTimeout(function() {
            self.textScroll();
        }, 1);
    }

    self.postRender();
}

LSBox_block.prototype.postRender = function() {
    var self = this;
    self.parent.postRender.call(self);

    if (!window.callPhantom) self.initCountdown(); //init countdown only client-side
}


/**
 * enable text scrolling
 */
LSBox_block.prototype.textScroll = function() {
    var self = this;
    self.box.addClass('fpls-textscroll');

    var scroller = $ls('<div class="fpls-textscroll-scroller" />');
    var text = $ls('<div class="fpls-textscroll-text" />');

    var wrapper = self.box.find('.fpls-box-text');
    var htmlContent = wrapper.html();
    wrapper.html('');

    text.html(htmlContent);
    scroller.append(text);
    wrapper.append(scroller);

    var cloneNumber = Math.min(Math.ceil(wrapper.width() / scroller.width()), 100);

    for (var i=0; i<=cloneNumber; i++) {
        var clone = text.clone();
        clone.attr('aria-hidden', true);
        clone.attr('tabindex', -1);
        scroller.append(clone);
    }

    setTimeout(function() {

        var scrollAmount = text.width();
        var duration = scrollAmount / self.item.textscroll.speed;

        var animation = LSCss.animationTextscroll(scrollAmount, duration < 0);
        LSHelpers.injectInlineCss(animation.css);

        scroller.css({
            animationDuration: Math.abs(duration) + 's',
            animationTimingFunction: 'linear',
            animationIterationCount: 'infinite',
            animationName: animation.name
        });
    }, 100);
}
