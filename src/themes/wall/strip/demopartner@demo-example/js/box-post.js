var LSBox_post_demopartner_demo_example = function() {
    this.photoTpl = LSTemplates.wall_strip_demopartner_demo_example.box_post_photo;
}
LSHelpers.extend(LSBox_post_demopartner_demo_example, LSBox_post);

LSBox_post_demopartner_demo_example.prototype.renderPhoto = function() {
    var self = this;

    self.parent.renderPhoto.call(this);
    // Added border color to photo boxes
    self.box.css('border-color', 'green')

}