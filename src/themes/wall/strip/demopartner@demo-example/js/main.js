var LSWall_strip_demopartner_demo_example = function() {
    this.LSBox_post = LSBox_post_demopartner_demo_example;
}
LSHelpers.extend(LSWall_strip_demopartner_demo_example, LSWall_strip_default);

LSWall_strip_demopartner_demo_example.prototype.renderWall = function(data) {
    var self = this;

    self.parent.renderWall.call(this, data);

    console.log('Strip example');
}