/**
 * LSWall_strip_default
 * @constructor
 * Strip for UGC contents
*/
var LSWall_strip_default = function() {
    this.wallTpl = LSTemplates.wall_strip_default.wall;
}
LSHelpers.extend(LSWall_strip_default, LSWall);


/**
 * Method that loads and parse the handlebars template
 * It also caches some html elements
 */
LSWall_strip_default.prototype.loadTemplate = function(context) {
    var self = this;

    var found = false;
    if (self.isSSR) {
        self.container = self.wrapper.find('.fpls-strip-content');
        if (self.container.length > 0) found = true;
    }
    if (!found) {
        self.container = $ls(self.wallTpl(context));
        self.wrapper.html('').append(self.container);
    }

    self.intContainer = self.container.find('.fpls-strip-container');
    self.stripwrapper = self.container.find('.fpls-strip-stripwrapper'); //outside div containing the scrolling part
    self.strip = self.container.find('.fpls-strip-strip'); //actual strip that scrolls
    self.nextButton = self.container.find('.fpls-strip-next');
    self.prevButton = self.container.find('.fpls-strip-prev');

    self.nextButton.on('click.ls', function() {
        self.scrollNext();
    });

    self.prevButton.on('click.ls', function() {
        self.scrollPrev();
    });

    self.checkTabStrip();

}

/**
 * A11Y, Manages Tab keydown event (for accessibility)
 */
LSWall_strip_default.prototype.checkTabStrip = function(){
    var self = this;
    var KEY_TAB = 9;
    $ls(document).on('keydown', function(evt) {
        if (evt.which == KEY_TAB && self.boxes) {
            self.checkStripBoxView();
        }
    });
}

/**
 * It sets the tabindex off to items outside the viewport for A11Y
 */
LSWall_strip_default.prototype.checkStripBoxView = function() {
    //exclude elements from strip if out of view
    var self = this;
    var tolerance = 5;
    self.boxes.forEach(function(box) {
        if (box.offset().left > self.intContainer.offset().left - tolerance &&
            box.offset().left + box.outerWidth() < self.intContainer.offset().left + self.intContainer.outerWidth() + tolerance ) {
                box.attr("tabIndex", "0");
        } else {
            box.attr("tabIndex", "-1");
        }
    });
}

/**
 * Method that renders the layout
 * @param {Object} data: wall JSON
 */
LSWall_strip_default.prototype.renderWall = function(data) {
    LSWall.prototype.renderWall.call(this, data);

    var self = this;
    // init
    self.skip = 0; //pagination
    self.loaded = 0; //total amount of box loaded
    self.count = data.count; //total count of posts
    self.boxes = []; //array of DOM elements of boxes

    self.data = data;

    if (data.count == 0) {
        //No posts, hide the strip
        LSHelpers.debug('Empty strip.');
        self.intContainer.hide();
        return;
    }

    //Legacy: strips with no settings
    if (typeof self.current_layout === 'undefined') {
        self.height = 300;
        self.margin = 10;
    } else {
        //set dimensions from strip settings
        self.height = parseFloat(self.current_layout.height);
        self.margin = parseFloat(self.current_layout.margin);
        if (isNaN(self.height)) self.height = 300;
        if (isNaN(self.margin)) self.margin = 10;
    }

    LSHelpers.debug('Render wall');

    if (!(self.wall.subwalls && self.wall.subwalls.length > 0)) {
        //Legacy mode for strips without breakpoints
        if (self.current_layout && self.current_layout.height_mobile && self.wrapper.outerWidth(true, true) < 768) {
            self.height = parseFloat(self.current_layout.height_mobile);
        }
    }

    if (!self.isSSR) {
        self.strip.html('');
        self.left = self.intContainer.width() / 2;
    } else {
        self.strip.html('');
        self.left = self.intContainer.width() / 2;
        self.isSSR = false;
        /*
        //mark ssr posts, so we can identify the ones that are not in the feed anymore
        self.strip.find('.fpls-box').addClass('fpls-box--ssr');
        //sets left position according to the SSR
        self.left = parseInt(self.strip.css('left'), 10);
        */
    }

    //Set strip height (wrapper and strip itself)
    self.stripwrapper.height(self.height + self.margin);
    self.strip.height(self.height + self.margin);

    //set width to zero (it will keep track of total width each time a post is added)
    self.width = 0;

    //set initial left position
    self.strip.css({
        position: 'absolute',
        top: 0,
        left: self.left
    });

    //set resoluton equal to height
    self.resolution = self.height;

    //disable transition while loading the posts (avoid jumps or anything)
    self.strip.addClass('fpls-transition-off');

    //Listen to drag events
    self.wrapper.off('touchstart.ls-strip').on('touchstart.ls-strip', function(e) {
        self.touchstart(e);
    });
    self.wrapper.off('touchend.ls-strip').on('touchend.ls-strip', function(e) {
        self.touchend(e);
    });
    self.wrapper.off('touchmove.ls-strip').on('touchmove.ls-strip', function(e) {
        self.touchmove(e);
    });

    //Append the posts
    self.appendPosts(data);

    //For live campaigns, enable socket.IO
    if (document.readyState === 'complete') {
        self.initSocket();
    } else {
        $ls(window).on('load.ls', function() {
            self.initSocket();
        });
    }

    //Refresh strip when resizing the screen
    $ls(window).off('ls-delayed-resize.ls-strip').on('ls-delayed-resize.ls-strip', function() {
        self.refresh();
    });

}

/**
 * Method that refresh the strip
 */
LSWall_strip_default.prototype.refresh = function() {
    var self = this;
    self.navAppearance();
}

/**
 * Touch start handler
 * @param {Event} e: touch event
 */
LSWall_strip_default.prototype.touchstart = function(e) {
    var self = this;
    self.touchEnabled = true;
    var pageX = e.originalEvent.targetTouches[0].pageX;
    self.touchPosition = pageX;
    self.touchDelta = 0;
    self.strip.addClass('fpls-dragging'); //add class for dragging
}

/**
 * Touch move handler
 * @param {Event} e: touch event
 */
LSWall_strip_default.prototype.touchmove = function(e) {
    var self = this;
    var pageX = e.originalEvent.targetTouches[0].pageX;
    self.touchDelta = pageX - self.touchPosition;
    if (Math.abs(self.touchDelta) > 25) {
        e.preventDefault();
        self.strip.css({
            transform: "translateX(" + self.touchDelta / 2 + "px)"
        });
    }
}

/**
 * Touch end handler
 * @param {Event} e: touch event
 */
LSWall_strip_default.prototype.touchend = function(e) {
    var self = this;
    self.strip.removeClass('fpls-dragging'); //remove class for dragging
    self.touchEnabled = false;
    if (Math.abs(self.touchDelta) > 100) {
        if (self.touchDelta < 0) {
            self.scrollNext();
        } else {
            self.scrollPrev();
        }
    }
    self.strip.css({
        transform: "translateX(" + 0 + "px)"
    });
}

/**
 * Enables Socket.IO
 */
LSWall_strip_default.prototype.initSocket = function() {
    var self = this;
    if (!self.live) return;
    if (typeof WebSocket !== 'undefined') {
        self.socket = new fpls_nes.Client(LSConfig.ioURL);
        self.socket.connect({}, function() {
            self.socket.subscribe('/ws/front/wall/' + self.wall.wall_id, function(post) {
                if (post) self.appendPost(post, true);
            }, function(err) {});
        });
    }
}

/**
 * Method that appends posts to strip
 * @param {Object} data: wall JSON
 */
LSWall_strip_default.prototype.appendPosts = function(data) {
    var self = this;
    if (!data.posts) return;
    self.count = data.count; //update count

    data.posts.forEach(function(post) {
        var box = self.appendPost(post); //append each post
        box && box.removeClass('fpls-box--ssr'); //remove ssr class to boxes found
    });

    //remove SSR posts that are not in the feed anymore
    self.strip.find('.fpls-box--ssr').remove();

    //check next/prev arrows
    self.checkPagination();

    //trigger pagination
    $ls(window).trigger('ls-paginated');
}

/**
 * Helper function to keep count of loaded elements.
 * When count is zero, it calls LSWall.onComplete() method
 */
LSWall_strip_default.prototype._loadStackRemove = function() {
    var self = this;
    LSWall.prototype._loadStackRemove.call(self);
    if (self._loadStack < 1) {
        setTimeout(function() {
            LSHelpers.debug('Strip posts appended');
            self.navAppearance();
        }, 1);
    }
}

/**
 * Method that appends a single post to strip
 * @param {Object} post: post JSON
 * @param {Boolean} prepend: flag to determine if post is appended at the end or prepended at the beginning of the strip (for realtime)
 * @returns {DOM} box html
 */
LSWall_strip_default.prototype.appendPost = function(post, prepend) {
    var self = this;
    post.index = 0 + self.loaded; //add index to posts
    self.loaded++;
    var boxObj = new self.LSBox_post();

    //variable useful to determine appropriate image resolution
    var restrain_y = true;
    if (self.current_layout.square) restrain_y = false;

    //build unique ID for post
    if (post._id) post.uniqueId = 'ssr' + self.wall.wall_id + '_' + post._id;

    //initialize Box class
    boxObj.init(post, self, self.resolution || self.height, restrain_y);

    //get the box html
    var box = boxObj.getHtml();

    //set box id as data-attribute
    box.data('id', post._id);

    //set if box is featured or not as attribute
    box.data('featured', post.featured);
    self.boxes.push(box);

    //hide box while it's not loaded yet
    box.css({opacity: 0});

    //set margin
    if (self.margin) box.css({margin: self.margin / 2});

    //check if box is found SSR
    var found = false;
    if (self.isSSR && self.strip.find('#' + post.uniqueId).length > 0) {
        found = true;
    }

    //If SSR and page 1, missing posts should be added at the top
    if (self.isSSR && self.skip == 0) prepend = true;

    //If not found, append it
    if (!self.isSSR || !found) self.appendElement(box, prepend);

    //add box to load stack
    self._loadStackAdd();

    //Listen to load event of box
    boxObj.onLoad(function(w, h, error) {
        self._loadStackRemove();
        if (error) {
            //hide box in case of error
            box.hide();
            return;
        }

        //set correct sizes according to square or proportional image crop
        if (self.current_layout && self.current_layout.square) {
            boxObj.setSquare();
            self.setSquare(box, w, h);
        } else {
            self.setProportional(box, w, h);
        }

        //sum the box width to the strip
        if (!found) self.setStripWidth(box);

        //show the box
        box.css({opacity: 1});
    });

    //if strip has a custom destination, attach destination to click on boxes
    if (self.current_layout.destination_url) {
        box.off('click.ls');
        box.on('click.ls', function() {
            document.location.href = self.current_layout.destination_url;
        });
    }

    return box;
}

/**
 * Append box to strip
 * @param {DOM} box: box html
 * @param {Boolean} prepend: flag to determine if post is appended at the end or prepended at the beginning of the strip (for realtime)
 */
LSWall_strip_default.prototype.appendElement = function(box, prepend) {
    var self = this;
    //append box
    if (prepend) {
        self.strip.prepend(box);
    } else {
        self.strip.append(box);
    }

    //update nav appearance
    self.navAppearance();
}

/**
 * Set strip width according to box width
 * @param {DOM} box: box html
 */
LSWall_strip_default.prototype.setStripWidth = function(box) {
    var self = this;
    self.width = Math.ceil(self.width) + Math.ceil(box.outerWidth()) + Math.ceil(self.margin) + 0.1;
    self.strip.width(self.width);
}

/**
 * Check if pagination is needed according to post count and loaded count
 */
LSWall_strip_default.prototype.checkPagination = function() {
    var self = this;
    if (self.loaded >= self.count) {
        self.loadNext = false;
    } else {
        self.loadNext = true;
    }
}

/**
 * Load next page
 * If a paginate is triggered by clicking "next" inside a popup, it keeps track of the ID in order to show the next popup after pagination
 * @param {String} popup_id: ID of the post
 */
LSWall_strip_default.prototype.paginate = function(popup_id) {
    var self = this;
    if (self._paginatingPending) {
        return false;
    }
    self._paginatingPending = true;

    if (self.loadNext) {
        self.skip += self.current_options.limit;
        var url = self.getUrl(self.skip);
        self.showLoader();
        $ls.getJSON(url, function(data) {
            self.hideLoader();
            self.extractElements(data);
            self.appendPosts(data);
            self._paginatingPending = false;
            //if paginating from a popup, then open next image popup
            if (popup_id) self.next(popup_id);
        });
    } else {
        self._paginatingPending = false;
    }
}

/**
 * Prev method: it is used by the popup to show the prev image when clicking to the popup arrows
 * @param {String} id: ID of the current post open in popup
 */
 LSWall_strip_default.prototype.prev = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box) {
        if (box.data('id') == id) found = box;
    });
    if (found) var prev = found.prev('.fpls-box');
    if (prev && prev.length) prev.trigger('click.ls');
}

/**
 * Next method: it is used by the popup to show the next image when clicking to the popup arrows
 * @param {String} id: ID of the current post
 */
LSWall_strip_default.prototype.next = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box) {
        if (box.data('id') == id) found = box;
    });
    if (found) var next = found.next('.fpls-box');
    if (next && next.length) {
        next.trigger('click.ls');
    } else {
        self.paginate(id);
    }
}

/**
 * Method to determine if "prev" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
 LSWall_strip_default.prototype.prevAvailable = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box) {
        if (box.data('id') == id) found = box;
    });
    if (found) var prev = found.prev('.fpls-box');
    if (prev && prev.length) {
        return true;
    } else {
        return false;
    }
}

/**
 * Method to determine if "next" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
LSWall_strip_default.prototype.nextAvailable = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box) {
        if (box.data('id') == id) found = box;
    });
    if (found) var next = found.next('.fpls-box');
    if (next && next.length || self.loaded < self.count) {
        return true;
    } else {
        return false;
    }
}

/**
 * Method to set width and height of the box, with square proportions
 * @param {DOM} box: html node of the box
 * @param {Number} w: NOT USED
 * @param {Number} h: NOT USED
 */
LSWall_strip_default.prototype.setSquare = function(box, w, h) {
    var self = this;
    box.css({
        width: self.height,
        height: self.height
    });
}

/**
 * Method to set width and height of the box, keeping the original proportions
 * @param {DOM} box: html node of the box
 * @param {Number} w: image width, used to calculate correct proportions
 * @param {Number} h: image height, used to calculate correct proportions
 */
LSWall_strip_default.prototype.setProportional = function(box, w, h) {
    var self = this;
    if (w && h) {
        var width = self.height / h * w;
    } else {
        var width = self.height;
    }
    box.css({
        width: width,
        height: self.height
    });
}

/**
 * Scroll strip to the left
 */
LSWall_strip_default.prototype.scrollNext = function() {
    //calculate left position:
    var self = this;
    self.strip.removeClass('fpls-transition-off');

    self.left -= self.intContainer.width();
    self.left = self.snap(self.left);

    var maxLeft = self.intContainer.width() - self.strip.outerWidth();

    var roughPostsPerScroll = self.intContainer.width() / self.height;

    if (self.left <= maxLeft + self.height) {
        //arrived at the end of the strip, then load more posts.
        self.paginate();
        if (!self.loadNext) {
            //arrived at the end, so avoid white space
            self.left = maxLeft;
        } else if (self.loaded > self.count - roughPostsPerScroll) {
            self.left = self.snap(maxLeft - self.height);
        }
    }

    self.strip.css({
        left: self.left
    });

    self.navAppearance();

    // Analytics
    var payload = self.getAnalyticsPayload('strip-next-click');
    livestory_analytics.pushEvent(payload, {
        wall: self.wall,
        wallgroup: self.wallgroup
    });
}

/**
 * Scroll strip to the right
 */
 LSWall_strip_default.prototype.scrollPrev = function() {
    var self = this;
    self.strip.removeClass('fpls-transition-off');
    self.left += (self.intContainer.width() - 50);
    self.left = self.snap(self.left);
    if (self.left > 0) self.left = 0;
    self.strip.css({
        left: self.left
    });
    self.navAppearance();

    // Analytics
    var payload = self.getAnalyticsPayload('strip-prev-click');
    livestory_analytics.pushEvent(payload, {
        wall: self.wall,
        wallgroup: self.wallgroup
    });
}

/**
 * Snap strip so that first image is not cut
 * @param {Number} x: desired position of the strip
 * @returns {Number} rounded value
 */
LSWall_strip_default.prototype.snap = function(x) {
    var self = this;
    var snaps = [];
    var self = this;
    var new_x = 0;

    self.boxes.forEach(function(box) {
        if (!box.is(':visible')) return;
        var box_left = box.offset().left - self.margin / 2 - self.strip.offset().left;
        //var box_right = box_left + box.width();
        snaps.push(-box_left);
        //snaps.push(-box_right);
    });

    if (snaps.length < 2) return 0;

    //for each snap, find distance from next and prev
    for (var i=1; i<snaps.length; i++) {
        var snap = snaps[i];
        var snap_prev = snaps[i-1];

        //we want to balance it more toward the next, so we make an adjustment (we balance 80% for the next, and 20% for the prev)
        var adjust = Math.abs(snap - snap_prev) * (80-50)/100;

        //if adjust is too big, set it to zero
        if (adjust > self.intContainer.width() * 0.25) adjust = 0;

        //calculate the distance from previous and next, adjusting by the amount
        var distanceFromPrev = Math.abs(new_x - x) - adjust;
        var distanceFromNext = Math.abs(snap - x) + adjust;

        //find the closest snap
        if (distanceFromNext < distanceFromPrev) {
            new_x = snap;
        }
    }

    return new_x;
}

/**
 * Method that decide if next / prev arrows of strip should be visible or not
 */
LSWall_strip_default.prototype.navAppearance = function() {
    var self = this;

    var maxLeft = self.intContainer.width() - self.width;

    if (self.width < self.intContainer.width()) {
        self.left = maxLeft / 2;
    } else {
        self.left = Math.min(self.left, 0);
    }

    self.strip.css({
        left: self.left
    });

    if (self.left >= 0) {
        self.prevButton.addClass('fpls-disabled');
    } else {
        self.prevButton.removeClass('fpls-disabled');
    }

    if ((self.left > 0 && self.loaded >= self.count) || self.left <= maxLeft + 1) {
        self.nextButton.addClass('fpls-disabled');
    } else {
        self.nextButton.removeClass('fpls-disabled');
    }

}