/**
 * LSWall_grid_default
 * @constructor
 * Grid for UGC contents
*/
var LSWall_grid_default = function() {
    this.wallTpl = LSTemplates.wall_grid_default.wall;

    this.isotopeSettings = {
        itemSelector: '.fpls-box',
        hiddenStyle: {opacity: 0},
        visibleStyle: {opacity: 1},
        transitionDuration: "0",
        sortBy: "timestamp",
        sortAscending: false,
        percentPosition: true,
        isResizeBound: false,
        layoutMode: 'packery',
        stamp: '.fpls-stamp',
        stagger: 30,
        packery: {
            columnWidth: '.fpls-masonry-sizer'
        },
        getSortData: {
            timestamp: function(el) {
                var data = $ls(el).attr('data-timestamp');
                var date = new Date(data);
                return parseFloat(date.valueOf());
            },
            alt: function(el) {
                return $ls(el).attr('data-alt');
            }
        }
    }
}
LSHelpers.extend(LSWall_grid_default, LSWall);

/**
 * Method that loads and parse the handlebars template
 * It also caches some html elements
 */
LSWall_grid_default.prototype.loadTemplate = function() {
    var self = this;

    //create default strings for load more button
    var context = {};
    context.static = {
        load_more: 'Load More'
    }

    //if custom string for load more is defined, use it
    if (self.wall.uiLabels && self.wall.uiLabels.wallLoadMore) {
        context.static.load_more = self.wall.uiLabels.wallLoadMore;
    }

    if (self.isSSR) {
        self.container = self.wrapper.find('.fpls-wall-content');
        self.container.find('.fpls-box').addClass('fpls-box--ssr');
    } else {
        self.container = $ls(self.wallTpl(context));
        self.wrapper.html('').append(self.container);
    }

    self.intContainer = self.container.find('.fpls-feed-container');
    self.loadMore = self.container.find('.fpls-feed-loadmore .fpls-button');
}

/**
 * Method that renders the layout
 * @param {Object} data: wall JSON
 */
LSWall_grid_default.prototype.renderWall = function(data) {
    LSWall.prototype.renderWall.call(this, data);
    
    var self = this;

    // init
    self.skip = 0; //pagination parameter
    self.loaded = 0; //total amount of box loaded
    self.boxes = []; //array of DOM elements of boxes

    self.columns = 6; //default columns
    self.margin = 20; //default marign

    //get columns and margin from settings
    if (self.current_layout) {
        if (self.current_layout.columns)  self.columns = self.current_layout.columns;
        if (self.current_layout.margin > 0 || self.current_layout.margin === 0 || self.current_layout.margin === "0") self.margin = self.current_layout.margin;
    }

    //if LIVE, sort by timestamp
    if (self.live) {
        self.isotopeSettings.sortBy = "timestamp";
    }

    //If sort by alt text, sort ascending
    if (self.wall.options.sort_by  == 'accessibility_caption') {
        self.isotopeSettings.sortBy = 'alt';
        self.isotopeSettings.sortAscending = true;
    }

    //Clean html if not SSR
    if (!self.isSSR) {
        self.intContainer.html('');
    }

    //enable isotope
    self.enableIsotope(function() {
        self.appendPosts(data);
    });
    
    self.loadMore.on('click.ls', function() {
        self.paginate();
    });

    //For live campaigns, enable socket.IO
    if (document.readyState === 'complete') {
        self.initSocket();
    } else {
        $ls(window).on('load.ls', function() {
            self.initSocket();
        });
    }
}

/**
 * Enables Socket.IO
 */
LSWall_grid_default.prototype.initSocket = function() {
    var self = this;
    if (!self.live) return;
    if (typeof WebSocket !== 'undefined') {
        self.socket = new fpls_nes.Client(LSConfig.ioURL);
        self.socket.connect({}, function() {
            self.socket.subscribe('/ws/front/wall/' + self.wall.wall_id, function(post) {
                if (post) self.appendPost(post);
            }, function(err) {});
        });
    }
}

/**
 * Method that appends posts to grid
 * @param {Object} data: wall JSON
 */
LSWall_grid_default.prototype.appendPosts = function(data) {
    var self = this;
    if (!data.posts) return;
    self.count = data.count;

    //keep track of the amount of posts to load
    var n = Math.min(data.posts.length, self.current_options.limit);

    var refreshTimeout = 0;

    for (var i = 0; i < Math.min(data.posts.length, self.current_options.limit); i++) {
        var box = self.appendPost(data.posts[i], function() {
            //this logic is to load refresh a little bit after the last element is loaded, but only once
            n--;
            if (n < 1) {
                clearTimeout(refreshTimeout);
                refreshTimeout = setTimeout(function() {
                    self.refresh();
                }, 100);
            }
            clearTimeout(refreshTimeout);
            refreshTimeout = setTimeout(function() {
                self.refresh();
            }, 500);
        });
        box && box.removeClass('fpls-box--ssr'); //remove ssr class to boxes found
    }

    //remove SSR posts that are not in the feed anymore
    self.intContainer.find('.fpls-box--ssr').remove();

    self.paginationVisibility();
    $ls(window).trigger('ls-paginated');
}

/**
 * Method that appends a single post to grid
 * @param {Object} post: post JSON
 * @param {Function} callback: function to be executed after the post has been appended (or in case of error)
 * @returns {DOM} box html
 */
LSWall_grid_default.prototype.appendPost = function(post, callback) {
    var self = this;
    post.index = 0 + self.loaded; //add index to posts
    self.loaded++;
    var boxObj = new self.LSBox_post();

    var resolution = self.optimalResolution(post);

    if (post._id) post.uniqueId = 'ssr' + self.wall.wall_id + '_' + post._id;

    boxObj.init(post, self, resolution);
    var box = boxObj.getHtml();
    box.data('id', post._id);
    box.data('featured', post.featured);
    box.data('timestamp', moment(post.timestamp).unix());
    self.boxes.push(box);

    self._loadStackAdd();
    boxObj.onLoad(function(w, h, error) {
        self._loadStackRemove();
        if (!error) {
            self.setBoxStyle(box);
            if (self.current_layout && self.current_layout.square) {
                boxObj.setSquare();
            }
            self.appendElement(box, callback);
        } else {
            LSHelpers.debug('error loading post', post);
            callback();
        }
    });

    //assign a reference to item in box DOM node (not the best, but ¯\_(ツ)_/¯)
    box.item = boxObj.item;

    return box;
}

/**
 * Append box to grid
 * @param {DOM} box: box html
 * @param {Function} callback: function to be executed after the post has been appended
 */
LSWall_grid_default.prototype.appendElement = function(box, callback) {
    var self = this;
    setTimeout(function() {
        var found = false;
        if (!self.isSSR || !box.data('ssr-found')) {
            self.isotopeInstance && self.isotopeInstance.insert(box);
        }
        if (self.popup_id) {
            //if I'm paginating from a popup
            self.next(self.popup_id);
            self.popup_id = false;
        }
        callback && callback();
    }, 1);
}

/**
 * Make load more button visible or not according to post count
 */
LSWall_grid_default.prototype.paginationVisibility = function() {
    var self = this;
    if (self.loaded >= self.count) {
        self.loadMore.hide();
    } else {
        self.loadMore.show();
    }
}

/**
 * Load next page
 * If a paginate is triggered by clicking "next" inside a popup, it keeps track of the ID in order to show the next popup after pagination
 * @param {String} popup_id: ID of the post
 */
LSWall_grid_default.prototype.paginate = function(popup_id) {
    var self = this;
    self.popup_id = popup_id;
    if (self.loaded < self.count) {
        self.skip += self.current_options.limit;
        var url = self.getUrl(self.skip);
        self.showLoader();
        //when paginating ssr, ignore ssr
        self.isSSR = false;
        $ls.getJSON(url, function(data) {
            self.hideLoader();
            self.extractElements(data);
            self.appendPosts(data);
        });
    }
}

/**
 * Enable isotope plugin
 * @param {Function} callback: function called when isotope is loaded
 */
LSWall_grid_default.prototype.enableIsotope = function(callback) {
    var self = this;
    self.sizer = $ls('<div class="fpls-masonry-sizer"></div>');
    self.intContainer.append(self.sizer);
    self.setSizerWidth();
    LSHelpers.appendJs(LSConfig.assetURL + '/js/grid.js', function() {
        self.isotopeInstance = new Isotope( self.intContainer[0], self.isotopeSettings);
        callback();
        $ls(window).on('ls-delayed-resize', function() {
            self.refresh();
        });
    });
}

/**
 * Prev method: it is used by the popup to show the prev image when clicking to the popup arrows
 * @param {String} id: ID of the current post open in popup
 */
LSWall_grid_default.prototype.prev = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box, index) {
        if (box.data('id') == id) found = index;
    });
    if (typeof found !== 'undefined' && found > 0) var prev = self.boxes[found - 1];
    if (prev && prev.length) prev.trigger('click.ls');
}

/**
 * Next method: it is used by the popup to show the next image when clicking to the popup arrows
 * @param {String} id: ID of the current post
 */
 LSWall_grid_default.prototype.next = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box, index) {
        if (box.data('id') == id) found = index;
    });
    if (typeof found !== 'undefined' && found < self.boxes.length) var next = self.boxes[found + 1];
    if (next && next.length) {
        next.trigger('click.ls');
    } else {
        self.paginate(id);
    }
}

/**
 * Method to determine if "prev" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
LSWall_grid_default.prototype.prevAvailable = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box, index) {
        if (box.data('id') == id) found = index;
    });
    if (typeof found !== 'undefined' && found > 0) var prev = self.boxes[found - 1];
    if (prev && prev.length) {
        return true;
    } else {
        return false;
    }
}

/**
 * Method to determine if "next" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
LSWall_grid_default.prototype.nextAvailable = function(id) {
    var self = this;
    var found;
    self.boxes.forEach(function(box, index) {
        if (box.data('id') == id) found = index;
    });
    if (typeof found !== 'undefined' && found < self.boxes.length) var next = self.boxes[found + 1];
    if (next && next.length || self.loaded < self.count) {
        return true;
    } else {
        return false;
    }
}

/**
 * Method that refresh the grid
 */
LSWall_grid_default.prototype.refresh = function() {
    // Refresh masonry layout when tab is showed again
    var self = this;
    self.setSizerWidth();
    self.isotopeInstance && self.isotopeInstance.arrange();
    LSHelpers.debug('refresh LS grid');
}

/**
 * Method to set box style
 * If featured, make it twice as big
 * @param {DOM} box: html node of the box
 */
LSWall_grid_default.prototype.setBoxStyle = function(box) {
    var self = this;
    if (self.columns) {
        box.width(self.sizer.data('width') + '%');
        // set double size if featured is enabled
        if (self.current_layout && self.current_layout.featured && box.data('featured'))
            box.width(self.sizer.data('featured-width') + '%');
    }
    if (self.margin) box.find('.fpls-box-inner').css({margin: self.margin / 2});
}

/**
 * Method to set column width in percent
 * Isotope requires a n html to measure (self.sizer)
 */
LSWall_grid_default.prototype.setSizerWidth = function() {
    var self = this;

    if (self.wall.subwalls && self.wall.subwalls.length > 0) {
        //If using the new responsive feature, just set the width according to the current breakpoint
        var width = 100 / self.columns;
    } else {
        //Keep this for backward compatibility
        var width = 100;
        if (self.intContainer.outerWidth(true, true) > 1024) {
            width = 100 / self.columns;
        } else if (self.intContainer.outerWidth(true, true) > 768) {
            width = 33.33;
        } else if (self.intContainer.outerWidth(true, true) > 480) {
            width = 50;
        } else {
            width = 100;
        }

        //new support for mobile
        if (self.current_layout && self.current_layout.columns_mobile && self.intContainer.outerWidth(true, true) < 768) {
            width = 100 / self.current_layout.columns_mobile;
        }
    }

    // set grid width
    self.sizer.width(width + '%');
    self.sizer.data('width', width);

    // set double size only if not responsive
    if (width <= 50) {
        self.sizer.data('featured-width', width * 2);
    } else {
        self.sizer.data('featured-width', width);
    }

    // set box width
    self.boxes.forEach(function(box) {
        self.setBoxStyle(box);
    });
}

/**
 * Method to calculate correct size of posts in grid
 * @param {Object} post: post JSON
 * @returns size in pixel
 */
LSWall_grid_default.prototype.optimalResolution = function(post) {
  var self = this;
  if (!self.sizer) return 1000;
  var pixels = self.sizer.outerWidth() * 1.25;
  if (self.current_layout && self.current_layout.featured && post.featured) {
    pixels = pixels * 2;
  }

  return pixels;
}
