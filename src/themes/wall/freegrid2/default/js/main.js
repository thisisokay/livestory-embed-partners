/**
 * LSWall_freegrid2_default
 * @constructor
 * Class for whiteboard templates
*/
var LSWall_freegrid2_default = function() {
    this.wallTpl = LSTemplates.wall_freegrid2_default.wall;
}
LSHelpers.extend(LSWall_freegrid2_default, LSWall);

/**
 * Method to load template
 */
LSWall_freegrid2_default.prototype.loadTemplate = function(context) {
    //return;
    var self = this;
    if (self.isSSR) {
        self.container = self.wrapper.find('> .fpls-wall-content');
    } else {
        self.container = $ls(self.wallTpl(context));
        self.wrapper.html('').append(self.container);
    }
}

/**
 * Method that renders the layout
 * Called each time a breakpoint switches
 * @param {Object} data: wall JSON
 */
LSWall_freegrid2_default.prototype.renderWall = function(data) {
    var self = this;

    LSWall.prototype.renderWall.call(this, data);

    //find grids
    self.grid = self.container.find('> .fpls-wall-grid');

    self.ls_grids = [];

    //clean-up existing styles
    $ls('style[data-style-id="' + self.wall.wall_id + '"]').attr('data-style-remove', 1);

    // LST-2987 (lazy loading)
    var config = {
        root: null,
        rootMargin: '10px 0px',
        threshold: 0
    }

    self.imgObserver = LSHelpers.createObserver(function(entry) {

        var $img = $ls(entry.target);
        var imgSrcset = $img.attr('lazy-srcset');
        $img.removeAttr('lazy-srcset');
        $img.attr('srcset', imgSrcset);

        var src = $img.attr('lazy-src');

        if (src) {
            $img.removeAttr('lazy-src');
            $img.attr('src', src);
        }

    }, config);

    self.videoObserver = LSHelpers.createObserver(function(entry) {

        var $video = $ls(entry.target);
        // Video poster
        var posterSrc = $video.attr('lazy-poster');
        $video.removeAttr('lazy-poster');
        $video.attr('poster', posterSrc);
        // Video source
        var source = $video.find('source');
        var src = source.attr('lazy-src');
        source.removeAttr('lazy-src');
        source.attr('src', src);

        // load the video:
        // The load() method is used to update the audio/video element after changing the source or other settings.
        $video[0].load();


    }, config);

    self.appended = [];

    self.boxes = [];

    if (self.current_sections && self.current_sections.length > 0) {
        if (!self.isSSR) self.grid.html('');

        //inject styles at layout level
        self.injectFontFaces(self.cssBrekapointPrefix + ' .fpls-element .fpls-box', self.wall.font_heading, self.wall.font_body);
        if (self.current_layout.textstyles) LSHelpers.injectTextstyles(self, self.cssBrekapointPrefix + ' .fpls-element .fpls-box', self.current_layout.textstyles, self.current_layout.effects);
        if (self.current_layout.overlays) self.injectOverlays(self.cssBrekapointPrefix + ' .fpls-element .fpls-box', self.current_layout.overlays, self.current_layout.effects);
        if (self.current_layout.effects) self.injectEffects(self.cssBrekapointPrefix + ' .fpls-element .fpls-box', self.current_layout.effects);

        //compare sections with old sections
        var existing_section_ids = [];
        var online_section_ids = [];
        
        self.grid.find(' > .fpls-section, > [id^="ls-section-"]').each(function() {
            existing_section_ids.push($ls(this).attr('id').replace('ls-section-', ''));
        });

        for (var i=0; i < self.current_sections.length; i++) {
            online_section_ids.push(self.current_sections[i]._id);
        }

        if (self.isSSR && existing_section_ids.join(',') != online_section_ids.join(',')) {
            //SSR and sections are not aligned, reset the grid.
            LSHelpers.warn('Sections mismatch', existing_section_ids, online_section_ids);
            self.isSSR = false;
            self.grid.html('');
        }

        for (var i=0; i < self.current_sections.length; i++) {
            var section = self.current_sections[i];
            self.renderSection(section, i);
        }

    } else {
        if (self.isSSR) {
            var gridElement = self.wrapper.find('.fpls-grid');
        } else {
            self.grid.html('');
            var gridElement = $ls('<div class="fpls-grid" />');
            gridElement.appendTo(self.grid);
        }
        self.renderLayout(self.grid, gridElement, self.current_layout);
    }

    $ls(window).on('ls-delayed-resize', function() {
        self.refresh();
    });

    $ls(window).on('ls-css-change', function() {
        $ls('style[data-style-id="' + self.wall.wall_id + '"][data-style-remove]').remove();
        self.refresh();
    });

    setTimeout(function() {
        self.detectHash.call(self);
    }, 500);

}

/**
 * Renders a single section (row)
 * @param {Object} section: section JSON
 */
LSWall_freegrid2_default.prototype.renderSection = function(section, index) {
    var self = this;
    if (self.isSSR) {
        var sectionDiv = self.wrapper.find('#ls-section-' + section._id);
    } else {
        var sectionDiv = $ls('<div id="ls-section-' + section._id + '" class="fpls-section fpls-section-' + section._id + ' fpls-' + self.wall.wall_id + section._id + '"><div class="fpls-grid--outer"><div class="fpls-grid"></div></div>');
        if (section.css_class) sectionDiv.addClass(section.css_class);
        sectionDiv.appendTo(self.grid);
    }

    var gridElement = sectionDiv.find('.fpls-grid');
    var gridOuter = sectionDiv.find('.fpls-grid--outer');

    if (section.layout && section.layout.maxwidth) {

        //if number without measures, add px
        var unit = '';
        if (parseFloat(section.layout.maxwidth).toString() == section.layout.maxwidth.toString()) unit = 'px';

        gridOuter.css({
            'max-width': section.layout.maxwidth + unit,
            'margin-left': 'auto',
            'margin-right': 'auto'
        });
    }

    if (section.wall_id) {
        //Render sub-wall
        var container;
        if (gridElement.find('#ls-' + section.wall_id).length < 1) {
            container = $ls('<div/>');
            gridElement.append(container);
        } else {
            container = gridElement.find('#ls-' + section.wall_id);
        }

        container.attr('id', 'ls-' + section.wall_id);
        container.attr('data-id', section.wall_id);
        container.attr('data-store', self.mainContainer.attr('data-store'));
        container.attr('data-lang', self.mainContainer.attr('data-lang'));

        //Check if payload is in the reply
        var external_wall;
        
        if (self.wall.walls) external_wall = self.wall.walls.find(function(w) {
            return w.wall_id == section.wall_id;
        });

        //inherit ls watermark settings
        if (external_wall) external_wall.ls_watermark = self.wall.ls_watermark;

        new LiveStory("ls-" + section.wall_id, {
            type:"wall",
            parent_wall: self.wall,
            rootContainer: self.mainContainer, //root container
            json: external_wall
        });
    } else {
        self.renderLayout(sectionDiv, gridElement, section.layout, section);
    }
}

/**
 * Renders the actual grid (one for each section in case of multi-row layouts)
 * @param {DOM} containerElement: outer div that will contain the rid
 * @param {DOM} gridElement: actual grid div
 * @param {Object} layout: JSON of the entire layout
 * @param {Object} section (optional): JSON of the specific section
 */
LSWall_freegrid2_default.prototype.renderLayout = function(containerElement, gridElement, layout, section) {
    var self = this;

    var section_id = ""; //section id
    var sectionPrefix = ""; //css selector for section
    if (section) {
        section_id = section._id;
        sectionPrefix = ' .fpls-section-' + section_id;
    }

    gridElement.attr('data-width', layout.width || self.current_layout.width); //width is global
    gridElement.attr('data-height', layout.height);

    if (layout.height_source) {
        var source = self.getItemId(null, layout.height_source, section_id);
        gridElement.attr('data-height-source', source);
    }

    gridElement.width(self.grid.width());

    var backgroundObj = Object.assign({}, self.backgroundObj(self.wall.layout && self.wall.layout.background), self.backgroundObj(layout.background));

    self.setBackground(containerElement, backgroundObj);

    var ls_grid = new LSGrid(gridElement, {
        enabled: false,
        contentClass: '.fpls-content',
        elementClass: '.fpls-element',
        carouselClass: '.fpls-carousel',
        carouseledClass: '.fpls-carousel-item',
        groupClass: '.fpls-group',
        groupedClass: '.fpls-grouped',
        compareClass: '.fpls-compare',
        comparedClass: '.fpls-compare__item',
    }, self.mainContainer);

    //Inject font CSS for this section
    if (section) {
        self.injectFontFaces(self.cssBrekapointPrefix + sectionPrefix + ' .fpls-element .fpls-box', section.font_heading, section.font_body);
        if (section.customcss) LSHelpers.injectInlineCss(section.customcss, self.cssBrekapointPrefix + sectionPrefix, self.container, self.wall.theme);
    }
    if (layout.textstyles) LSHelpers.injectTextstyles(self, self.cssBrekapointPrefix + sectionPrefix + ' .fpls-element .fpls-box', layout.textstyles, layout.effects);
    if (layout.overlays) self.injectOverlays(self.cssBrekapointPrefix + sectionPrefix + ' .fpls-element .fpls-box', layout.overlays, layout.effects);
    if (layout.effects) self.injectEffects(self.cssBrekapointPrefix + sectionPrefix + ' .fpls-element .fpls-box', layout.effects);

    var _grid = {
        ls_grid: ls_grid,
        layout: layout,
        gridElement: gridElement,
        containerElement: containerElement
    };

    //add section id
    if (section_id) _grid.section_id = section_id;

    self.ls_grids.push(_grid);

    self.setZoom();

    self.appendNodes(layout, ls_grid);

    var event_prefix = '.' + self.wall.wall_id + section_id + (self.current_breakpoint || 'default');

    /*
    $ls(window).off('ls-delayed-resize' + event_prefix).on('ls-delayed-resize' + event_prefix, function() {
        self.appendNodes(layout, ls_grid);
    });
    */

    $ls(window).off('ls-delayed-scroll' + event_prefix).on('ls-delayed-scroll' + event_prefix, function() {
        self.appendNodes(layout, ls_grid);
    });
}

/**
 * Append nodes to the grid
 * @param {Object} layout: JSON with the items to append
 * @param {DOM} ls_grid: div where to append the new elements
 */
LSWall_freegrid2_default.prototype.appendNodes = function(layout, ls_grid) {
    var self = this;

    var offset = ls_grid.container.offset().top;

    if (layout.grid) layout.grid.forEach(function(item) {

        function _findSubitemCssClasses(item) {
            item.cssclass_tree = item.cssclass_tree || [];
            if (item.cssclass && item.cssclass_tree) item.cssclass_tree.push(item.cssclass);
            if (item.subitems) {
                item.subitems.forEach(function(subitem) {
                    if (item.cssclass_tree) {
                        subitem.cssclass_tree = subitem.cssclass_tree || [];
                        subitem.cssclass_tree = subitem.cssclass_tree.concat(item.cssclass_tree);
                    }
                    _findSubitemCssClasses(subitem);
                });
            }
        }

        _findSubitemCssClasses(item);

        var item_id = self.getItemId(item);
        var appended = false;
        if (self.appended.indexOf(item_id) >= 0) appended = true;
        if (appended) return;
        //do not append element if out of viewport
        if (offset + item.top * ls_grid.options.zoom > $ls(window).scrollTop() + $ls(window).outerHeight() * 2.5 && //TODO: item top must take section top into consideration
            item.type !== 'block') return;
        if (item.node == 'group') {
            self.appendGroup(item, ls_grid.container, false);
        } else {
            self.appendNode(item, ls_grid.container, false);
        }
        self.appended.push(item_id);
    });
    ls_grid.init();
    var els = self.container.find('a[href],[data-href]');
    livestory_sfcc.processUrls(els);

    // LST-2987 (lazy loading)
    //observe imgs
    LSHelpers.initObserver(self.container.find('img[lazy-srcset],img[lazy-src]'), self.imgObserver);

    //observe slides
    LSHelpers.initObserver(self.container.find('.fpls-carousel-item').find('img[lazy-srcset],img[lazy-src]'), self.imgObserver);

    // observe videos
    LSHelpers.initObserver(self.container.find('video[lazy-poster]'), self.videoObserver);

}

LSWall_freegrid2_default.prototype.parseNode = function(item, parent) {
    var self = this;

    switch (item.type) {
        case 'photo':
        case 'video':
        case 'text':
        case 'mixed':
            // post block
            if (!self.posts[item.id]) return;
            var _item = $ls.extend({}, item, self.posts[item.id]);
            _item.default_image_url = item.img;
            _item._id = item._id;
            var boxObj = new self.LSBox_post();
            var resolution = self.optimalResolution(_item, parent);
            boxObj.init(_item, self, resolution);
            break;
        case 'asset':
            // asset block
            if (!self.products[item.id]) return;
            var _item = $ls.extend({}, item, self.products[item.id]);
            var boxObj = new self.LSBox_asset();
            var resolution = self.optimalResolution(_item, parent);
            boxObj.init(_item, self, resolution);
            break;
        case 'block':
            // custom text block
            var boxObj = new self.LSBox_block();
            boxObj.init(item, self);
            break;
        case 'embed':
            // custom text block
            var boxObj = new self.LSBox_embed();
            var resolution = self.optimalResolution(item, parent);

            boxObj.init(item, self, resolution);
            break;
        case 'icon':
            // custom icon block
            var boxObj = new self.LSBox_icon();
            boxObj.init(item, self);
            break;

    }

    if (!boxObj) return;

    var box = boxObj.getHtml();
    // add specific attributes for gridster
    box.data('id', item.id);
    box.data('type', item.type);
    box.data('popup_disabled', item.popup_disabled);

    //fix array link
    if (item.link && item.link.length == 0) {
        item.link = "";
    }

    box.data('link', item.link);

    box.addClass('fpls-content');

    return {
        el: box,
        obj: boxObj
    }
}

LSWall_freegrid2_default.prototype.appendNode = function(item, parentElement, additionalClass, parent) {
    var self = this;

    if (!item.width && !additionalClass) return;

    var item_id = self.getItemId(item);
    if (item_id) item.uniqueId = self.getItemSsrId(item);

    var found = false;
    if (self.isSSR) {
        var element = self.grid.find('#_' + item_id);
        if (element.length  > 0) {
            found = true;
        } else LSHelpers.debug('SSR element not found', '#_' + item_id);
    }
    if (!found) {
        var element = $ls('<div class="fpls-element" />');
        element.attr('id', '_' + item_id);
        element.appendTo(parentElement);
    }

    if (additionalClass) element.addClass(additionalClass);
    //element.css({opacity: 0});

    element.addClass('fpls-element-loading');

    if (!item.slides) item.slides = 1;

    //add custom css class
    if (item.cssclass) element.addClass(item.cssclass);

    //add data-item-class to identify blocks with css class
    if (item.cssclass) element.attr('data-item-class', item.cssclass);

    //add data-item-id to block (to allow specific targeting)
    if (item._id) element.attr('data-item-id', item._id);

    if (item.type == 'carousel' && item.subitems) {
        self.handleCarousel(element, parentElement, item);
        if (item.type != 'photo' && item.type != 'video') self.setBackground(element, item.background, '.fpls-box-carousel');
    } else if (item.type == 'compare' && item.subitems) {
        self.handleCompare(element, item);
    } else {
        var box = self.parseNode(item, parent);
        if (!box) return;

        //append if wall is not SSR or box is not SSR
        if (!self.isSSR || !box.obj.isSSR) {
            box.el.appendTo(element);
        }

        //element.css({opacity: 0});

        self._loadStackAdd();

        //handle initial state of animation before load
        self.handleAnimationInitialState(element, item);

        box.obj.onLoad(function(w, h, error) {
            self._loadStackRemove();
            if (error) {
                LSHelpers.error(error);
                return;
            }
            //element.css({opacity: 1});

            element.removeClass('fpls-element-loading');
            element.addClass('fpls-element-loaded');

            self.fillBox(box.el, item.crop);
            self.handleAnimation(element, item);
        });

        self.boxes.push(box.el);
        if (item.type != 'photo' && item.type != 'video') self.setBackground(element, item.background, '.fpls-box-inner');
    }

    self.injectFontFaces(self.cssBrekapointPrefix + ' #_' + item_id, item.font_heading, item.font_body);
    if (item.type != 'carousel') LSHelpers.injectTextstyles(self, self.cssBrekapointPrefix + ' #_' + item_id, item.textstyles, item.effects);
    self.injectAlignmentStyles(self.cssBrekapointPrefix + ' #_' + item_id, item.alignment);
    self.injectOverlays(self.cssBrekapointPrefix + ' #_' + item_id, item.overlays, item.effects);
    self.injectEffects(self.cssBrekapointPrefix + ' #_' + item_id, item.effects);
    self.injectFilter(self.cssBrekapointPrefix + ' #_' + item_id, item.filter || {});

    self.setAttributes(element, item);
    //note: do not apply background if image or video
    self.setBorder(element, item.border);

}

LSWall_freegrid2_default.prototype.postRender = function() {
    var self = this;
    LSWall.prototype.postRender.call(this);
}

LSWall_freegrid2_default.prototype.backgroundObj = function(obj) {
    var self = this;

    if (obj) {
        Object.keys(obj).filter(function(key) { return obj[key] === null || obj[key] === '' }).forEach(function (key) { delete(obj[key]) });
        return obj;
    }
}

LSWall_freegrid2_default.prototype.getItemId = function(item, id, section_id) {
    var self = this;
    if (!id) {
        id = item._id;
    }
    if (!section_id && item) section_id = item.section_id;
    if (self.legacyMode()) return id; //legacy mode!
    var wall_id_length = 9;

    //FIX ID LENGTH
    try {
        if (!self.LS_FIXED_LENGTH) {
            var __ssr_id = self.container.find('.fpls-element .fpls-box[id*="ssr"]').eq(0).attr('id');
            if (__ssr_id) wall_id_length = __ssr_id.split('_')[0].replace('ssr', '').length;
            self.LS_FIXED_LENGTH = wall_id_length;
        } else {
            wall_id_length = self.LS_FIXED_LENGTH;
        }
    } catch (err) {
        LSHelpers.log(err);
    }

    var item_id = self.wall.wall_id.substr(0,wall_id_length) + "_";
    if (id) item_id += id;
    if (section_id) item_id += '_' + section_id;

    return item_id;
}

LSWall_freegrid2_default.prototype.legacyMode = function() {
    var self = this;
    return !(typeof self.wall.sections !== 'undefined' && self.wall.sections.length > 0);
}

LSWall_freegrid2_default.prototype.getItemSsrId = function(item) {
    var self = this;
    if (self.legacyMode()) return 'ssr' + self.wall.wall_id + '_' + item._id; //legacy mode!
    var item_id = self.getItemId(item);
    return 'ssr' + item_id;
}

/**
 * Method that creates a carousel
 * @param {DOM} element: element that will contain the carousel
 * @param {DOM} parentElement: parent element of the carousel
 * @param {Object} item: item JSON
 * @returns {DOM} carousel html
 */
LSWall_freegrid2_default.prototype.handleCarousel = function (element, parentElement, item) {
    var self = this;

    //create carousel html
    var found = false;
    if (self.isSSR) {
        var elementPrev = element.find('.fpls-element-prev');
        var elementNext = element.find('.fpls-element-next');
        var slider = element.find('.fpls-carousel-inner');
        if (slider.length > 0) found = true;
    }

    if (!found) {
        var elementPrev = $ls('<button class="fpls-element-prev">Previous</button>');
        var elementNext = $ls('<button class="fpls-element-next">Next</button>');
        elementPrev.appendTo(element);
        element.addClass('fpls-carousel');
        var slider = $ls('<div class="fpls-carousel-inner" />');
        slider.appendTo(element);
        elementNext.appendTo(element);
        slider.wrap('<div class="fpls-box-carousel" />');
    }

    self.fillTrackingCodes(item);

    if (item && item.fade_enabled) {
        element.addClass('fpls-fade-carousel');

        if (item.fade_transition) element[0].style.setProperty('--fadeTransition', item.fade_transition + 's');
    }

    item.auto_seconds = item.auto_seconds || 3;

    //Prefill stepper if not set
    if (!item.stepper) {
        item.stepper = item.slides;
        if (item.dots) item.stepper = 1;
    }

    //create dots html
    if (item.dots) {
        var dotsContainer = $ls('<div class="fpls-dots-container"></div>');
        dotsContainer.appendTo(element);

        var dotsToAppend = (item.subitems.length - item.slides + item.stepper) / item.stepper;
        var roundedDotsToAppend = Math.ceil(dotsToAppend);

        for (var i=1; i<= roundedDotsToAppend; i++) {
            var dot = $ls('<span class="fpls-dot"></span>');
            dot.appendTo(dotsContainer);
        }
    }

    //hide arrows
    if (item.hide_arrows) {
        elementPrev.hide();
        elementNext.hide();
    }

    //create child elements and inject them into 
    item.subitems && item.subitems.forEach(function(subitem, index) {
        // LST-2987 set offscreen slides
        if (self.wall.themeOptions && self.wall.themeOptions.carouselLazyLoad) {
            if (item.padding_right && (index > item.slides)) { // append visibile slides + 1
                subitem.carouselLazyLoad = true;
            } else if (index > (item.slides - 1)) { //append visible slide - 1 (start from 0)
                subitem.carouselLazyLoad = true;
            }
        }

        if (subitem.node === 'group') {
            self.appendGroup(subitem, slider, 'fpls-carousel-item');
            if (index == 0) {
                //element.css({opacity: 1});
                self.handleAnimation(element, item);
            }
        } else {
            self.appendNode(subitem, slider, 'fpls-carousel-item', item);
        }
    });

    var setCurrentSlide = 1;

    //ANALYTICS: carousel-slide-impression
    var payload = self.getAnalyticsPayload('carousel-slide-impression', item);
    payload.wall_slide_number = setCurrentSlide;
    livestory_analytics.pushEvent(payload, {
        item: item,
        wall: self.wall,
        wallgroup: self.wallgroup
    });

    elementNext.on('click fplsnext', function() {
        var next = self.carouselMoveSlider(element, item.stepper, item);
        self.carouselSetCurrent(element, next, slider, item);
        self.carouselAuto(element, item);

        if (!item.auto) {
            // LST-1205
            var payload = self.getAnalyticsPayload('carousel-next-click', item);
            payload.wall_slide_number = next.index() + setCurrentSlide;
            livestory_analytics.pushEvent(payload, {
                item: item,
                wall: self.wall,
                wallgroup: self.wallgroup
            });

            // LST-1205
            var payload = self.getAnalyticsPayload('carousel-slide-impression', item);
            payload.wall_slide_number = next.index() + setCurrentSlide;
            livestory_analytics.pushEvent(payload, {
                item: item,
                wall: self.wall,
                wallgroup: self.wallgroup
            });
        }

    });

    elementPrev.on('click', function() {
        var next = self.carouselMoveSlider(element, -item.stepper, item);
        self.carouselSetCurrent(element, next, slider, item);
        self.carouselAuto(element, item);

        if (!item.auto) {
            // LST-1205
            var payload = self.getAnalyticsPayload('carousel-slide-impression', item);
            payload.wall_slide_number = next.index() + setCurrentSlide;
            livestory_analytics.pushEvent(payload, {
                item: item,
                wall: self.wall,
                wallgroup: self.wallgroup
            });

            // LST-1205
            var payload = self.getAnalyticsPayload('carousel-prev-click', item);
            payload.wall_slide_number = next.index() + setCurrentSlide;
            livestory_analytics.pushEvent(payload, {
                item: item,
                wall: self.wall,
                wallgroup: self.wallgroup
            });
        }
    });

    //manage dots click
    if (item.dots) dotsContainer.on('click', '.fpls-dot', function() {
        var index = $ls(this).index();
        var next = self.carouselNextSlideByDot(index, element, item);
        self.carouselSetCurrent(element, next, slider, item);
        self.carouselAuto(element, item);

        // LST-1205
        var payload = self.getAnalyticsPayload('carousel-slide-impression', item);
        payload.wall_slide_number = index + setCurrentSlide;
        livestory_analytics.pushEvent(payload, {
            item: item,
            wall: self.wall,
            wallgroup: self.wallgroup
        });
    });

    if (element.find('.fpls-carousel-item').length <= item.slides) {
        elementNext.hide();
        elementPrev.hide();
    }

    self.carouselSetCurrent(element, slider.find('.fpls-carousel-item').eq(0), slider, item);
    self.carouselAuto(element, item);

    self.carouselTouchEvents(element, item);

    //ANALYTICS: carousel-impression
    var payload = self.getAnalyticsPayload('carousel-impression');
    livestory_analytics.pushEvent(payload, {
        item: item,
        wall: self.wall,
        wallgroup: self.wallgroup
    });

    // LST-1205
    slider.on('mouseenter.ls', function(){
        var payload = self.getAnalyticsPayload('carousel-hover', item);
        livestory_analytics.pushEvent(payload, {
            item: item,
            wall: self.wall,
            wallgroup: self.wallgroup
        });
    })

    // LST-1205
    slider.on('click.ls', function(e){
        var target = $ls(e.target);
        var payload = self.getAnalyticsPayload('carousel-click', item);
        try {
            var currentSlide = target.closest('.fpls-carousel-item');
            if (currentSlide.length > 0) {
                var currentSlideIndex = slider.find('.fpls-carousel-item').index(currentSlide);
                payload.wall_slide_number = currentSlideIndex + 1;
            }
        } catch(err) {}

        livestory_analytics.pushEvent(payload, {
            item: item,
            wall: self.wall,
            wallgroup: self.wallgroup
        });
    })

    // LST-1205
    element.find('.fpls-carousel-item').each(function() {
        var _timer;
        $ls(this).on('mouseenter.ls', function(){
            clearTimeout(_timer);
            _timer = setTimeout(function() {
                var payload = self.getAnalyticsPayload('carousel-slide-hover', item);
                payload.wall_slide_number = $ls(this).index() + setCurrentSlide;
                livestory_analytics.pushEvent(payload, {
                    item: item,
                    wall: self.wall,
                    wallgroup: self.wallgroup
                });
            }, 100);
        });
    });

    return element;
}

/**
 * Method that attach touch events to carousels
 * @param {DOM} element: element that will contain the carousel
 * @param {Object} item: item JSON
 */
LSWall_freegrid2_default.prototype.carouselTouchEvents = function (element, item) {
    var self = this;

    var slider = element.find('.fpls-carousel-inner');
    element.off('touchstart.ls-carousel').on('touchstart.ls-carousel', function(e) {
        self.carouselTouchStart(element, slider, e);
        LSHelpers.debug('carousel: touchstart');
    });
    element.off('touchend.ls-carousel').on('touchend.ls-carousel', function(e) {
        self.carouselTouchEnd(element, slider, e);
        LSHelpers.debug('carousel: touchend');
    });
    element.off('touchmove.ls-carousel').on('touchmove.ls-carousel', function(e) {
        self.carouselTouchMove(element, slider, e);
        LSHelpers.debug('carousel: touchmove');
    });

    if (item.drag_enabled) {
        element.off('mousedown.ls-carousel').on('mousedown.ls-carousel', function(e) {
            self.carouselTouchStart(element, slider, e);
            LSHelpers.debug('carousel: mousedown');
        });
        element.off('mouseup.ls-carousel').on('mouseup.ls-carousel', function(e) {
            self.carouselTouchEnd(element, slider, e);
            LSHelpers.debug('carousel: mouseup');
        });
        element.off('mouseout.ls-carousel').on('mouseout.ls-carousel', function(e) {
            self.carouselTouchEnd(element, slider, e);
            LSHelpers.debug('carousel: mouseout');
        });
        element.off('mousemove.ls-carousel').on('mousemove.ls-carousel', function(e) {
            self.carouselTouchMove(element, slider, e);
            LSHelpers.debug('carousel: mousemove');
        });
    }
}

/**
 * Carousel Touch start
 * @param {DOM} element: element that will contain the carousel
 * @param {DOM} slider: slider element
 * @param {Event} e: event
 */
LSWall_freegrid2_default.prototype.carouselTouchStart = function(element, slider, e) {
    var self = this;
    if (e.type == 'mousedown') {
        var pageX = e.originalEvent.pageX;
        e.preventDefault();
    } else {
        var pageX = e.originalEvent.targetTouches[0].pageX;
    }
    self.touchPosition = pageX;
    self.touchDelta = 0;
    self.dragOn = true;
    element.addClass('fpls-dragging');
}

/**
 * Carousel Touch move
 * @param {DOM} element: element that will contain the carousel
 * @param {DOM} slider: slider element
 * @param {Event} e: event
 */
LSWall_freegrid2_default.prototype.carouselTouchMove = function(element, slider, e) {
    var self = this;
    if (!self.dragOn) return;
    if (e.type == 'mousemove') {
        var pageX = e.originalEvent.pageX;
    } else {
        var pageX = e.originalEvent.targetTouches[0].pageX;
    }
    self.touchDelta = pageX - self.touchPosition;
    if (Math.abs(self.touchDelta) > 25) {
        e.preventDefault();
        slider.css({
            transform: "translateX(" + self.touchDelta / 2 + "px)"
        });
    }
}

/**
 * Carousel Touch end
 * @param {DOM} element: element that will contain the carousel
 * @param {DOM} slider: slider element
 * @param {Event} e: event
 */
LSWall_freegrid2_default.prototype.carouselTouchEnd = function(element, slider, e) {
    var self = this;
    if (!self.dragOn) return;
    var prev = element.find('.fpls-element-prev');
    var next = element.find('.fpls-element-next');
    element.removeClass('fpls-dragging');
    if (Math.abs(self.touchDelta) > 60) {
        if (self.touchDelta < 0) {
            next.trigger('click');
        } else {
            prev.trigger('click');
        }
    }
    self.dragOn = false;
    slider.css({
        transform: "translateX(" + 0 + "px)"
    });
}

/**
 * Method that actually move the slider
 * @param {DOM} element: element that will contain the carousel
 * @param {Number} delta: amount of movement
 * @param {Object} item: item JSON
 * @returns {DOM} current item
 */
LSWall_freegrid2_default.prototype.carouselMoveSlider = function (element, delta, item) {
    // var element = $ls('<div class="fpls-carousel-item" />');
    var current = element.find('.fpls-current');
    var current_index = element.find('.fpls-carousel-item').index(current);
    var next_index = current_index + delta;
    var max_index = element.find('.fpls-carousel-item').length - item.slides;
    if (next_index > max_index) {
        if (current_index == max_index && !item.loop_disabled) {
            //already at the end, go to beginning
            next_index = 0;
        } else {
            next_index = max_index;
        }
    }
    if (next_index < 0) {
        if (current_index == 0 && !item.loop_disabled) {
            //already at the beginning, go to the end
            next_index = max_index;
        } else {
            next_index = 0;
        }
    }

    return element.find('.fpls-carousel-item').eq(next_index);
}

/**
 * Method that listen dots interaction and set the slide to scroll
 * arrived at the end prevent empy spaces
 * @param {Number} index: the index of the current dot
 * @param {DOM} element: element that will contain the carousel
 * @param {Object} item: item JSON
 * @returns {DOM} the slide to scroll
 */
LSWall_freegrid2_default.prototype.carouselNextSlideByDot = function (index, element, item) {
    var self = this;

    var currentSlideIndex = index * item.stepper;

    var scrolledSlides = (index + 1) * item.stepper;
    var slidesLength = item.subitems.length;
    var dotsLength = element.find('.fpls-dot').length;

    if (scrolledSlides > slidesLength) {
        var remainingSlidesLength = slidesLength - (item.stepper * (dotsLength - 1));
        currentSlideIndex = Number(((index * item.stepper) - item.stepper) + remainingSlidesLength);
    }

    var max_index = element.find('.fpls-carousel-item').length - item.slides;
    if (currentSlideIndex > max_index) currentSlideIndex = max_index;

    return  element.find('.fpls-carousel-item').eq(currentSlideIndex);

}

/**
 * Method that sets the current element active state
 * @param {DOM} element: element that will contain the carousel
 * @param {DOM} next: slide to activate
 * @param {DOM} slider: slider element
 */
LSWall_freegrid2_default.prototype.carouselSetCurrent = function (element, next, slider, item) {
    var self = this;
    if (next.length < 1) {
        return;
    }

    //element.find('.fpls-carousel-item').find('a').attr('tabindex', '-1');
    element.find('.fpls-current').removeClass('fpls-current');
    next.addClass('fpls-current');

    // LST-1995
    element.find('.fpls-visible').removeClass('fpls-visible');
    element.find('.fpls-prev-carousel-item').removeClass('fpls-prev-carousel-item');
    element.find('.fpls-next-carousel-item').removeClass('fpls-next-carousel-item');

    next.addClass('fpls-visible');

    var carouselColumns = item.slides - 1;
    var slidesAfterCurrent = element.find('.fpls-current').nextAll();

    slidesAfterCurrent.each(function(i, v){
        var noOpacitySlides = $ls(this);
        if (i < carouselColumns) {
            noOpacitySlides.addClass('fpls-visible');
        } else {
            noOpacitySlides.addClass('fpls-next-carousel-item');
        };
    });

    var slidesBeforeCurrent = element.find('.fpls-current').prevAll();
    slidesBeforeCurrent.addClass('fpls-prev-carousel-item');

    if (item.opacity_enabled) {

        element.find('.fpls-semi-visible').removeClass('fpls-semi-visible');
        element.find('.fpls-carousel-item').not('.fpls-visible').addClass('fpls-semi-visible');

    };

    var sliderPaddingLeft = 0;
    var zoom = self.getZoom(item);
    if (item.padding_left) sliderPaddingLeft = item.padding_left * zoom;

    var offset = next.offset().left - slider.offset().left - sliderPaddingLeft;

    slider.css({ left: -offset });
    //next.addClass('fpls-current').find('a[tabindex="-1"]').removeAttr('tabindex');

    //highlight current dot
    var dotsContainer = element.find('.fpls-dots-container');
    if (dotsContainer) {
        dotsContainer.find('.fpls-dot.fpls-current')
            .removeClass('fpls-current');
        var currentDotIndex = Math.ceil(next.index() / item.stepper);
        dotsContainer.find('.fpls-dot')
            .eq(currentDotIndex)
                .addClass('fpls-current');
    }

}

/**
 * Enables auto rotation of slider
 * @param {DOM} element: element that will contain the carousel
 * @param {Object} item: item JSON
 */
LSWall_freegrid2_default.prototype.carouselAuto = function(element, item) {
    if (!item.auto) return;
    var next = element.find('.fpls-element-next');
    clearTimeout(item.__autoTimer);
    item.__autoTimer = setTimeout(function() {
        next.trigger('fplsnext');
    }, item.auto_seconds * 1000);
}

/**
 * Method that creates a compare
 * @param {DOM} element: element that will contain the compare
 * @param {Object} item: item JSON
 * @returns {DOM} compare html
 */
LSWall_freegrid2_default.prototype.handleCompare = function (element, item) {
    var self = this;

    var found = false;
    if (self.isSSR) {
        var slider = element.find('.fpls-compare__inner');
        if (slider.length > 0) found = true;
    }

    if (!found) {
        element.addClass('fpls-compare');
        var slider = $ls('<div class="fpls-compare__inner" />');
        slider.appendTo(element);
        slider.wrap('<div class="fpls-box-compare" />');
    }

    if (item) self.getAndSetCompareOptions(item, element);

    item.subitems && item.subitems.forEach(function(subitem, index) {
        if (subitem.node === 'group') {
            self.appendGroup(subitem, slider, 'fpls-compare__item');
        } else {
            self.appendNode(subitem, slider, 'fpls-compare__item', item);
        }
    });

    var comparedSlide = element.find('.fpls-compare__item').eq(0);
    comparedSlide.addClass('fpls-compared-slide');

    var slideToCopare = element.find('.fpls-compare__item').eq(1);
    slideToCopare.addClass('fpls-slide-to-compare');

    self.createCompareSlider(item, element);

    return element;
}

/**
 * Method that get and set the compare custom options
 * @param {Object} item: item JSON
 * @param {DOM} element: the compare
 */
LSWall_freegrid2_default.prototype.getAndSetCompareOptions = function (item, element) {
    var self = this;

    if (item.compare_color) element[0].style.setProperty('--compare-slider-color', item.compare_color);
    if (item.compare_opacity) {
        element[0].style.setProperty('--compare-slider-opacity', item.compare_opacity + '%');
        element.addClass('fpls-custom-opacity');
    }
    if (item.compare_reverse_arrows_color) element.addClass('fpls-reverse-arrows-color');
}

/**
 * Method that create compare elements
 * @param {Object} item: item JSON
 * @param {DOM} element: the compare element
 */
LSWall_freegrid2_default.prototype.createCompareSlider = function (item, element) {
    var self = this;

    var sliderPosition = item && item.compare_slider_position || 50;

    var slideToCompare = element.find('.fpls-slide-to-compare');
    slideToCompare.addClass('fpls-slide-to-compare__element');
    var slideToCompareWrapper = $ls('<div class="fpls-slide-to-compare__wrapper"> </div>');
    if (!self.isSSR) slideToCompare.wrap(slideToCompareWrapper);
    var compareSlider = $ls('<div class="fpls-slide-to-compare__slider"> </div>');
    if (!self.isSSR) element.find('.fpls-slide-to-compare__wrapper').prepend(compareSlider);

    if (!self.isSSR) element.css('opacity', 0);

    setTimeout(function(){
        slideToCompare.css({
            'width': sliderPosition + '%'
        });

        compareSlider.css({
            'left': sliderPosition + '%'
        });

        element.css('opacity', 1);

        self.moveCompareSlider(slideToCompare, element);

    }, 800);

    $ls(window).on('ls-delayed-resize', function(e) {
        self.adjustCompare(element, element.find('.fpls-slide-to-compare__slider'), e);
    });

    $ls(window).on('scroll', function(e) {
        self.adjustCompare(element, element.find('.fpls-slide-to-compare__slider'), e);
    });
};

/**
 * Method that listen events like ls resize and scroll
 * for adjust the compare
 * @param {DOm} compareElement: the compare element
 * @param {DOM} compareSlider: the compare slider
 * @param {Object} event: the current trigger event
 */
LSWall_freegrid2_default.prototype.adjustCompare = function (compareElement, compareSlider, event) {
    var self = this;

    var compareElementWidth, slideToCompareWidth, sliderPosition;
    var eventType = event.type;

    compareElementWidth = compareElement.width();
    slideToCompareWidth = parseInt(compareSlider.css('left'), 10);

    // trovo la % che occupa la slide da comparare
    sliderPosition = (slideToCompareWidth * 100) / compareElementWidth;

    if (eventType == 'scroll') {
        self.setCompareElements(compareElement, sliderPosition);
    } else if (eventType == 'ls-delayed-resize') {
        setTimeout(function(){
            self.setCompareElements(compareElement, sliderPosition);
        }, 1);
    }

}

/**
 * Method that set the width to the slide to compare
 * @param {DOM} compareElement: the compare element
 * @param {Number} sliderPosition: the current position of the slider
 */
LSWall_freegrid2_default.prototype.setCompareElements = function (compareElement, sliderPosition) {
    var self = this;

    var slideToCompare = compareElement.find('.fpls-slide-to-compare__element');

    slideToCompare.width(sliderPosition + '%');
}

/**
 * Method that set the compare slider move
 * @param {DOM} slideToCompare: the slide to compare
 * @param {DOM} compareElement: the compare element
 */
LSWall_freegrid2_default.prototype.moveCompareSlider = function (slideToCompare, compareElement) {
    var self = this;

    var clicked = 0;
    var compareSlider = compareElement.find('.fpls-slide-to-compare__slider');

    compareSlider.on("mousedown", slideReady);
    $ls(window).on("mouseup", slideFinish);
    compareSlider.on("touchstart", slideReady);
    $ls(window).on("touchend", slideFinish);

    function slideReady(e) {
        e.preventDefault();
        clicked = 1;
        $ls(window).on("mousemove", slideMove);
        $ls(window).on("touchmove", slideMove);
    };

    function slideFinish() {
        clicked = 0;
    };

    function slideMove(e) {
        var pos;
        if (clicked == 0) return false;
        pos = getCursorPos(e)
        if (pos < 0) pos = 0;
        if (pos > 100) pos = 100;
        slide(pos);
    };

    function getCursorPos(e) {
        var a, w, x = 0;
        e = (e.changedTouches) ? e.changedTouches[0] : e;
        a = slideToCompare.offset().left;
        w = compareElement.width();
        x = e.pageX - a;
        x = (x * 100) / w;
        return x;
    };

    function slide(x) {
        slideToCompare.width(x + '%');
        compareSlider.css({
            'left': x + '%'
        })
    };

}

/**
 * Get zoom
 * @param {JSON} item: optional item, used to retrieve the section_id
 * @returns {Float} zoom level
 */
LSWall_freegrid2_default.prototype.getZoom = function(item) {
    var self = this;
    var section_id;
    if (item) section_id = item.section_id;
    if (!section_id) return self.zoom; //if no section, return default zoom

    var grid = self.ls_grids.find(function(grid) {
        return grid.section_id == section_id;
    });

    if (grid) return grid.ls_grid.options.zoom;

    return self.zoom;
}

/**
 * Method that add animation to elements
 * @param {DOM} element: element that will contain the carousel
 * @param {Object} item: item JSON
 */

LSWall_freegrid2_default.prototype.handleAnimationInitialState = function(element, item) {
    var self = this;

    //read active state only if explicitly set (not undefined)
    var active = true;
    var hasDuration = item.animation && item.animation.duration > 0;

    if (item.animation && item.animation.active === false) active = item.animation.active;

    //if not active, or if duration is zero, then do nothing
    if (!active || !hasDuration) return false;

    var zoom = self.getZoom(item);

    if (element.find('>.animation-wrapper').length < 1) {
        element.wrapInner('<div class="animation-wrapper"></div>');
    }

    var elementContent = element.find('>.animation-wrapper');

    var adjustedDistance = item.animation.distance * zoom; //TODO: use zoom of the specific section

    if (!item.animation.scale && "" + item.animation.scale !== "0") item.animation.scale = 100;
    if (!item.animation.opacity && "" + item.animation.opacity !== "0") item.animation.opacity = 100;

    if (item.animation.scale == 0) item.animation.scale = 5; //set a minimun scale in order to avoid glitch
    if (item.animation.opacity == 0) item.animation.opacity = 5; //set a minimun scale in order to avoid glitch

    var scale = LSCss.scale(item.animation.scale / 100);
    var translate = LSCss.translate(item.animation.direction, adjustedDistance);

    elementContent.css({
        opacity: item.animation.opacity / 100,
        transform: translate + ' ' + scale,
        transformOrigin: '50% 50%'
    });

    return true;
}

LSWall_freegrid2_default.prototype.handleAnimation = function(element, item) {
    var self = this;
    var done = false;
    var active = self.handleAnimationInitialState(element, item);

    if (!active) return;

    var zoom = self.getZoom(item);

    if (element.find('>.animation-wrapper').length < 1) {
        element.wrapInner('<div class="animation-wrapper"></div>');
    }

    var elementContent = element.find('>.animation-wrapper');

    var adjustedDistance = item.animation.distance * zoom; //TODO: use zoom of the specific section
    var adjustedOffset = item.animation.offset * zoom;
    var adjustedDuration = item.animation.duration * zoom;

    function _getMeasures() {
        var offset = adjustedOffset || 0;
        var elementTop = element.offset().top + offset;
        var elementBottom = elementTop + element.outerWidth() + offset;
        var viewportMin = $ls(window).scrollTop();
        var viewportMax = viewportMin + $ls(window).outerHeight();
        var timelineStart = viewportMax;
        var timelineEnd = viewportMax - item.animation.duration;
        var timelineCursor = elementTop - timelineStart;

        if (item.animation.v2 === true ||
            item.animation.v3 === true) {
            //V2 or V3
            var timelineEnd = viewportMax - adjustedDuration;
            var timelineCursor = elementTop - timelineEnd;
        }

        return {
            offset: offset,
            elementTop: elementTop,
            viewportMin: viewportMin,
            viewportMax: viewportMax,
            timelineStart: timelineStart,
            timelineEnd: timelineEnd,
            timelineCursor: timelineCursor
        }
    }

    var initialState = _getMeasures();

    function _raf() {
        
        if (!item.animation.scrollbased) return;

        //LSHelpers.debug('raf');

        if (item._cssFinal) {
            if (typeof item._css == 'undefined') {
                item._css = $ls.extend({}, item._cssFinal);
            }

            var k = 0.15;

            var ease = {
                distance: _round(item._css.distance + (item._cssFinal.distance - item._css.distance) * k, 3),
                scale: _round(item._css.scale + (item._cssFinal.scale - item._css.scale) * k, 7),
                opacity: _round(item._css.opacity + (item._cssFinal.opacity - item._css.opacity) * k, 7)
            }

            if (_isDifferent(item._css.distance, item._cssFinal.distance, 3) ||
                _isDifferent(item._css.scale, item._cssFinal.scale, 7) ||
                _isDifferent(item._css.opacity, item._cssFinal.opacity, 7)) {
                    elementContent.css({
                        transform: LSCss.translate(item.animation.direction, ease.distance) + ' ' + LSCss.scale(ease.scale),
                        opacity: ease.opacity
                    });

                    window.requestAnimationFrame(function() {
                        _raf();
                    });
            }

            item._css = ease;
        }
    }

    //function to round number to N digits
    function _round(n, d) {
        var order = Math.pow(10, d);
        return Math.ceil(n * order) / order;
    }

    function _isDifferent(a, b, d) {
        var order = Math.pow(10, d);
        return Math.abs(a - b) > 10 / order;
    }

    _raf();

    $ls(window).on('scroll.ls-animation', function(e) {
        var state = _getMeasures();

        if (item.animation.scrollbased) {
            var progress = state.timelineCursor / (state.timelineEnd - state.timelineStart);

            if (item.animation.v2 === true) {
                //V2
                progress = state.timelineCursor / (state.timelineStart - state.timelineEnd);
            }

            if (item.animation.v3 === true) {
                //V3
                progress = state.timelineCursor / (state.timelineStart - state.timelineEnd);
                progress = 1 - progress;
            }

            if (progress < 0) progress = 0;
            if (progress > 1) progress = 1;

            item._cssFinal = {
                distance: _round(adjustedDistance * (1 - progress), 2),
                scale: _round(progress + item.animation.scale / 100 * (1 - progress), 6),
                opacity: _round(progress + item.animation.opacity / 100 * (1 - progress), 6)
            }

            //Set animation initial state
            if (!item.__animationInitialized) {
                elementContent.css({
                    transform: LSCss.translate(item.animation.direction, item._cssFinal.distance) + ' ' + LSCss.scale(item._cssFinal.scale),
                    opacity: item._cssFinal.opacity
                });
                item.__animationInizialized = true;
            }

            _raf();
        } else {
            //animation progress controlled by time
            var transition = 'all ' + item.animation.duration + 'ms';
            if (state.elementTop < state.viewportMax && !done) {
                setTimeout(function() {
                        elementContent.css({
                        transition: transition,
                        opacity: 1,
                        transform: 'translate(0) scale(1)'
                    });
                }, 0);
                done = true;

                // LST-1205
                var payload = self.getAnalyticsPayload('animation-start', item);
                livestory_analytics.pushEvent(payload, {
                    item: item,
                    wall: self.wall,
                    wallgroup: self.wallgroup
                });

                // LST-1205
                elementContent.one('transitionend', function() {
                    var payload = self.getAnalyticsPayload('animation-end', item);
                    livestory_analytics.pushEvent(payload, {
                        item: item,
                        wall: self.wall,
                        wallgroup: self.wallgroup
                    });
                });
            }
        }
    }).trigger('scroll.ls-animation');
}

/**
 * Method that set data-attributes to each element
 * Data attributes are then used by LSGrid plugin to create the actual grid
 * @param {DOM} element: element that will contain the carousel
 * @param {Object} item: item JSON
 */
LSWall_freegrid2_default.prototype.setAttributes = function(element, item) {
    var self = this;
    //set attributes needed by the grid
    element.attr('data-width', item.width);
    element.attr('data-left', item.left);
    element.attr('data-top', item.top);
    element.attr('data-zindex', item.zindex);
    element.attr('data-opacity', item.opacity);
    element.attr('data-height', item.height);

    if (item.type == 'block') {
        element.attr('data-min-height', item.height);
    }

    if (item.textscaling_off) {
        element.attr('data-textscaling-off', true);
        element.addClass('fpls-textscaling-off');
    }

    if (item.top_source) {
        var source = self.getItemId(item, item.top_source);
        element.attr('data-top-source', source);
    }

    if (item.height_source) {
        var source = self.getItemId(item, item.height_source);
        element.attr('data-height-source', source);
    }

    if (item.textscaling_off && item.textscaling_safe_margin > 0) {
        element.attr('data-textscaling-margin', item.textscaling_safe_margin);
    }

    if (item.sticky) {
        element.attr('data-sticky-start', item.sticky_start || 0);
        element.attr('data-sticky-end', item.sticky_end || 0);
    }

    if (item.sticky && item.sticky_bottom) {
        element.attr('data-sticky-bottom', true);
    }

    if (item.sticky && item.sticky_scaling_off) {
        element.attr('data-sticky-scaling-off', true);
    }

    if (item.slides) element.attr('data-columns', item.slides);
    if (item.padding) element.attr('data-margin', item.padding);

    if (item.padding_left) element.attr('data-padding-left', item.padding_left);
    if (item.padding_right) element.attr('data-padding-right', item.padding_right);
    if (item.arrow_left) element.find('.fpls-element-prev').attr('data-left', item.arrow_left);
    if (item.arrow_right) element.find('.fpls-element-next').attr('data-right', item.arrow_right);
    if (item.arrow_top) element.find('.fpls-element-prev, .fpls-element-next').attr('data-top', item.arrow_top);
    if (item.dots_horizontal_offset) element.find('.fpls-dots-container').attr('data-offset-x', item.dots_horizontal_offset);
    if (item.dots_vertical_offset) element.find('.fpls-dots-container').attr('data-offset-y', item.dots_vertical_offset);
    if (item.dots_color) element.find('.fpls-dots-container').attr('data-color', item.dots_color);

    if (item.accessibility) self.setAccessibilityAttributes(element, item.accessibility);
}

/**
 * Method that add effects to elements
 * @param {String} prefix: css selector for elements
 * @param {JSON} effects: effect parameters
 */
LSWall_freegrid2_default.prototype.injectFilter = function(prefix, filter) {
    var self = this;
    
    if (!filter.active) return;
    var css = "";
    css += prefix + ' .fpls-box-inner {';
    css += 'filter: blur(' + filter.blur + 'px);';
    css += '}';

    if (css) {
        LSHelpers.injectInlineCss(css, self.wall.wall_id, self.container);
    }
}

/**
 * Method that add effects to elements
 * @param {String} prefix: css selector for elements
 * @param {JSON} effects: effect parameters
 */
LSWall_freegrid2_default.prototype.injectEffects = function(prefix, effects) {
    var self = this;
    var css = "";

    if (!effects) return;
    if (!effects.active) return;

    if (!effects.loop) {
        if (parseFloat(effects.opacity) >= 0 && parseFloat(effects.opacityHover) >= 0) {
            var hoverDuration = '600';
            if (effects.hasOwnProperty('duration') && effects.duration >= 0) hoverDuration = effects.duration;
            css += prefix + ' {';
            css += 'transition: all ' + hoverDuration + 'ms;';
            css+= 'opacity: ' + effects.opacity / 100 + ';'
            css += '}';
            css += prefix + ':hover {';
            css+= 'opacity: ' + effects.opacityHover / 100 + ';'
            css += '}';
        }

        if (effects.background || effects.backgroundHover) {
            //set transparent if one of the color is not set
            if (!effects.background) effects.background = 'rgba(0,0,0,0)';
            if (!effects.backgroundHover) effects.backgroundHover = 'rgba(0,0,0,0)';
            css += prefix + ' .fpls-box-image:after {';
            css += 'transition:background 600ms;content:"";position:absolute;top:0;left:0;width:100%;height:100%;display:block;';
            css += 'background: ' + effects.background;
            css += '}';
            css += prefix + ':hover .fpls-box-image:after {';
            css += 'background: ' + effects.backgroundHover;
            css += '}';
        }

        if (effects.zoom && effects.zoom > 100) {
            css += prefix + ' .fpls-box-image img,';
            css += prefix + ' .fpls-box-image video {';
            css += 'transition:transform 600ms;';
            css += 'transform: ' + LSCss.scale(1);
            css += '}';
            css += prefix + ':hover .fpls-box-image img,';
            css += prefix + ':hover .fpls-box-image video {';
            css += 'transform: ' + LSCss.scale(effects.zoom/100);
            css += '}';
        }

        if (effects.zoom && effects.zoom < 100) {
            css += prefix + ' .fpls-box-image img,';
            css += prefix + ' .fpls-box-image video {';
            css += 'transition:transform 600ms;';
            css += 'transform: ' + LSCss.scale((100 / effects.zoom));
            css += '}';
            css += prefix + ':hover .fpls-box-image img,';
            css += prefix + ':hover .fpls-box-image video {';
            css += 'transform: ' + LSCss.scale(1);
            css += '}';
        }
        
        if (effects.scale > 0 || effects.distance > 0) {

            var transformStart = '';
            var transformEnd = '';

            if (effects.scale > 0) {
                transformStart += LSCss.scale(1);
                transformEnd += LSCss.scale(effects.scale / 100);
            }

            if (effects.distance > 0) {
                transformStart += ' translate(0) ';
                transformEnd += LSCss.translate(effects.direction, effects.distance);
            }
            
            css += prefix + ' .fpls-box-inner {';
            css += 'transition: all 600ms;';
            css += 'transform: ' + transformStart;
            css += '}';
            css += prefix + ':hover .fpls-box-inner {';
            css += 'transform: ' + transformEnd;
            css += '}';
        }
    }

    if (effects.loop == true) {
        var total_duration = (2*effects.duration+ 1*effects.delay);
        var p1 = effects.duration / total_duration * 100;
        var p2 = p1 * 2;

        if (effects.opacity && effects.opacityHover) {
            var stylesStart = 'opacity: ' + effects.opacity / 100 + ';';
            var stylesEnd = 'opacity: ' + effects.opacityHover / 100 + ';';
            css += LSCss.animationBounce(prefix, p1, p2, total_duration, stylesStart, stylesEnd).css;
        }

        if (effects.background || effects.backgroundHover) {
            var selector = prefix + ' .fpls-box-image:after';
            if (!effects.background) effects.background = 'rgba(0,0,0,0)';
            if (!effects.backgroundHover) effects.backgroundHover = 'rgba(0,0,0,0)';
            css += selector + ' {';
            css += 'content:"";position:absolute;top:0;left:0;width:100%;height:100%;display:block;';
            css += '}';
            var stylesStart = 'background: ' + effects.background + ';';
            var stylesEnd = 'background: ' + effects.backgroundHover + ';';
            css += LSCss.animationBounce(selector, p1, p2, total_duration, stylesStart, stylesEnd).css;
        }

        if (effects.zoom && effects.zoom > 100) {
            var selector = prefix + ' .fpls-box-image img,' + prefix + ' .fpls-box-image video';
            var stylesStart = 'transform: ' + LSCss.scale(1) + ';';
            var stylesEnd = 'transform: ' + LSCss.scale(effects.zoom/100) + ';';
            css += LSCss.animationBounce(selector, p1, p2, total_duration, stylesStart, stylesEnd).css;
        }

        if (effects.scale > 0 || effects.distance > 0) {
            var selector = prefix + ' .fpls-box-inner';
            var transformStart = '';
            var transformEnd = '';

            if (effects.scale > 0) {
                transformStart += LSCss.scale(1);
                transformEnd += LSCss.scale(effects.scale / 100);
            }

            if (effects.distance > 0) {
                transformStart += ' translate(0) ';
                transformEnd += LSCss.translate(effects.direction, effects.distance);
            }
            var stylesStart = 'transform: ' + transformStart;
            var stylesEnd = 'transform: ' + transformEnd;

            css += LSCss.animationBounce(selector, p1, p2, total_duration, stylesStart, stylesEnd).css;

            // Fix for Safari browser (LST-2584)
            if (platform.name == 'Safari') {
                var img = selector + ' img'
                css += img + ' {'
                css += '-webkit-backface-visibility: hidden;'
                css += '-webkit-transform: translateZ(0);'
                css += '}';
            ;}
        }
    }

    if (css) {
        LSHelpers.injectInlineCss(css, self.wall.wall_id, self.container);
    }
}

/**
 * Method that inject css for text styles
 * @param {String} prefix: css selector for elements
 * @param {JSON} textstyles: styles parameters
 * @param {JSON} effects: effect parameters (used for duration only)
 */
LSWall_freegrid2_default.prototype.injectTextstyles = function(prefix, textstyles, effects) {
    var self = this;
    //MOVED TO HELPERS
    LSHelpers.injectTextstyles(self, prefix, textstyles, effects);
}

/**
 * Method that inject alignment of text blocks
 * @param {String} prefix: css selector for elements
 * @param {JSON} alignment: alignment parameters
 */
LSWall_freegrid2_default.prototype.injectAlignmentStyles = function(prefix, alignment) {
    var self = this;
    var css = "";

    if (!alignment) return;

    css += prefix + ' .fpls-box-inner {';
    switch (alignment.horizontal) {
    case 'left':
        css += 'text-align: left;';
        break;
    case 'center':
        css += 'text-align: center;';
        break;
    case 'right':
        css += 'text-align: right;';
        break;
    }

    if (alignment.vertical) {
        css += 'display: flex;';
        switch (alignment.vertical) {
        case 'top':
            css += 'align-items: flex-start';
            break;
        case 'center':
            css += 'align-items: center';
            break;
        case 'bottom':
            css += 'align-items: flex-end';
            break;
        }
    }
    
    css += '}';
    
    if (css) {
        LSHelpers.injectInlineCss(css, self.wall.wall_id, self.container);
    }
}

/**
 * Method that inject css for fonts
 * @param {String} selector: CSS selector for items to apply fonts to
 * @param {String} font_heading: font for heading
 * @param {String} font_body: font for body
 */
LSWall_freegrid2_default.prototype.injectFontFaces = function(selector, font_heading, font_body) {
    var self = this;
    var css = "";
    if (font_heading) {
        font_heading = LSHelpers.injectFont(font_heading, self.container);
        css += selector + ' h1,';
        css += selector + ' h2,';
        css += selector + ' h3,';
        css += selector + ' h4,';
        css += selector + ' h5,';
        css += selector + ' h6 {';
            css += ' font-family:' + font_heading + ' !important;';
        css += '}';
    }

    if (font_body) {
        font_body = LSHelpers.injectFont(font_body, self.container);
        css += selector + ',';
        css += selector + ' p {';
            css += ' font-family:' + font_body + ' !important;';
        css += '}';
    }

    if (css) LSHelpers.injectInlineCss(css, self.wall.wall_id, self.container);
}

/**
 * Method to append groups
 * @param {Object} item: item JSON
 * @param {DOM} parentElement: parent element of the carousel
 * @param {String} additionalClass: additional class for group
 */
LSWall_freegrid2_default.prototype.appendGroup = function(item, parent, additionalClass) {
    var self = this;

    var item_id = self.getItemId(item);

    if (item_id) item.uniqueId = self.getItemSsrId(item);

    var found = false;

    if (self.isSSR) {
        var group = self.grid.find('#_' + item_id);
        if (group.length > 0) {
            found = true;
        } else LSHelpers.debug('SSR element not found', '#_' + item_id);
    }

    if (!found) {
        var group = $ls('<div />');
        group.attr('id', '_' + item_id);
        group.appendTo(parent);
    }

    self.fillTrackingCodes(item);

    group.addClass('fpls-group');
    if (additionalClass) group.addClass(additionalClass);

    //add custom css class to group
    if (item.cssclass) group.addClass(item.cssclass);

    //add data-item-class to identify blocks with css class
    if (item.cssclass) group.attr('data-item-class', item.cssclass);

    //add data-item-id to block (to allow specific targeting)
    if (item._id) group.attr('data-item-id', item._id);

    // LST-2987
    item.subitems && item.subitems.forEach(function(subitem) {
        subitem.carouselLazyLoad = item.carouselLazyLoad;
        self.appendNode(subitem, group, 'fpls-grouped');
    });

    self.injectEffects(self.cssBrekapointPrefix + ' #_' + item_id, item.effects);
    self.injectFilter(self.cssBrekapointPrefix + ' #_' + item_id, item.filter || {});
    self.setAttributes(group, item);
    self.handleAnimation(group, item);

    //LST-2270
    if (
        item.link &&
        item.link.length > 0
    ) {
        group.addClass('fpls-group-link');
        group.attr('data-href', item.link);

        //add tabindex to allow keybaord navigation
        group.attr('tabindex', '0');

        group.on('click.ls', function(e) {
            e.preventDefault();
            if (item.blank) {
                window.open(group.attr('data-href'), '_blank');
            } else {
                window.open(group.attr('data-href'), '_self');
            }
        });

        //handle keydown event (enter) to trigger click
        group.on('keydown.ls', function(e) {
            if (e.keyCode == 13) group.trigger('click.ls');
        });

    }

    // internal link scroll
    if (
        !item.link &&
        item.internal_link
    ) self.internalLink(group, item);
}

/**
 * Method to set the layout scaling (zoom)
 */
LSWall_freegrid2_default.prototype.setZoom = function() {
    var self = this;

    //global zoom
    var desired_width = self.current_layout.width;

    //desired width
    self.ls_grids.forEach(function(g) {
        if (g.layout.width) desired_width = Math.max(desired_width, g.layout.width);
    });

    var container_width = self.container.outerWidth();
    self.zoom = container_width / desired_width;

    //sections specific zoom
    self.ls_grids.forEach(function(g) {
        var section = g.containerElement.find('.fpls-grid--outer');
        if (section.length < 1) section = g.containerElement.find('.fpls-grid').parent();
        var section_width = section.outerWidth();
        var desired_section_width = g.layout.width || desired_width;
        var zoom = section_width / desired_section_width;
        g.ls_grid.setZoom(Math.min(zoom, self.zoom));
    });
}

/**
 * Method to calculate correct resolution of items in grid
 * @param {Object} item: item JSON
 * @param {Object} parent_item: parent item JSON (used for groups or carousels)
 * @returns size in pixel
 */
LSWall_freegrid2_default.prototype.optimalResolution = function(item, parent) {
    var self = this;

    var width = item.width;
    var height = item.height;

    if (!width && parent && parent.width) width = parent.width;
    if (!height && parent && parent.height) height = parent.height;

    var zoom = self.getZoom(item);

    width = width * zoom; //upscale to compensate crop and other things...
    height = height * zoom;

    // if (window.devicePixelRatio >= 2) {
    //     width = width * 2;
    //     height = height * 2;
    // }

    if (item.crop && item.crop.width) {
        width = width * item.crop.width / 100;
        height = height * item.crop.width / 100;
    }

    return width;
}

/**
 * Method to adapt image in box, according to crop
 * @param {DOM} box: element that contains image
 * @param {JSON} crop: crop settings
 */
LSWall_freegrid2_default.prototype.fillBox = function(box, crop) {
    var imgEl = box.find('.fpls-box-image img')[0];
    var imgCont = box.find('.fpls-box-image > span').eq(0);
    var img = box.find('.fpls-box-image img').eq(0);
    var ih = img.height();
    var bh = box.find('.fpls-box-image').height();

    if (crop && typeof crop.marginLeft !== 'undefined' && typeof crop.marginTop !== 'undefined' && typeof crop.width !== 'undefined') {
        imgCont.addClass('fpls-box-crop-container');
        img.addClass('fpls-box-crop-image');
        img.css({
            marginLeft: crop.marginLeft + '%',
            marginTop: crop.marginTop + '%',
            width: crop.width + '%'
        });
    } else {
        imgCont.removeClass('fpls-box-crop-container').removeAttr('style');
        img.removeClass('fpls-box-crop-image').removeAttr('style');
    }
}

/**
 * Method to set background
 * @param {DOM} el: element to apply background to
 * @param {JSON} background: background parameters
 * param {String} backgroundClass: if needed, the background can be applied to a child element, with this selector
 */
LSWall_freegrid2_default.prototype.setBackground = function(el, background, backgroundClass) {
    if (!background) return;

    var element = el;
    if (backgroundClass) element = el.find(backgroundClass);

    var width = element.outerWidth() || 3000;
    var resolution = LSHelpers.resolutionRounder(width);

    if (background.color) {
        element.css('background-color', background.color);
    } else {
        element.css('background-color', 'transparent');
    }
    if (background.image) {
        element.css('background-image', 'url(' + LSHelpers.resize(background.image, width) + ')');

        switch (background.imageMode) {
            case 'repeat':
                element.css('background-repeat', 'repeat');
                element.css('background-size', 'auto');
            break;
            case 'fillrepeat':
                element.css('background-repeat', 'repeat-y');
                element.css('background-size', '100%');
            break;
            case 'fill':
                element.css('background-repeat', 'no-repeat');
                element.css('background-size', '100%');
            break;
            case 'contain':
                element.css('background-repeat', 'no-repeat');
                element.css('background-size', 'contain');
            break;
            default:
                element.css('background-repeat', 'no-repeat');
                element.css('background-size', 'cover');
            break;
        }

        if (background.focalPoint) {
            element.addClass('fpls-background-focal--' + background.focalPoint);
        } else {
            element.addClass('fpls-background-focal--n');
        }

        if (background.stickyMode) element.css('background-attachment', 'fixed');
    }
}

/**
 * Method to set box borders
 * @param {DOM} element: element to apply border to
 * @param {JSON} border: border parameters
 */
LSWall_freegrid2_default.prototype.setBorder = function(element, border) {
    var borderClass = '.fpls-box-inner';
    if (element.hasClass('fpls-carousel')) borderClass = '.fpls-box-carousel';

    if (border && border.color && border.width) {
        element.find(borderClass).css({
            border: border.width + 'px solid ' + border.color
        });
    }

    if (border && border.radius) {
        element.find(borderClass).css({
            borderRadius: border.radius + 'px'
        });
    }

    if (border && border.shadowColor && border.shadow !== false) {
        element.find(borderClass).css({
            'box-shadow': (border.shadowX || 0) + 'px ' + (border.shadowY || 0) + 'px ' + (border.shadowBlur || 0) + 'px ' + border.shadowColor
        });
    }
}

/**
 * Method that injects css to show overlay effects (hover on boxes)
 * @param {String} selector: CSS selector for items that need overlay
 * @param {JSON} overlays: overlay parameters
 * @param {JSON} effects: if using new effect functionality, use that setting as hover color
 */
LSWall_freegrid2_default.prototype.injectOverlays = function(selector, overlays, effects) {
    if (!overlays) return;
    var self = this;
    var css = "";

    var overlay_color;

    if(effects && effects.active && effects.backgroundHover) overlay_color = effects.backgroundHover;
    if(overlays.background) overlay_color = overlays.background;

    if (overlay_color) {
        css += selector + ' .fpls-box-overlay-content {';
        css += 'background-color: ' + overlay_color + ' !important;';
        css += '}';
    }

    if(overlays.text) {
        css += selector + ' .fpls-box-overlay-content,';
        css += selector + ' .fpls-box-overlay-content * {';
        css += 'color: ' + overlays.text + ' !important;';
        css += '}';
    }
    
    if (css) LSHelpers.injectInlineCss(css, self.wall.wall_id, self.container);
}

/**
 * Method that refresh the layout after resizing or other events
 */
LSWall_freegrid2_default.prototype.refresh = function() {
    this.setZoom();
    //fix carousels
    var carousels = this.container.find('.fpls-carousel');
    var zoom = this.getZoom();

    carousels.each(function(){
        var carousel = $ls(this);
        var paddingLeft = 0;
        if (carousel.attr('data-padding-left')) paddingLeft = parseInt(carousel.attr('data-padding-left'), 10) * zoom;
        var slider = carousel.find('.fpls-carousel-inner');
        if (slider.length < 1) return;
        var current = slider.find('.fpls-current');
        if (current.length < 1) return;
        var offset = slider.find('.fpls-current').offset().left - slider.offset().left - paddingLeft;
        slider.css({ left: -offset });
    });
}

/**
 * Next method: it is used by the popup to show the next image when clicking to the popup arrows
 * @param {String} id: ID of the current post
 */
LSWall_freegrid2_default.prototype.next = function(id) {
    var self = this;
    var found;
    var posts = self.boxes.filter(self._filterPosts);
    posts.forEach(function(post, index) {
        if (post.data('id') == id) found = index;
    });
    found++;
    if (found >= posts.length) found = 0;
    if (posts[found])
        posts[found].trigger('click.ls');
}

/**
 * Prev method: it is used by the popup to show the prev image when clicking to the popup arrows
 * @param {String} id: ID of the current post open in popup
 */
LSWall_freegrid2_default.prototype.prev = function(id) {
    var self = this;
    var found;
    var posts = self.boxes.filter(self._filterPosts);
    posts.forEach(function(post, index) {
        if (post.data('id') == id) found = index;
    });
    found--;
    if (found < 0) found = posts.length - 1;
    if (posts[found])
        posts[found].trigger('click.ls');

}

/**
 * Method to determine if "prev" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
LSWall_freegrid2_default.prototype.prevAvailable = function(id) {
    var self = this;
    return self.boxes.filter(self._filterPosts).length > 1;
}

/**
 * Method to determine if "next" arrow of the popup should be visible
 * @param {String} id: ID of the current post open in popup
 */
LSWall_freegrid2_default.prototype.nextAvailable = function(id) {
    var self = this;
    return self.boxes.filter(self._filterPosts).length > 1;
}