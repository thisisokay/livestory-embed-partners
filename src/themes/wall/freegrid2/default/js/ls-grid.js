/**
 * LSGrid
 * @param {DOM} container: DIV that contains the grid elements
 * @param {JSON} options: custom options for LS Grid
 * @param {DOM} mainContainer: main container of Live Story
 * @constructor
 * Class for whiteboard templates
*/
LSGrid = function(container, options, mainContainer) {
	this.container = container;
	this.mainContainer = mainContainer;
	var defaults = {
		zoom: 1,
		grid_spacing: 10,
		grid_enabled: false,
		guides_enabled: true,
		guides_proximity: 200,
		snap_proximity: 200,
		snap_tolerance: 5,
		selectionWrapper: false,
		contentClass: '.content',
		elementClass: '.element',
		carouselClass: '.carousel',
		carouseledClass: '.carouseled',
		groupedClass: '.grouped',
		groupClass: '.group',
		compareClass: '.compare',
		comparedClass: '.compared',
		guideClassX: '.guide-x',
		guideClassY: '.guide-y',
		gridClass: '.grid',
		selectionOverlay: '.selection',
		selectedClass: '.selected',
		debugClass: '.info-coordinates',
		enabled: true,
		selectionEnabled: true
	}
	this.options = jQuery.extend({}, defaults, options);
	this.timers = {};

}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.disable = function() {
	var self = this;
	self.options.enabled = false;
	self.init();
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.enable = function() {
	var self = this;
	self.options.enabled = true;
	self.init();
}

/**
 * Set canvas width and height
 * @param {Number} width: width
 * @param {Number} height: height
*/
LSGrid.prototype.setCanvasSize = function(width, height) {
	var self = this;
	if (width) self.container.attr('data-width', Math.round(width / self.options.zoom));
	if (height) this.container.attr('data-height', Math.round(height / self.options.zoom));
	self._scaleCanvas();
	self._trigger('change');
}

/**
 * Set zoom level
 * If the canvas is set 1000px wide, and the container is 1500px, zoom should be 1.5 
 * @param {Float} zoom: scaling of the canvas
*/
LSGrid.prototype.setZoom = function(zoom) {
	var self = this;
	this.options.zoom = zoom;
	self.container.width(self.container.attr('data-width') * self.options.zoom);
	self.container.height(self.container.attr('data-height') * self.options.zoom);
	self._scaleCanvas();
}

/**
 * Main init method
*/
LSGrid.prototype.init = function() {
	var self = this;
	self.guide_x = self.container.find(self.options.guideClassX);
	self.guide_y = self.container.find(self.options.guideClassY);
	self.grid = self.container.find(self.options.gridClass);
	self.selectionWrapper = self.container;
	if (self.options.selectionWrapper) self.selectionWrapper = jQuery(self.options.selectionWrapper);

	self.container.find(self.options.elementClass)
		.not(self.options.groupedClass)
		.not(self.options.carouseledClass)
		.not(self.options.comparedClass)
		.each(function() {
		if (self.options.enabled) {
			self._initElement(jQuery(this), false);
		} else {
			self._initElementOff(jQuery(this));
		}
	});

	self.container.find(self.options.groupClass)
		.not(self.options.carouseledClass)
		.not(self.options.comparedClass)
		.each(function() {
		if (self.options.enabled) {
			self._initElement(jQuery(this), true);
		} else {
			self._initElementOff(jQuery(this));
		}
	});

	self.container.find(self.options.carouselClass)
		.not(self.options.carouseledClass)
		.each(function() {
		if (self.options.enabled) {
			self._initElement(jQuery(this), false);
		} else {
			self._initElementOff(jQuery(this));
		}
	});

	self.container.find(self.options.compareClass)
		.not(self.options.comparedClass)
		.each(function() {
		if (self.options.enabled) {
			self._initElement(jQuery(this), false);
		} else {
			self._initElementOff(jQuery(this));
		}
	});

	self._scaleCanvas();

	if (self.options.enabled) {
		self._enableSelection();
		self._enableKeys();
		self._initCanvas();
	} else {
		self._disableSelection();
		self._disableKeys();
	}

	//update instance
	self.container.data('lsgrid-instance', self);
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.enableGrid = function() {
	var self = this;
	this.options.grid_enabled = true;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.disableGrid = function() {
	var self = this;
	this.options.grid_enabled = false;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.setGrid = function(grid) {
	if (grid > 0) {
		this.options.grid_spacing = parseFloat(grid);
	}
	if (grid == 0) {
		this.options.grid_spacing = parseFloat(grid);
	}
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.findElementByCoordinates = function(x, y) {
	var self = this;
	var found = false;
	self.container.find(self.options.elementClass).each(function() {
		var el = jQuery(this);
		if (el.position().left < x &&
			x < el.position().left + el.outerWidth() &&
			el.position().top < y &&
			y < el.position().top + el.outerHeight() && (
				found === false || 
				parseFloat(found.attr('data-zindex')) < parseFloat(el.attr('data-zindex'))
			)
		) found = el;
	});
	return found;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.selectionCallback = function(callback) {
	var self = this;
	self.container.off('selection.create').one('selection.create', function(e, selection) {

		if (!selection) return callback(false);
		if (typeof selection == 'undefined') return callback(false);

		unit = 1;
		if (self.options.grid_enabled) {
			unit = self.options.grid_spacing * self.options.zoom;
		}
		var snap = {
			left: Math.round(selection.left / unit) * unit / self.options.zoom,
			top: Math.round(selection.top / unit) * unit / self.options.zoom,
			width: Math.round((selection.right - selection.left) / unit) * unit / self.options.zoom,
			height: Math.round((selection.bottom - selection.top) / unit) * unit / self.options.zoom
		}

		callback(snap);
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.alignX = function(direction, scope) {
	var self = this;
	var selected = self.container.find(self.options.selectedClass);
	if (selected.length < 1) return;

	var left = 9999999999999;
	var right = 0;
	var center;
	
	selected.each(function() {
		var el = jQuery(this);
		var s = self._size(el);
		left = Math.min(left, s.left);
		right = Math.max(right, s.right);
	});

	center = (left + right) / 2;

	if (scope == 'page') {
		left = 0;
		center = self.container.width() / 2;
		right = self.container.width();
	}

	selected.each(function() {
		var el = jQuery(this);
		var s = self._size(el);
		var snap;
		switch (direction) {
			case 'left':
				snap = { left: left };
			break;
			case 'center':
				snap = { left: center - s.halfwidth };
			break;
			case 'right':
				snap = { left: right - s.width };
			break;
		}
		if (snap) {
			el.css(snap);
			self._updateElementData(snap, el);
		}
	});
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.alignY = function(direction, scope) {
	var self = this;
	var selected = self.container.find(self.options.selectedClass);
	if (selected.length < 1) return;

	var top = 9999999999999;
	var bottom = 0;
	var middle;
	
	selected.each(function() {
		var el = jQuery(this);
		var s = self._size(el);
		top = Math.min(top, s.top);
		bottom = Math.max(bottom, s.bottom);
	});

	middle = (top + bottom) / 2;

	if (scope == 'page') {
		top = 0;
		middle = self.container.height() / 2;
		bottom = self.container.height();
	}

	selected.each(function() {
		var el = jQuery(this);
		var s = self._size(el);
		var snap;
		switch (direction) {
			case 'top':
				snap = { top: top };
			break;
			case 'middle':
				snap = { top: middle - s.halfheight };
			break;
			case 'bottom':
				snap = { top: bottom - s.height };
			break;
		}
		if (snap) {
			el.css(snap);
			self._updateElementData(snap, el);
		}
	});
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.distributeX = function(equal_space) {
	var self = this;
	var selected = self.container.find(self.options.selectedClass);
	if (selected.length < 3) return;

	selected.sort(function(a, b) {
		var _a = self._size(jQuery(a));
		var _b = self._size(jQuery(b));
		return _a.left - _b.left;
	});

	var size_first = self._size(selected.eq(0));
	var size_last = self._size(selected.eq(selected.length-1));

	if (equal_space) {
		var left = size_first.left;
		var right = size_last.right;
		var total_width = 0;
		selected.each(function() {
			total_width += self._size(jQuery(this)).width;
		});
		var unit = (right - left - total_width) / (selected.length - 1);
	} else {
		var left = size_first.centerx;
		var right = size_last.centerx;
		var unit = (right - left) / (selected.length - 1);
	}

	for (var i = 1; i < selected.length - 1; i++) {
		var el = selected.eq(i);
		var s = self._size(el);
		var el_before = selected.eq(i-1);
		var s_before = self._size(el_before);

		if (equal_space) {
			var snap = {
				left: s_before.right + unit
			}
		} else {
			var snap = {
				left: s_before.centerx + unit - s.halfwidth
			}
		}
		el.css(snap);
		self._updateElementData(snap, el);
	}
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.distributeY = function(equal_space) {
	var self = this;
	var selected = self.container.find(self.options.selectedClass);
	if (selected.length < 3) return;

	selected.sort(function(a, b) {
		var _a = self._size(jQuery(a));
		var _b = self._size(jQuery(b));
		return _a.top - _b.top;
	});

	var size_first = self._size(selected.eq(0));
	var size_last = self._size(selected.eq(selected.length-1));

	if (equal_space) {
		var top = size_first.top;
		var bottom = size_last.bottom;
		var total_height = 0;
		selected.each(function() {
			total_height += self._size(jQuery(this)).height;
		});
		var unit = (bottom - top - total_height) / (selected.length - 1);
	} else {
		var top = size_first.centery;
		var bottom = size_last.centery;
		var unit = (bottom - top) / (selected.length - 1);
	}

	for (var i = 1; i < selected.length - 1; i++) {
		var el = selected.eq(i);
		var s = self._size(el);
		var el_before = selected.eq(i-1);
		var s_before = self._size(el_before);

		if (equal_space) {
			var snap = {
				top: s_before.bottom + unit
			}
		} else {
			var snap = {
				top: s_before.centery + unit - s.halfheight
			}
		}
		el.css(snap);
		self._updateElementData(snap, el);
	}
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.groupSelected = function() {
	var self = this;
	var selected = self.container.find(self.options.selectedClass);
	if (selected.length < 1) return;
	selected.each(function() {
		var item = jQuery(this);
		self._ungroup(item);
	});

	var selected = self.container.find(self.options.selectedClass);
	self._group(selected);
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.ungroupSelected = function() {
	var self = this;
	var selected = self.container.find(self.options.selectedClass);
	if (selected.length < 1) return;
	selected.each(function() {
		var item = jQuery(this);
		self._ungroup(item);
	});
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.bringToFront = function(el) {
	var self = this;
	if (!el) el = self.container.find(self.options.selectedClass);
	var zindex = self._getMaxZIndex() + 1;
	self._setZIndex(el, zindex);
	self._normalizeZIndexes();
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.bringToBack = function(el) {
	var self = this;
	if (!el) el = self.container.find(self.options.selectedClass);
	var zindex = self._getMinZIndex() - 1;
	self._setZIndex(el, zindex);
	self._normalizeZIndexes();
	self._trigger('change');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._createBlock = function(html, snap, callback) {
	var self = this;
	var el = jQuery('<div />');
	el.addClass(self.options.elementClass);
	el.css(snap);
	el.appendTo(self.container);
	self._initElement(el);
	self._updateElementData(snap, el);
	self.bringToFront(el);
	self._enableSelection();
	callback && callback(el);
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._extendHeight = function(el) {
	var self = this;
	//TODO: implement
	var h = el.offset().top - self.container.offset().top;
	var current_h = self.container.height();

	h += el.outerHeight();
	current_h = Math.max(current_h, h);

	self.container.height(current_h);
	self.container.attr('data-height', current_h / self.options.zoom);
	self._trigger('change');
}

/**
 * Scale canvas and adapt content to zoom level
*/
LSGrid.prototype._scaleCanvas = function() {
	var self = this;

	//reset delta
	self.scalingDelta = 0;

	var width = 1 * self.container.attr('data-width');
	var height = 1 * self.container.attr('data-height');

	self.container.width(width * self.options.zoom);

	//scale elements
	self.container.find(self.options.elementClass)
		.not(self.options.carouseledClass)
		.not(self.options.groupedClass)
		.not(self.options.comparedClass)
		.each(function() {
		self._positionElement(jQuery(this), 0);
		self._scaleContent(jQuery(this));
	});

	//scale groups (not inside a carousel)
	self.container.find(self.options.groupClass)
		.not(self.options.carouseledClass)
		.not(self.options.comparedClass)
		.each(function() {
		self._positionElement(jQuery(this), 0);
		self._scaleContent(jQuery(this));
		jQuery(this).find(self.options.groupedClass).each(function() {
			self._positionElement(jQuery(this), 1);
			self._scaleContent(jQuery(this));
		});
	});

	//scale carousels
	self.container.find(self.options.carouselClass)
		.each(function() {
		self._positionElement(jQuery(this), 0);
		self._scaleContent(jQuery(this));
		self._scaleCarousel(jQuery(this));
	});

	//scale compares
	self.container.find(self.options.compareClass)
		.each(function() {
		self._positionElement(jQuery(this), 0);
		self._scaleContent(jQuery(this));
		self._scaleCompare(jQuery(this));
	});

	if (self.options.grid_enabled) {
		self.showGrid();
	}

	//adjust linked elements properties
	self._adjustLinkedContent();

	//adjust canvas height
	var height_source = self.container.attr('data-height-source');

	if (height_source) {
		var source = self.container.find('#_' + height_source);

		if (source.hasClass('fpls-textscaling-off')) {

			var expectedHeight = source.attr('data-height') * self.options.zoom;
			var actualHeight = source.outerHeight();
			var actualHeightContent = source.find(self.options.contentClass).outerHeight();
			var delta = Math.max(actualHeight, actualHeightContent) - expectedHeight;

			if (delta > 0) {
				height += delta / self.options.zoom;
			}

		}

	}

	self.container.height(height * self.options.zoom);
}

/**
 * Get element attributes and place element in the canvas
 * @param {DOM} el: element html node
 * @param {Number} level: it is 1 if node is child of another element (for example element in group)
*/
LSGrid.prototype._positionElement = function(el, level) {
	var self = this;

	el.css({
		width: el.attr('data-width') * self.options.zoom,
		zIndex: parseFloat(el.attr('data-zindex') || 1)
	});

	if (!el.attr('data-min-height')) {
		el.css({
			height: el.attr('data-height') * self.options.zoom
		});
	} else {
		el.css({
			height: 'auto',
			minHeight: el.attr('data-min-height') * self.options.zoom
		});
	}

	if (el.attr('data-textscaling-off')) {
		el.css({
			height: "auto"
		});
	}

	if (level == 0 && el.attr('data-sticky-start')) {
		self._sticky(el);
	}

	if (el.attr('data-stuck')) {
		el.css({
			position: 'fixed',
			top: el.attr('data-stuck'),
			left: el.attr('data-left') * self.options.zoom + self.container.offset().left
		});
	} else if (el.attr('data-new-top')) {
		el.css({
			position: 'absolute',
			top: el.attr('data-new-top') * self.options.zoom,
			left: el.attr('data-left') * self.options.zoom
		});
	} else {
		el.css({
			position: 'absolute',
			top: el.attr('data-top') * self.options.zoom,
			left: el.attr('data-left') * self.options.zoom
		});
	}

	if (parseFloat(el.attr('data-opacity')) >= 0) el.find(self.options.contentClass).css({
		opacity: el.attr('data-opacity')
	});
}

/**
 * Makes an element sticky
 * @param {DOM} el: element html node
*/
LSGrid.prototype._sticky = function(el) {

	var self = this;
	if (!el.attr('data-sticky-start')) return;
	jQuery(window).off('scroll.lsgrid' + el.attr('id')).on('scroll.lsgrid' + el.attr('id'), function(e) {
		var sticky_start = parseFloat(el.attr('data-sticky-start')) * self.options.zoom;
		var sticky_end = parseFloat(el.attr('data-sticky-end')) * self.options.zoom;

		//support for absolute sticky values (not scaled by zoom)
		if (el.attr('data-sticky-scaling-off')) {
			sticky_start = parseFloat(el.attr('data-sticky-start'));
		}

		var top = parseFloat(el.attr('data-top')) * self.options.zoom;
		var height = parseFloat(el.attr('data-height')) * self.options.zoom;
		var left = parseFloat(el.attr('data-left')) * self.options.zoom;
		var container_top = self.container.offset().top;
		var mainContainer_top = self.mainContainer.offset().top;
		var window_height = window.innerHeight;

		var original_sticky_start = sticky_start;

		//adjust scroll top removing container top
		var scroll_top = jQuery(window).scrollTop() - container_top;

		//if reference is bottom, calculate top based on jquery height
		if (el.attr('data-sticky-bottom')) {
			sticky_start = window_height - sticky_start;
		} else {
			//adjust sticky start (can't be bigger than original top)
			sticky_start = Math.min(sticky_start, top + container_top);	
		}

		//adjust sticky end (can't be bigger than layout size)
		sticky_end = Math.min(sticky_end, self.mainContainer.height() - height - sticky_start - container_top + mainContainer_top);

		if (top - scroll_top <= sticky_start && scroll_top < sticky_end) {
			//stuck, so fixed

			if (el.attr('data-sticky-bottom')) {
				el.css({
					position: 'fixed',
					bottom: - height + original_sticky_start,
					left: left + self.container.offset().left
				});
			} else {
				el.css({
					position: 'fixed',
					top: sticky_start,
					left: left + self.container.offset().left
				});
			}

			el.attr('data-stuck', sticky_start);
			el.removeAttr('data-new-top');

			self.container.css({
				zIndex: 10
			});
			
		} else if (scroll_top >= sticky_end) {
			//not stuck and below the sticky end
			var new_top = sticky_end + sticky_start;
			el.attr('data-new-top', new_top / self.options.zoom);
			el.removeAttr('data-stuck');
			el.css({
				position: 'absolute',
				top: new_top,
				bottom: 'auto',
				left: left
			});

			self.container.css({
				zIndex: 10
			});
		} else {
			//not stuck and above the sticky start
			el.removeAttr('data-new-top');
			el.removeAttr('data-stuck');
			el.css({
				position: 'absolute',
				top: top,
				bottom: 'auto',
				left: left
			});

			self.container.css({
				zIndex: 1
			});
		}
	}).trigger('scroll.lsgrid' + el.attr('id'));
}

/**
 * Scale content with css "scale" to match the zoom
 * If text scaling is disabled (data-textscaling-off) don't use scale
 * @param {DOM} el: element html node
*/
LSGrid.prototype._scaleContent = function(el) {
	var self = this;
	var content = el.find(self.options.contentClass).eq(0);
	if (!el.attr('data-textscaling-off')) {
		content.css({
			transform: 'scale(' + self.options.zoom * 1.001 + ')',
			width: el.attr('data-width')
		});
		if (el.attr('data-min-height')) {
			content.css({
				height: 'auto',
				minHeight: el.attr('data-min-height') + 'px'
			});
		} else {
			content.css({
				height: el.attr('data-height')
			});
		}
	} else {
		content.css({
			transform: 'scale(1)',
			width: 'auto',
			height: 'auto',
			minHeight: el.attr('data-height') * self.options.zoom
		});

		return;
	}
}

/**
 * Adjust the hight of contents connected to the height of a given text
*/
LSGrid.prototype._adjustLinkedContent = function() {
	var self = this;
	self.container.find('[data-textscaling-off]').each(function() {
		var el = jQuery(this);

		var expectedHeight = el.attr('data-height') * self.options.zoom;
		var actualHeight = el.outerHeight();
		var actualHeightContent = el.find(self.options.contentClass).outerHeight();
		var delta = Math.max(actualHeight, actualHeightContent) - expectedHeight;

		var id = el.attr('id').replace('_', '');

		var linked_heights = self.container.find('[data-height-source="' + id + '"]');
		linked_heights.each(function() {
			var linked_height = jQuery(this);
		
			var data_height = 1 * linked_height.attr('data-height');
			var original_height = data_height * self.options.zoom;

			if (delta < 0) delta = 0;

			linked_height.css('height', original_height + delta);
			
			if (linked_height.is('[data-textscaling-off="true"]')) {
				linked_height.find(self.options.contentClass).eq(0).css({
					height: data_height * self.options.zoom + delta
				});
			} else {
				linked_height.find(self.options.contentClass).eq(0).css({
					height: data_height + delta / self.options.zoom
				});
			}
		});

		var linked_tops = self.container.find('[data-top-source="' + id + '"]');
		linked_tops.each(function() {
			var linked_top = jQuery(this);
			var original_top = linked_top.attr('data-top') * self.options.zoom;

			linked_top.css('top', original_top + delta);
		});
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._initCanvas = function() {
	var self = this;
	self.container.resizable({
		start: function (event, ui) {
			self.options.selectionEnabled = false;
		},
		resize: function ( event, ui ) {
			if (self.options.grid_enabled) {
				var unit = self.options.grid_spacing * self.options.zoom;
				ui.size.width = Math.round(ui.size.width / unit) * unit;
				ui.size.height = Math.round(ui.size.height / unit) * unit;
			}
		},
		stop: function (event, ui) {
			self.setCanvasSize(ui.size.width, ui.size.height);
			setTimeout(function() {
				self.options.selectionEnabled = true;
			}, 1);
		}
	})
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._initElement = function(el, aspectRatio) {
	var self = this;

	el.find(self.options.debugClass).remove();
	var debug = jQuery('<div/>');
	debug.appendTo(el).addClass(self.options.debugClass.substring(1));

	el.resizable({
		aspectRatio: aspectRatio,
		resize: function ( event, ui ) {
			var snap = self._snapResize({
				width: ui.size.width,
				height: ui.size.height,
				left: ui.position.left,
				top: ui.position.top
			}, el, aspectRatio);

			ui.size.width = snap.width;
			ui.size.height = snap.height;

			self._updateElementData(snap, el);

			debug.html('w: ' + snap.width + ', h: ' + snap.height );
		},
		stop: function (event, ui) {
			var scaling = ui.size.width / ui.originalSize.width;
			if (el.is(self.options.carouselClass)) self._scaleCarousel(el, ui.size.width);
			if (el.is(self.options.groupClass)) self._scaleGroup(el, scaling);
			if (el.is(self.options.compareClass)) self._scaleCompare(el, ui.size.width);
			self._hideGuide();
			self._trigger('change');
		}
	}).draggable({
		distance: 3,
		start: function ( event, ui ) {
			//cache original position of other selected elements
			el.is(self.options.selectedClass) && self.container.find(self.options.selectedClass).not(el).each(function() {
				jQuery(this).data('orignalPosition', jQuery(this).position());
			});
		},
		drag: function ( event, ui ) {

			//determine drag direction
			var axis = "x";
			if (Math.abs(ui.position.top - ui.originalPosition.top) > Math.abs(ui.position.left - ui.originalPosition.left)) axis = "y";

			var snap = self._snapDrag({
				left: ui.position.left,
				top: ui.position.top
			}, el);
			ui.position.left = snap.left;
			ui.position.top = snap.top;

			//if shift key is pressed, lock to the prevalent direction
			if (event.shiftKey) {
				if (axis == 'x') {
					ui.position.top = ui.originalPosition.top;
				} else {
					ui.position.left = ui.originalPosition.left;
				}
			}

			//update position of other selected elements
			el.is(self.options.selectedClass) && self.container.find(self.options.selectedClass).not(el).each(function() {
				var selected_el = jQuery(this);
				var selected_originalPosition = selected_el.data('orignalPosition');
				var selected_snap = {
					top: selected_originalPosition.top + ui.position.top - ui.originalPosition.top,
					left: selected_originalPosition.left + ui.position.left - ui.originalPosition.left
				}
				selected_el.css(selected_snap);
				self._updateElementData(selected_snap, selected_el);
			});

			self._updateElementData(snap, el);

			debug.html('x: ' + ui.position.left + ', y: ' + ui.position.top);
		},
		stop: function (event, ui) {
			self._hideGuide();
			self._trigger('change');
		}
	});

	//if element is a group, disable draggable children
	if (el.has(self.options.groupedClass) || el.has(self.options.carouseledClass)) {
		var children = el.find(self.options.groupedClass + ', ' + self.options.carouseledClass);
		children.each(function() {
			self._initElementOff(jQuery(this));
		});
	}
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._initElementOff = function(el) {
	var self = this;
	el.find(self.options.debugClass).remove();
	el.hasClass('ui-resizable') && el.resizable('destroy');
	el.hasClass('ui-draggable') && el.draggable('destroy');
}

/**
 * Scale carousel
 * @param {DOM} el: element html node
*/
LSGrid.prototype._scaleCarousel = function(el) {
	var self = this;
	var margin = 0;
	var paddingLeft = 0;
	var paddingRight = 0;
	if (el.attr('data-margin')) margin = parseInt(el.attr('data-margin'), 10);
	if (el.attr('data-padding-left')) paddingLeft = parseInt(el.attr('data-padding-left'), 10);
	if (el.attr('data-padding-right')) paddingRight = parseInt(el.attr('data-padding-right'), 10);
	var width = (el.attr('data-width') - paddingLeft - paddingRight - margin * (el.attr('data-columns') - 1)) / el.attr('data-columns');
	var offset = 0;
	el.find(self.options.carouseledClass).each(function() {
		var child = jQuery(this);
		child.attr('data-top', 0);
		child.attr('data-left', offset);
		offset += (width + margin);
		if (child.is(self.options.groupClass)) {
			var _width = child.attr('data-width');
			var _height = child.attr('data-height');
			var scaling = width / _width;
			child.attr('data-width', width);
			child.attr('data-height', _height * scaling);
			self._positionElement(child, 1);
			self._scaleContent(child);
			self._scaleGroup(child, scaling);
		} else {
			var height = el.attr('data-height');
			child.attr('data-width', width);
			child.attr('data-height', height);
			self._positionElement(child, 1);
			self._scaleContent(child);
		}
	});

	self._carouselDots(el);
	self._carouselArrows(el);

}


/**
 * Manage carousel dots
 * @param {DOM} el: element html node
*/
LSGrid.prototype._carouselDots = function(el) {

	var self = this;
	var dotsContainer = el.find('.fpls-dots-container');
	if (dotsContainer.length < 1) return;
	var dataOffsetX = dotsContainer.attr('data-offset-x');
	var scaledaDataOffsetX;
	var dataOffsetY = dotsContainer.attr('data-offset-y');
	var scaledDataOffsetY = dataOffsetY * self.options.zoom;
	var dataColor = dotsContainer.attr('data-color');

	// X axis
	if (typeof dataOffsetX !== 'undefined'
		&& dataOffsetX !== false) {

		if (dataOffsetX >= 1) {
			scaledaDataOffsetX = 'calc(50% + '+ dataOffsetX * self.options.zoom + 'px)';
		} else if (dataOffsetX <= -1) {
			scaledaDataOffsetX = 'calc(50% - '+  dataOffsetX.replace('-', '') * self.options.zoom + 'px)';
		}

		dotsContainer.css({
			'left': scaledaDataOffsetX
		});
	};

	// Y axis
	if (typeof dataOffsetY !== 'undefined'
		&& dataOffsetY !== false) {
		dotsContainer.css({
			'bottom': scaledDataOffsetY,
			'z-index': '6'
		});
	};

	// Color
	if (typeof dataColor !== 'undefined'
		&& dataColor !== false) {
		dotsContainer[0].style.setProperty('--dotsColor', dataColor);
	};

}

/**
 * Manage carousel arrows
 * @param {DOM} el: element html node
*/
LSGrid.prototype._carouselArrows = function(el) {

	var self = this;
	var leftArrow = el.find('.fpls-element-prev');
	var dataLeft = leftArrow.attr('data-left');
	var rightArrow = el.find('.fpls-element-next');
	var dataRight = rightArrow.attr('data-right');

	leftArrow.css({
		'left': leftArrow.attr('data-left') * self.options.zoom,
		'margin-top': leftArrow.attr('data-top') * self.options.zoom
	});
	if (typeof dataLeft !== 'undefined' && dataLeft !== false) {
		leftArrow.css({
			'transform': 'translate(-50%, -50%)'
		});
	};

	rightArrow.css({
		'right': rightArrow.attr('data-right') * self.options.zoom,
		'margin-top': rightArrow.attr('data-top') * self.options.zoom
	});
	if (typeof dataRight !== 'undefined' && dataRight !== false) {
		rightArrow.css({
			'transform': 'translate(50%, -50%)'
		});
	};

}

/**
 * Scale group
 * @param {DOM} el: element html node,
 * @param {Float} scaling: how much the group need to be scaled
*/
LSGrid.prototype._scaleGroup = function(el, scaling) {
	var self = this;
	el.find(self.options.groupedClass).each(function() {
		var child = jQuery(this);

		var left = child.attr('data-left') * scaling;
		var top = child.attr('data-top') * scaling;
		var width = child.attr('data-width') * scaling;
		var height = child.attr('data-height') * scaling;

		child.attr('data-left', left);
		child.attr('data-top', top);
		child.attr('data-width', width);
		child.attr('data-height', height);

		self._positionElement(child, 1);
		self._scaleContent(child);
	});
}

/**
 * Scale compare
 * @param {DOM} el: element html node
*/
LSGrid.prototype._scaleCompare = function(el) {
	var self = this;
	var width = el.attr('data-width');
	el.find(self.options.comparedClass).each(function() {
		var child = jQuery(this);
		child.attr('data-top', 0);
		child.attr('data-left', 0);
		if (child.is(self.options.groupClass)) {
			var _width = child.attr('data-width');
			var _height = child.attr('data-height');
			var scaling = width / _width;
			child.attr('data-width', width);
			child.attr('data-height', _height * scaling);
			self._positionElement(child, 1);
			self._scaleContent(child);
			self._scaleGroup(child, scaling);
		} else {
			var height = el.attr('data-height');
			child.attr('data-width', width);
			child.attr('data-height', height);
			self._positionElement(child, 1);
			self._scaleContent(child, 1);
		}
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._updateElementData = function(data, el) {
	var self = this;
	if (data.left   !== undefined) el.attr('data-left',   Math.round(data.left   / self.options.zoom));
	if (data.top    !== undefined) el.attr('data-top',    Math.round(data.top    / self.options.zoom));
	if (data.width  !== undefined) el.attr('data-width',  Math.round(data.width  / self.options.zoom));
	if (data.height !== undefined) el.attr('data-height', Math.round(data.height / self.options.zoom));
	self._extendHeight(el);
	//scale content only for direct elements, not groups
	if (!el.is(self.options.groupClass)) {
		self._scaleContent(el);
	}
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._trigger = function(event, target) {
	var self = this;
	//trigger change event
	clearTimeout(self.timers[event]);
	if (!target) target = self.container;
	self.timers[event] = setTimeout(function() {
		target.trigger(event);
	}, 100);
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._getSnaps = function(el) {
	var self = this;
	var proximity = self.options.snap_proximity * self.options.zoom;
	var snaps_x = [
		0,
		self.container.width() / 2,
		self.container.width()
	];
	var snaps_y = [
		0,
		self.container.height() / 2,
		self.container.height()
	];

	self.container.find(self.options.elementClass + ', ' + self.options.groupClass)
		.not(self.options.selectedClass)
		.each(function() {
		if (jQuery(this).is(el) || el.has(this).length > 0) {
			return;
		}
		var distance = self._distance(jQuery(this), el);
		var left = jQuery(this).offset().left - self.container.offset().left;
		var center = left + jQuery(this).width() / 2;
		var right = left + jQuery(this).width();
		if (distance.y < proximity) {
			snaps_x.push(left);
			snaps_x.push(center);
			snaps_x.push(right);
		}
		var top = jQuery(this).offset().top - self.container.offset().top;
		var center = top + jQuery(this).height() / 2;
		var bottom = top + jQuery(this).height();
		if (distance.x < proximity) {
			snaps_y.push(top);
			snaps_y.push(center);
			snaps_y.push(bottom);
		}
	});

	return {
		x: snaps_x,
		y: snaps_y
	}
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._snapDrag = function(data, el) {
	var self = this;
	self._hideGuide();
	if (!self.options.guides_enabled) return data;

	var x1 = data.left;
	var x2 = data.left + el.width() / 2;
	var x3 = data.left + el.width();
	var y1 = data.top;
	var y2 = data.top + el.height() / 2;
	var y3 = data.top + el.height();
	var tolerance = self.options.snap_tolerance * self.options.zoom;

	var snaps = self._getSnaps(el);

	snaps.x.forEach(function(snap) {
		if (Math.abs(snap - x1) < tolerance) {
			data.left = snap;
			self._showGuide(snap, null);
		} else if (Math.abs(snap - x2) < tolerance) {
			data.left = snap - el.width() / 2;
			self._showGuide(snap, null);
		} else if (Math.abs(snap - x3) < tolerance) {
			data.left = snap - el.width();
			self._showGuide(snap, null);
		}
	});

	snaps.y.forEach(function(snap) {
		if (Math.abs(snap - y1) < tolerance) {
			data.top = snap;
			self._showGuide(null, snap);
		} else if (Math.abs(snap - y2) < tolerance) {
			data.top = snap - el.height() / 2;
			self._showGuide(null, snap);
		} else if (Math.abs(snap - y3) < tolerance) {
			data.top = snap - el.height();
			self._showGuide(null, snap);
		}
	});

	if (self.options.grid_enabled) {
		var unit = self.options.grid_spacing * self.options.zoom;
		data.left = Math.round(data.left / unit) * unit;
		data.top = Math.round(data.top / unit) * unit;
	}

	return data;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._snapResize = function(data, el, aspectRatio) {
	var self = this;
	self._hideGuide();
	if (!self.options.guides_enabled) return data;
	var x1 = data.left + data.width;
	var y1 = data.top + data.height;
	var tolerance = self.options.snap_tolerance * self.options.zoom;
	var ratio = data.width / data.height;

	var snaps = self._getSnaps(el);

	snaps.x.forEach(function(snap) {
		if (Math.abs(snap - x1) < tolerance) {
			data.width = snap - data.left;
			if (aspectRatio) data.height = data.width / ratio;
			self._showGuide(snap, null);
		}
	});
	snaps.y.forEach(function(snap) {
		if (Math.abs(snap - y1) < tolerance) {
			data.height = snap - data.top;
			if (aspectRatio) data.width = data.height * ratio;
			self._showGuide(null, snap);
		}
	});

	if (self.options.grid_enabled) {
		var unit = self.options.grid_spacing * self.options.zoom;
		data.width = Math.round(data.width / unit) * unit;
		data.height = Math.round(data.height / unit) * unit;
	}

	return data;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._hideGuide = function() {
	var self = this;
	jQuery(self.options.guideClassX).css({
		opacity: 0
	});
	jQuery(self.options.guideClassY).css({
		opacity: 0
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._showGuide = function(x, y) {
	var self = this;
	if (x) jQuery(self.options.guideClassX).css({
		opacity: 1,
		left: x
	});
	if (y) jQuery(self.options.guideClassY).css({
		opacity: 1,
		top: y
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._distance = function(a, b) {
	var self = this;
	var a_left = a.offset().left;
	var b_left = b.offset().left;
	var a_right = a_left + a.width();
	var b_right = b_left + b.width();
	var delta_x = Math.max(a_left, b_left) - Math.min(a_right, b_right);
	if (delta_x < 0) delta_x = 0;

	var a_top = a.offset().top;
	var b_top = b.offset().top;
	var a_bottom = a_top + a.height();
	var b_bottom = b_top + b.height();
	var delta_y = Math.max(a_top, b_top) - Math.min(a_bottom, b_bottom);
	if (delta_y < 0) delta_y = 0;

	return {
		x: delta_x,
		y: delta_y
	}
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._inRange = function(el, selection) {
	return (el.position().left > selection.left && 
		el.position().left + el.outerWidth() < selection.right &&
		el.position().top > selection.top &&
		el.position().top + el.outerHeight() < selection.bottom );
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._normalizeSelection = function(coord1, coord2) {
	var out = {};
	out.top = Math.min(coord1.y, coord2.y);
	out.left = Math.min(coord1.x, coord2.x);
	out.bottom = Math.max(coord1.y, coord2.y);
	out.right = Math.max(coord1.x, coord2.x);
	return out;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.showGrid = function(grid) {
	var self = this;
	self.setGrid(grid); 
	var Canvas = document.createElement("canvas");
	var context = Canvas.getContext('2d');
	var unit = self.options.grid_spacing * self.options.zoom * 2;
	context.canvas.width = unit;
	context.canvas.height = unit;
	context.moveTo(0, 0);
	context.lineTo(0, unit);
	context.moveTo(0, 0);
	context.lineTo(unit, 0);
	context.strokeStyle = "#808080";
	var dash_width = unit / 8;
	//set min and max width for the dashes
	if (dash_width > 8) dash_width = 8;
	if (dash_width < 4) dash_width = 4;
	context.setLineDash([dash_width, unit - dash_width * 2 + 1, dash_width - 1]);
	context.stroke();

	self.grid && self.grid.css({
		'background-image':"url(" + Canvas.toDataURL("image/png")+ ")",
		'background-size': unit / 2
	});

	self.grid.show();
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype.hideGrid = function() {
	var self = this;
	self.grid.hide();
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._getSelectable = function() {
	var self = this;
	var _selector = self.options.elementClass; //take all elements
	_selector += ', ' + self.options.groupClass; //and all groups
	_selector += ', ' + self.options.carouselClass; //and all carousels
	_selector += ', ' + self.options.compareClass; //and all compares
	return self.container.find(_selector)
		.not(self.options.carouseledClass)
		.not(self.options.groupedClass)
		.not(self.options.comparedClass)
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._enableSelection = function() {
	var self = this;
	self._select = {
		status: false,
		coord1: {x:0, y:0},
		coord2: {x:0, y:0}
	};

	//Direct selection
	self._getSelectable()
		.off('mousedown.selection')
		.on('mousedown.selection', function(e) {
		if (!self.options.selectionEnabled) return;

		self._elMousedown = e;
		e.stopPropagation();
	});

	self._getSelectable()
		.off('mouseup.selection')
		.on('mouseup.selection', function(e) {

		var el = jQuery(this);

		if (!self._elMousedown) return;
		if (Math.abs(e.pageX - self._elMousedown.pageX) > 3 || Math.abs(e.pageY - self._elMousedown.pageY) > 3) return;

		if (!e.shiftKey) {
			self.container.find(self.options.selectedClass).removeClass(self.options.selectedClass.substring(1));
		}

		el.addClass(self.options.selectedClass.substring(1));
		self._trigger('selection');
		self._elMouedown = false;
	});

	//Range selection
	self.selectionWrapper.off('mousedown.selection').on('mousedown.selection', function(e) {
		if (!self.options.selectionEnabled) return;

		if (e.target != e.currentTarget && jQuery(e.target).parents(e.currentTarget).length < 1) {
			return true;
		}
		
		self._select.status = true;
		self.container.find(self.options.selectionOverlay).show();
		self._select.coord1 = {
			x: e.pageX - self.container.offset().left,
			y: e.pageY - self.container.offset().top
		}
		self._select.coord2 = {
			x: e.pageX - self.container.offset().left,
			y: e.pageY - self.container.offset().top	
		}
		var selection = self._normalizeSelection(self._select.coord1, self._select.coord2);
		self._setSelectionBox(selection);
	});

	self.selectionWrapper.off('mousemove.selection').on('mousemove.selection', function(e) {
		if (!self.options.selectionEnabled) return;

		if (self._select.status == true) {
			e.preventDefault();
			self._select.coord2 = {
				x: e.pageX - self.container.offset().left,
				y: e.pageY - self.container.offset().top
			}
			var selection = self._normalizeSelection(self._select.coord1, self._select.coord2);
			self._setSelectionBox(selection);
		}
	});

	jQuery('body').off('mouseup.selection').on('mouseup.selection', function(e) {
		if (!self._select.status) return true;
		self._select.status = false;
		self.container.find(self.options.selectionOverlay).hide();
		var selection = self._normalizeSelection(self._select.coord1, self._select.coord2);

		self.container.trigger('selection', selection);

		//detect elements in range
		self._getSelectable()
			.each(function() {
			var el = jQuery(this);
			if (self._inRange(el, selection)) {
				el.addClass(self.options.selectedClass.substring(1));
			} else {
				el.removeClass(self.options.selectedClass.substring(1));
			}
		});
		self._trigger('selection');
		self.container.find(self.options.selectionOverlay).hide();
	});

	jQuery('body').off('mouseout.selection').on('mouseout.selection', function(e) {
		if (e.relatedTarget == jQuery('body')[0] ||
			jQuery.contains(jQuery('body')[0], e.relatedTarget)) {
			
		} else {
			//annulla drag
			self._select.status = false;
			self.container.find(self.options.selectionOverlay).hide();
		}
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._disableSelection = function() {
	var self = this;
	
	//Direct selection
	self._getSelectable()
		.off('mousedown.selection')
		.off('mouseup.selection');

	self.container.off('mousedown.selection');
	self.container.off('mousemove.selection');
	jQuery('body').off('mouseup.selection');
	jQuery('body').off('mouseout.selection');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._setSelectionBox = function(selection) {
	var self = this;
	self.container.find(self.options.selectionOverlay).css({
		left: selection.left,
		top: selection.top,
		width: selection.right - selection.left,
		height: selection.bottom - selection.top
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._group = function(elements, callback) {
	var self = this;
	var min_x = 999999999;
	var min_y = 999999999;
	var max_x = 0;
	var max_y = 0;
	var selected = true;

	var group = jQuery('<div />');
	group.addClass(self.options.groupClass.substring(1));
	group.insertBefore(elements.eq(0));
	elements.addClass(self.options.groupedClass.substring(1)).detach().appendTo(group);

	elements.each(function() {
		var el = jQuery(this);
		min_x = Math.min(min_x, parseFloat(el.attr('data-left')));
		min_y = Math.min(min_y, parseFloat(el.attr('data-top')));
		max_x = Math.max(max_x, parseFloat(el.attr('data-left')) + parseFloat(el.attr('data-width')));
		max_y = Math.max(max_y, parseFloat(el.attr('data-top')) + parseFloat(el.attr('data-height')));
		selected = selected && el.hasClass(self.options.selectedClass.substring(1));
	});

	group.attr('data-left', min_x);
	group.attr('data-top', min_y);
	group.attr('data-width', max_x - min_x);
	group.attr('data-height', max_y - min_y);

	elements.each(function() {
		var el = jQuery(this);
		var left = parseFloat(el.attr('data-left')) - min_x;
		var top = parseFloat(el.attr('data-top')) - min_y;
		el.attr('data-left', left);
		el.attr('data-top', top);
	});

	if (selected) {
		group.addClass(self.options.selectedClass.substring(1));
		elements.removeClass(self.options.selectedClass.substring(1));
	}

	self._initElement(group, true);
	self._scaleCanvas();
	self._enableSelection();
	self.bringToFront(group);
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._ungroup = function(group) {
	var self = this;
	if (!group.hasClass(self.options.groupClass.substring(1))) return;
	var children = group.find(self.options.groupedClass);
	children.each(function() {
		var child = jQuery(this);

		var child_left = parseFloat(child.attr('data-left'));
		var group_left = parseFloat(group.attr('data-left'));

		child.attr('data-left', child_left + group_left);

		var child_top = parseFloat(child.attr('data-top'));
		var group_top = parseFloat(group.attr('data-top'));

		child.attr('data-top', child_top + group_top);

		child.detach().removeClass(self.options.groupedClass.substring(1)).insertAfter(group);

		//maintain selection state
		if (group.hasClass(self.options.selectedClass.substring(1))) child.addClass(self.options.selectedClass.substring(1));
		self._initElement(child);
	});
	group.remove();
	self._scaleCanvas();
	self._enableSelection();
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._enableKeys = function() {
	var self = this;
	jQuery('body').off('keydown.grid').on('keydown.grid', function(e) {
		var delta_x = 0;
		var delta_y = 0;

		var step = 1 / self.options.zoom;

		if (self.options.grid_enabled) {
			step = self.options.grid_spacing * self.options.zoom;
		}

		if (e.shiftKey) step *= 10;

		//disable arrows if focused on an input element
		if (jQuery(e.target).is('input,select,textarea')) return;

	    switch(e.which) {
	        case 37: // left
	        delta_x = -step;
	        break;

	        case 38: // up
	        delta_y = -step;
	        break;

	        case 39: // right
	        delta_x = step;
	        break;

	        case 40: // down
	        delta_y = step;
	        break;

	        default: return; // exit this handler for other keys
	    }

	    if (self.container.find(self.options.selectedClass).length > 0) e.preventDefault();

	    self.container.find(self.options.selectedClass).each(function() {
	    	var el = jQuery(this);
	    	var snap = {
	    		left: el.position().left + delta_x,
	    		top: el.position().top + delta_y
	    	}
	    	el.css(snap);
	    	self._updateElementData(snap, el);
	    });
	});
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._disableKeys = function() {
	var self = this;
	jQuery('body').off('keydown.grid');
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._size = function(el) {
	var self = this;
	var out = {
		left: el.position().left,
		right: el.position().left + el.outerWidth(),
		centerx: el.position().left + el.outerWidth() / 2,
		top: el.position().top,
		bottom: el.position().top + el.outerHeight(),
		centery: el.position().top + el.outerHeight() / 2,
		width: el.outerWidth(),
		height: el.outerHeight(),
		halfwidth: el.outerWidth() / 2,
		halfheight: el.outerHeight() / 2
	}
	return out;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._getMaxZIndex = function() {
	var self = this;
	var els = self.container.find(self.options.elementClass + ', ' + self.options.groupClass + ', ' + self.options.carouselClass);
	var maxZIndex = 1;
	els.each(function() {
		var zindex = parseFloat(jQuery(this).attr('data-zindex') || 1);
		if (zindex > maxZIndex) maxZIndex = zindex;
	});
	return maxZIndex;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._getMinZIndex = function() {
	var self = this;
	var els = self.container.find(self.options.elementClass + ', ' + self.options.groupClass + ', ' + self.options.carouselClass);
	var minZIndex = 9999999999;
	els.each(function() {
		var zindex = parseFloat(jQuery(this).attr('data-zindex') || 1);
		if (zindex < minZIndex) minZIndex = zindex;
	});
	return minZIndex;
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._setZIndex = function(el, zindex) {
	var self = this;
	el.css({
		zIndex: zindex
	}).attr('data-zindex', zindex);
}

/**
 * USED ONLY IN CONSOLE!
*/
LSGrid.prototype._normalizeZIndexes = function() {
	var self = this;
	var delta = 1 - self._getMinZIndex();
	var els = self.container.find(self.options.elementClass + ', ' + self.options.groupClass + ', ' + self.options.carouselClass);
	els.each(function() {
		var zindex = parseFloat(jQuery(this).attr('data-zindex') || 1);
		zindex += delta;
		self._setZIndex(jQuery(this), zindex);
	});
}