var LSWall_freegrid2_demopartner_demo_example_3 = function() {
    this.LSPopup = LSPopup_wall_freegrid2_demopartner_demo_example_3;
    // this.wallTpl = LSTemplates.wall_freegrid2_demopartner_demo_example_3.wall;
    // this.LSBox_post = LSBox_post_demopartner_demo_example_3;
    // this.LSBox_asset = LSBox_asset_demopartner_demo_example_3;
    // this.LSBox_block = LSBox_block_demopartner_demo_example_3;
    // this.LSBox_embed = LSBox_embed_demopartner_demo_example_3;
    // this.LSEmbed = LSEmbed_demopartner_demo_example_3;
}
LSHelpers.extend(LSWall_freegrid2_demopartner_demo_example_3, LSWall_freegrid2_default);

LSWall_freegrid2_demopartner_demo_example_3.prototype.renderWall = function(data) {
    LSWall_freegrid2_default.prototype.renderWall.call(this, data);

    console.log('Freegrid2 example');
}