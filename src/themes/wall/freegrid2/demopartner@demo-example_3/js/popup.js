var LSPopup_wall_freegrid2_demopartner_demo_example_3 = function() {
    this.popupTpl = LSTemplates.wall_freegrid2_demopartner_demo_example_3.popup;
}

LSHelpers.extend(LSPopup_wall_freegrid2_demopartner_demo_example_3, LSPopup);

LSPopup_wall_freegrid2_demopartner_demo_example_3.prototype.showPopup = function(pin) {

    var self = this;
    self.parent.showPopup.call(this, pin);

    console.log('Popup opened');

}

LSPopup_wall_freegrid2_demopartner_demo_example_3.prototype.mapContext = function() {

    var self = this;

    var context = LSPopup.prototype.mapContext.call(self);

    // Added custom label for the username
    if (self.wallInstance.wall.uiLabels && self.wallInstance.wall.uiLabels.popupAuthor) {
        context.username = self.wallInstance.wall.uiLabels.popupAuthor;
    }

    return context;

}