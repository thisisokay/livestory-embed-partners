var LSWallgroup_global_slide = function() {
    this.tabContainerTpl = LSTemplates.wallgroup_global_slide.tabs;
}

LSHelpers.extend(LSWallgroup_global_slide, LSWallgroup_default);

// LSWallgroup_global_slide.prototype.initContainers = function() {
//     var self = this;
//     LSHelpers.debug('Wallgroup initContainers');
//     self.mainContainer.append('<div class="fpls-slide-container"></div>');
//     self.slideWrapper = self.mainContainer.find('.fpls-slide-container');
//     $ls.each(self.walls, function(key, wall) {
//         wall.wallContainer = $ls('<div class="fpls-wall-container"></div>').appendTo(self.slideWrapper);
//     });

//     //listen to wall loading complete
//     self.mainContainer.on('ls-wall-complete', 'div',  function() {
//         self.setHeight();
//     });

//     self.mainContainer.on('ls-refresh', function() {
//         self.setHeight();
//     });

//     //Support drag events
//     self.mainContainer.off('touchstart.ls-slide').on('touchstart.ls-slide', function(e) {
//         self.touchstart(e);
//     });
//     self.mainContainer.off('touchend.ls-slide').on('touchend.ls-slide', function(e) {
//         self.touchend(e);
//     });
//     self.mainContainer.off('touchmove.ls-slide').on('touchmove.ls-slide', function(e) {
//         self.touchmove(e);
//     });
// }

LSWallgroup_global_slide.prototype.initContainers = function() {
    var self = this;
    LSHelpers.debug('Wallgroup initContainers');
    LSWallgroup_default.prototype.initContainers.call(this);

    self.wallgroupContainer.addClass('fpls-slide-container');
    self.slideWrapper = self.wallgroupContainer;

    //listen to wall loading complete
    self.mainContainer.on('ls-wall-complete', 'div',  function() {
        self.setHeight();
    });

    self.mainContainer.on('ls-refresh', function() {
        self.setHeight();
    });

    //Support drag events
    self.mainContainer.off('touchstart.ls-slide').on('touchstart.ls-slide', function(e) {
        self.touchstart(e);
    });
    self.mainContainer.off('touchend.ls-slide').on('touchend.ls-slide', function(e) {
        self.touchend(e);
    });
    self.mainContainer.off('touchmove.ls-slide').on('touchmove.ls-slide', function(e) {
        self.touchmove(e);
    });
}

LSWallgroup_global_slide.prototype.initTabs = function() {
    var self = this;
    LSWallgroup_default.prototype.initTabs.call(this);
    $ls.each(self.walls, function(key, wall) {
        if (!wall.htmlLoaded) {
            self.loadWall(wall);
        }
    });

    self.initDots();

    self.handleThemeOptions();

    self.time = 6000;

    if (self.wallgroup.themeOptions && self.wallgroup.themeOptions.setInterval) {
        self.time = self.wallgroup.themeOptions.setInterval * 1000;
    }

    //if interacting with the wall, cancel the loop
    self.mainContainer.on('click.ls', '.fpls-wall-container', function(e) {
        clearTimeout(self.slideshowTimeout);
    });
}

LSWallgroup_global_slide.prototype.showTab = function(wall, params) {
    LSHelpers.debug('Wallgroup showTab');
    var self = this;

    if (self.__changing) {
        return;
    }
    self.__changing = true;

    clearTimeout(self.slideshowTimeout);

    //if switching to the same wall, do nothing
    if (self.__prevWall == wall) return;

    if (self.__prevWall) {
        //find direction
        dir = self.findDirection(self.__prevWall, wall);
        if (params && params.direction) dir = params.direction;

        if (dir == 'next') {
            wall.wallContainer.css('left', '105%').animate({ left: 0 });
            self.__prevWall.wallContainer.css('left', 0).animate({ left: '-105%' }, function() { self.__changing = false; });
        } else {
            wall.wallContainer.css('left', '-105%').animate({ left: 0 });
            self.__prevWall.wallContainer.css('left', 0).animate({ left: '105%' }, function() { self.__changing = false; });
        }

        //remove class from current slide
        self.__prevWall.wallContainer.removeClass('current');
    } else {
        $ls.each(self.walls, function(key, wall) {
            wall.wallContainer.css('left', '105%');
        });
        wall.wallContainer.css('left', 0);
        self.__changing = false;
    }

    //set correct class
    wall.wallContainer.addClass('current');

    self.__prevWall = wall;

    //set correct dots
    self.setDotState(wall.wall_id);
    self.setTabState(wall.wall_id);

    if (self.time > 0) self.slideshowTimeout = setTimeout(function() {
        self.tabContainer.find('.fpls-next').trigger('click.ls');
    }, self.time);

    self.setHeight();
}

LSWallgroup_global_slide.prototype.findDirection = function(prev, next) {
    var self = this;

    var tabs = self.tabContainer.find('.fpls-wallgroup-tab a');
    var prevIndex = tabs.index(tabs.filter('[data-target="' + prev.wall_id + '"]'));
    var nextIndex = tabs.index(tabs.filter('[data-target="' + next.wall_id + '"]'));

    //if i'm going from the last one to the first one, it means I reached the end
    if (nextIndex == 0 && prevIndex == tabs.length - 1) return 'next';

    //if i'm going from the first one to the last one, it means I reached the beginning
    if (nextIndex == tabs.length - 1 && prevIndex == 0) return 'prev';

    if (nextIndex > prevIndex) return 'next';
    if (nextIndex <= prevIndex) return 'prev';
}

LSWallgroup_global_slide.prototype.setHeight = function() {
    var self = this;
    clearTimeout(self.__heightTimer);
    self.__heightTimer = setTimeout(function() {
        var h = self.mainContainer.find('.fpls-wall-container.current').outerHeight();
        self.slideWrapper.css('min-height', h);
    }, 100);
}

LSWallgroup_global_slide.prototype.initDots = function() {
    var self = this;

    var dots = self.tabContainer.find('.fpls-dot');
    var arrowNext = self.tabContainer.find('.fpls-next');
    var arrowPrev = self.tabContainer.find('.fpls-prev');
    var tabs = self.tabContainer.find('.fpls-wallgroup-tab a');

    dots.on('click.ls', function(e) {
        e.preventDefault();
        var index = dots.index(this);
        tabs.eq(index).trigger('click.ls');
    });

    arrowNext.on('click.ls', function(e) {
        e.preventDefault();
        var index = tabs.index(tabs.filter('.current'));
        index++;
        if (index >= tabs.length) index = 0;
        tabs.eq(index).trigger('click.ls', { direction: 'next' });
    });

    arrowPrev.on('click.ls', function(e) {
        e.preventDefault();
        var index = tabs.index(tabs.filter('.current'));
        index--;
        if (index < 0) index = tabs.length - 1;
        tabs.eq(index).trigger('click.ls', { direction: 'prev' });
    });

}

LSWallgroup_global_slide.prototype.setDotState = function(wall_id) {
    var dots = this.tabContainer.find('.fpls-dot');
    dots.removeClass('current');
    dots.filter('[data-target="' + wall_id + '"]').addClass('current');
}

LSWallgroup_global_slide.prototype.touchstart = function(e) {
    var self = this;
    self.touchEnabled = true;
    var pageX = e.originalEvent.targetTouches[0].pageX;
    self.touchPosition = pageX;
    self.touchDelta = 0;
    self.slideWrapper.css({
       'transition': 'transform 0ms'
    });
}

LSWallgroup_global_slide.prototype.touchmove = function(e) {
    var self = this;
    var pageX = e.originalEvent.targetTouches[0].pageX;
    self.touchDelta = pageX - self.touchPosition;
    if (Math.abs(self.touchDelta) > 25) {
        e.preventDefault();
        self.slideWrapper.css({
            transform: "translateX(" + self.touchDelta / 4 + "px)"
        });
    }
}

LSWallgroup_global_slide.prototype.touchend = function(e) {
    var self = this;
    self.touchEnabled = false;

    var arrowNext = this.tabContainer.find('.fpls-next');
    var arrowPrev = this.tabContainer.find('.fpls-prev');

    if (Math.abs(self.touchDelta) > 100) {
        if (self.touchDelta < 0) {
            arrowNext.trigger('click.ls');
        } else {
            arrowPrev.trigger('click.ls');
        }
    }

    self.slideWrapper.css({
        'transition': 'transform 150ms'
    });

    self.slideWrapper.css({
        transform: "translateX(" + 0 + "px)"
    });
}

LSWallgroup_global_slide.prototype.handleThemeOptions = function() {
    var self = this;
    var tabs = self.tabContainer.find('.fpls-wallgroup-tabs');

    if (self.wallgroup.themeOptions && self.wallgroup.themeOptions.hideTabs === true) {
        tabs.hide();
    } else {
        tabs.show();
    }

    var arrowNext = self.tabContainer.find('.fpls-next');
    var arrowPrev = self.tabContainer.find('.fpls-prev');

    if (self.wallgroup.themeOptions && self.wallgroup.themeOptions.hideArrows === true) {
        arrowNext.hide();
        arrowPrev.hide();
    } else {
        arrowNext.show();
        arrowPrev.show();
    }

    var dots = self.tabContainer.find('.fpls-dots .fpls-dot');

    if (self.wallgroup.themeOptions && self.wallgroup.themeOptions.hideDots === true) {
        dots.hide();
    } else {
        dots.show();
    }
}