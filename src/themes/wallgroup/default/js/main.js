/**
 * LSWallgroup_default
 * @constructor
 * Class for wallgroups (destinations)
*/
var LSWallgroup_default = function() {
    this.tabContainerTpl = LSTemplates.wallgroup_default.tabs;
    this.wallContainerTpl = LSTemplates.wallgroup_default.tab;
}

/**
 * Init method for wallgroup
 * @param {String} id - ID of the wallgroup
 * @param {Object} options - Wallgroup options
 * @param {DOM} mainContainer - outermost div of LS, the one with data-store
 */
LSWallgroup_default.prototype.init = function(id, options, mainContainer, rootContainer) {
    this.walls = {};
    this.tabs = {};
    this.id = id;
    this.options = options;
    this.mainContainer = mainContainer || $ls('#' + this.id);
    this.rootContainer = rootContainer || self.mainContainer;
}

/**
 * Parse Wallgroup JSON
 * it calls all the different methods needed to render the wallgroup
 * @params {JSON} wallgroup: Wallgroup JSON coming from APIs
 */
LSWallgroup_default.prototype.parseJSON = function(wallgroup) {
    LSHelpers.debug('Wallgroup parseJSON');
    var self = this;
    this.wallgroup = wallgroup;

    if (wallgroup.walls.length) {
        for (var i = 0; i < wallgroup.walls.length; i++) {
            this.tabs[wallgroup.walls[i].wall_id] = {
                title: wallgroup.walls[i].title,
                id: wallgroup.walls[i].wall_id
            }
            this.walls[wallgroup.walls[i].wall_id] = wallgroup.walls[i];
        }
        this.loadThemeAssets();
        this.addCssClasses();
        this.initContainers();
        this.initTabs();
        self.sendAnalytics();
    } else {
        this.__checkSSR();
    }
}

/**
 * Assemble API url to get wallgroup payload
 * @returns {String} url of the API to call
 */
LSWallgroup_default.prototype.getUrl = function() {
    var self = this;
    var url = LSConfig.apiURL + 'wallgroup/' + self.wallgroup._id;
    var store_code = self.mainContainer.data('store');
    var lang_code = self.mainContainer.data('lang');

    //if no store or lang passed, take it from the browser
    if (store_code == 'STORE_ID' && !lang_code) {
        lang_code = LSHelpers.defaultLang();
    }

    //check if specific AB test of this wallgroup is in the session of the user
    var assignedExpID = LSHelpers.getSessionStorage('LS_ABEXP_ID_' + self.wallgroup._id);

    //if experience id is passed via querystring, use it
    var qsExperienceID = LSHelpers.parseQuerystring('experience_id');
    if (qsExperienceID) assignedExpID = qsExperienceID;

    if (assignedExpID) url += '/' + assignedExpID;

    url = LSHelpers.appendFilters(url, {
        hashtags: self.mainContainer.data('hashtag'),
        skus: self.mainContainer.data('sku'),
        store_code: store_code,
        lang_code: lang_code,
        post_type: self.mainContainer.data('type')
    });

    return url;
}

/**
 * Append all the css required by the wallgroup
 * @param {Function} callback: function called when all css are loaded
 */
LSWallgroup_default.prototype.loadThemeAssets = function() {
    var self = this;
    LSHelpers.debug('Wallgroup loadThemeAssets');
    this.parent_theme = LSHelpers.getDefaultTheme(this.wallgroup.theme);

    var cssWallgroupPrefix = '.fpls-' + this.wallgroup._id;
    
    // Append wallgroup CSS
    if (this.parent_theme) LSHelpers.appendCss(LSHelpers.getAssetURL(this.parent_theme) + '/css/' + this.parent_theme + '.css', null, self.rootContainer);
    LSHelpers.appendCss(LSHelpers.getAssetURL(this.wallgroup.theme) + '/css/' + this.wallgroup.theme + '.css', null, self.rootContainer);

    // Inject wallgroup style, "namespaced" by wallgroup id
    LSHelpers.injectCss(LSHelpers.getAssetURL(this.wallgroup.style) + '/css/' + this.wallgroup.style + '.css', cssWallgroupPrefix, self.mainContainer, this.wallgroup.style);

    //Inject custom inline CSS for this wall group
    if (this.wallgroup.customcss) LSHelpers.injectInlineCss(this.wallgroup.customcss, cssWallgroupPrefix, self.mainContainer, this.wallgroup.theme);

    // Inject fonts for the wall group
    LSHelpers.injectFonts(LSHelpers.getAssetURL(this.wallgroup.theme) + '/css/' + this.wallgroup.theme + '-fonts.css', 
        this.wallgroup._id,
        this.wallgroup.font_body, 
        this.wallgroup.font_heading, 
        self.mainContainer);
}

/**
 * Add classes to mainContainar according to wallgroup theme
 * Determine if wallgorup has SSR (server-side rendering) or not
 */
LSWallgroup_default.prototype.addCssClasses = function() {
    var self = this;
    LSHelpers.debug('Wallgroup addCssClasses');
    // Add CSS classes for style and theme
    if (self.wallgroup.theme.indexOf('@') > 0) {
        self.mainContainer.addClass('fpls-' + self.wallgroup.theme.replace(/@/g, '')); //legacy
        self.mainContainer.addClass('fpls-' + self.wallgroup.theme.replace(/@/g, '-'));
    } else {
        self.mainContainer.addClass('fpls-' + self.wallgroup.theme);
    }

    self.mainContainer.addClass('fpls-' + self.id.replace('ls-', ''));
    if (self.parent_theme) self.mainContainer.addClass('fpls-' + self.parent_theme);

    this.__checkSSR();
}

LSWallgroup_default.prototype.__checkSSR = function() {
    var self = this;
    if (self.mainContainer.find('[data-ssr="true"]').length > 0) {
        self.isSSR = true;
    } else {
        self.isSSR = false;
    }

    try {
        if (self.isSSR) {
            var ssrIds = [];
            self.mainContainer.find('.fpls-wallgroup-container').eq(0).find('.fpls-wall-container[id]').each(function() {
                ssrIds.push($ls(this).attr('id').replace('ssr', ''));
            });
            ssrIds = ssrIds;
            var apiIds = Object.keys(self.walls);

            if (ssrIds.join(',') != apiIds.join(',')) {
                //remove missing layouts
                var missing = ssrIds.filter(function(id) {
                    return apiIds.indexOf(id) < 0;
                });

                missing.forEach(function(id) {
                    self.mainContainer.find('#ssr' + id).remove();
                });
                /*
                LSHelpers.warn('SSR to refresh');
                self.isSSR = false;
                self.mainContainer.attr('data-ssr-status', 'not sync');
                self.mainContainer.find('.fpls-wallgroup-container').remove();
                */
               self.outatedSSR = true;
            }
        }
    } catch(err) {
        LSHelpers.error(err);
    }
}

/**
 * Prepare the DIV containers for each wall
 */
LSWallgroup_default.prototype.initContainers = function() {
    var self = this;
    LSHelpers.debug('Wallgroup initContainers');

    //DIV that will contain all the walls
    if (self.isSSR) {
        //SSR
        self.wallgroupContainer = self.mainContainer.find('.fpls-wallgroup-container');
        if (self.wallgroupContainer.length < 1) {
            //No DIV found SSR
            self.wallgroupContainer = $ls('<div class="fpls-wallgroup-container" />');
            self.wallgroupContainer.appendTo(self.mainContainer);    
        }
        self.wallgroupContainer.find('> .fpls-wall-container').addClass('fpls-wall-container--ssr'); //keep track of ssr divs
    } else {
        //NO SSR
        self.wallgroupContainer = $ls('<div class="fpls-wallgroup-container" />');
        self.wallgroupContainer.appendTo(self.mainContainer);
    }

    ssrNeedsRefresh = false;

    for (var key in self.walls) {
        //Add one DIV for each wall
        if (self.isSSR) {
            //SSR
            self.walls[key].wallContainer = self.mainContainer.find('#ssr' + key);
            if (self.walls[key].wallContainer.length < 1) {
                //No DIV found SSR
                self.walls[key].wallContainer = $ls('<div class="fpls-wall-container" id="ssr' + key + '"></div>');
                self.walls[key].wallContainer.appendTo(self.wallgroupContainer);
                ssrNeedsRefresh = true;
            } else if (self.outatedSSR) {
                self.walls[key].wallContainer.detach();
                self.walls[key].wallContainer.appendTo(self.wallgroupContainer);
                ssrNeedsRefresh = true;
            }
        } else {
            //NO SSR
            self.walls[key].wallContainer = $ls('<div class="fpls-wall-container" id="ssr' + key + '"></div>');
            self.walls[key].wallContainer.appendTo(self.wallgroupContainer);
        }
        self.walls[key].wallContainer.removeClass('fpls-wall-container--ssr'); //keep track of SSR divs that have been reused
    }
    //remove walls from server that are not in the wallgroup
    self.wallgroupContainer.find('.fpls-wall-container--ssr').remove();
}

/**
 * Initialize tabs functionality
 */
LSWallgroup_default.prototype.initTabs = function() {
    LSHelpers.debug('Wallgroup initTabs');
    var self = this;
    
    if (Object.keys(self.tabs).length > 1) {

        //If more than one tab:
        self.tabContainer = $ls(self.tabContainerTpl({tabs: self.tabs}));

        //Get class of the template
        var tabContainerClass = self.tabContainer.eq(0).attr('class');

        if (self.isSSR && tabContainerClass) {
            //SSR
            //always replace tabs coming from SSR, since we might have new elements
            var existingTabs = self.wallgroupContainer.find('.' + tabContainerClass);
            if (existingTabs.length > 0) {
                existingTabs.replaceWith(self.tabContainer);
            } else {
                //no tabs found SSR
                self.tabContainer.prependTo(self.wallgroupContainer);
            }
        } else {
            //NO SSR
            self.tabContainer.prependTo(self.wallgroupContainer);
        }

        //Find tabs
        var tabs = self.tabContainer.find('.fpls-wallgroup-tab a');

        //Detect tabs click
        tabs.on('click.ls', function(e, params) {
            e.preventDefault();

            if (tabs.length > 1 && self.wallgroup.fragment_navigation) {
                //If fragment navigation is active (eg #tab1) append fragment when clicking on tabs
                self.pushTab(LSHelpers.createFragment( $ls(this).text() ));
            }

            self.switchTab($ls(this).data('target'), params);
        });

        //Check if there is a fragment in URL
        var fragment = LSHelpers.parseFragment();
        
        var found = false;

        if (fragment) {
            //If there is a fragment, check if it matches one of the tabs
            tabs.each(function() {
                if (LSHelpers.createFragment($ls(this).text()) == fragment) {
                    found = true;
                    //If found, show that tab
                    $ls(this).trigger('click.ls');
                }
            });
        }

        //if no fragment or fragment not found, show the first one
        if (!found) tabs.eq(0).trigger('click.ls');
    } else {

        //If only one tab: show it
        self.switchTab(this.tabs[Object.keys(this.tabs)[0]].id);
    }
}

/**
 * Initialize tabs functionality
 * @param {String} wall_id: ID of wall to show
 * @param {Obhect} params: additional params
 */
LSWallgroup_default.prototype.switchTab = function(wall_id, params) {
    LSHelpers.debug('Wallgroup switchTab');
    var self = this;
    self.mainContainer.trigger('switch.tab.ls');
    if (!wall_id) return;

    var target = this.walls[wall_id];

    if (target.htmlLoaded) {
        //If tab has already been shown, just activate it and refersh the wall inside it
        this.showTab(target, params);
        // refresh rendering
        target.wallInstance && target.wallInstance.refresh();
        // send page view
        target.wallInstance && target.wallInstance.sendAnalytics();
    } else {
        //Tab never shown, add handlebar template to the div container 
        if (!this.isSSR || target.wallContainer.find('.fpls-wallgroup-content').length < 1) {
            target.wallContainer.append($ls(this.wallContainerTpl(target)));
        }
        //Initialize the wall inside the tab
        target.htmlLoaded = true;
        this.loadWall(target, function(wall) {
            self.showTab(wall, params);
        });
    }
}

/**
 * Shows the current tab and hide the others
 * @param {Function} wall: wall instance
 * @param {Obhect} params: additional params
 */
LSWallgroup_default.prototype.showTab = function(wall, params) {
    LSHelpers.debug('Wallgroup showTab');
    var self = this;
    // Hide other walls
    for (var i in this.walls) {
        if (this.walls[i].wallContainer) this.walls[i].wallContainer.hide();
    }
    // Show current wall
    wall.wallContainer.show();
    self.setTabState(wall.wall_id);
}

/**
 * Load wall JS file
 * @param {Object} wall: wall JSON
 * @param {Function} callback: callback function
 */
LSWallgroup_default.prototype.loadWall = function(wall, callback) {
    LSHelpers.debug('Wallgroup loadWall');
    var self = this;
    var parent_theme = LSHelpers.getDefaultTheme(wall.theme);
    if (parent_theme) {
        LSHelpers.appendJs(LSConfig.assetURL + '/js/' + parent_theme + '.js', function() {
            LSHelpers.appendJs(LSHelpers.getAssetURL(wall.theme) + '/js/' + wall.theme + '.js', function() {
                setTimeout(function() {
                    self.initWall(wall, callback);
                }, 1);
            });
        });
    } else {
        LSHelpers.appendJs(LSHelpers.getAssetURL(wall.theme) + '/js/' + wall.theme + '.js', function() {
            setTimeout(function() {
                self.initWall(wall, callback);
            }, 1);
        });
    }
}

/**
 * Push tab switch to browser history
 * @param {String} fragment: Tab hash fragment
 */
LSWallgroup_default.prototype.pushTab = function(fragment) {
    LSHelpers.debug('Wallgroup pushTab');
    newState = window.location.href.split('#')[0] + '#' + fragment;
    window.history.pushState({}, document.title + ' | ' + fragment, newState);
}

/**
 * Initialize Wall Class
 * @param {Object} wall: wall JSON
 * @param {Function} callback: callback function
 */
LSWallgroup_default.prototype.initWall = function(wall, callback) {
    LSHelpers.debug('Wallgroup initWall');
    var self = this;

    //pass watermark to walls
    if (!wall.ls_watermark) wall.ls_watermark = self.wallgroup.ls_watermark;

    if (!wall.wallContainer) return;

    //Find correct wall wrapper
    var wrapper = wall.wallContainer.find('.fpls-wallgroup-content').eq(0);

    if (self.isSSR) {
        var ssrWrapper = wall.wallContainer.find('[data-ssr-version="3"]');
        if (ssrWrapper.length) {
            //in case of SSR, actually identify the correct container according to media query
            var matchWidth = self.mainContainer.width();
            LSHelpers.debug('MATCH WIDTH', matchWidth);
            var found = ssrWrapper.find(' > [ssr-breakpoint]').filter(function() {
                var min = $ls(this).attr('data-min');
                var max = $ls(this).attr('data-max');

                LSHelpers.debug('MIN', min);
                LSHelpers.debug('MAX', max);

                if (min && max) {
                    return matchWidth >= min && matchWidth <= max;
                } else if (min) {
                    return matchWidth >= min;
                } else if (max) {
                    return matchWidth <= max;
                }
            });

            //found actual media query, assign to wrapper
            if (found) wrapper = found.find('.fpls-wallgroup-content').eq(0);
        }
    }

    //add theme class to wall DIV
    if (wall.theme.indexOf('@') > 0) {
        wrapper.addClass('fpls-' + wall.theme.replace(/@/g, '')); //legacy
        wrapper.addClass('fpls-' + wall.theme.replace(/@/g, '-'));
    } else {
        wrapper.addClass('fpls-' + wall.theme);
    }

    //add parent theme class to wall DIV
    var parent_theme = LSHelpers.getDefaultTheme(wall.theme);
    if (parent_theme) wrapper.addClass('fpls-' + parent_theme);

    // Instantiate the correct LSWall class based on template
    var className = wall.theme.replace(/-/g, '_').replace(/@/g, '_').replace('wall_', 'LSWall_');
    wall.wallInstance = new window[className]();

    // Inject wallgroup in wall
    wall.wallInstance.wallgroup = this.wallgroup;

    // Init wall instance
    wall.wallInstance.init(wrapper, this.options, wall, self.mainContainer, self.rootContainer);

    //add credits to container if not present
    if (!self.wallgroup.ls_watermark.hide_logo_on_layout) {
        self.addCredits(self.wallgroup.ls_watermark.disable_logo_link_on_layout);
    }

    callback && callback(wall);
}

/**
 * Method to set css class of current tab given the active wall ID
 * @param {String} wall_id: id of the current wall
 */
LSWallgroup_default.prototype.setTabState = function(wall_id) {
    if (!this.tabContainer) return;
    var tabs = this.tabContainer.find('.fpls-wallgroup-tab a');
    tabs.removeClass('current');
    tabs.filter('[data-target="' + wall_id + '"]').addClass('current');
}

/**
 * Method to send wallgroup impression event
 */
LSWallgroup_default.prototype.sendAnalytics = function() {
    var self = this;

    //ANALYTICS: wallgroup-impression
    var payload = {
        event_type: 'wallgroup-impression',
        brand: self.wallgroup.brand,
        wallgroup_id: self.wallgroup._id,
        wallgroup_name: self.wallgroup.title
    }

    if (self.wallgroup.ab_experience_id) payload.ab_experience_id = self.wallgroup.ab_experience_id;

    livestory_analytics.pushEvent(payload, {
        wallgroup: self.wallgroup
    });
}

/**
 * Method to add Live Story logo
 * @param {boolean} disable_link: if true, link to watermark is disabled
 */
 LSWallgroup_default.prototype.addCredits = function(disable_link) {
    var self = this;
    //do not append credits if SSR
    if (typeof window.callPhantom == 'undefined') return;
    
    //append to wall
    if (self.mainContainer.find('.' + livestory_analytics.getCreditsId('wall')).length < 1) {
        if (disable_link) {
            self.mainContainer.append('<span class="' + livestory_analytics.getCreditsId('wall') + '" style="display: block !important;"></span>');
        } else {
            self.mainContainer.append('<a href="https://www.livestory.nyc" target="_blank" class="' + livestory_analytics.getCreditsId('wall') + '" style="display: block !important;"></a>');
        }
    }
}