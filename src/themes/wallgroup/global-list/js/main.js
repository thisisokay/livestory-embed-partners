var LSWallgroup_global_list = function() {
    
}
LSHelpers.extend(LSWallgroup_global_list, LSWallgroup_default);

LSWallgroup_global_list.prototype.initTabs = function() {
    var self = this;
    for (var key in self.walls) {
    	if (self.walls.hasOwnProperty(key)) {
	    	wall = self.walls[key];	
				if (!this.isSSR || wall.wallContainer.find('.fpls-wallgroup-content').length < 1) {
					wall.wallContainer.append($ls(self.wallContainerTpl(wall)));
					// load livestory wall inside the container
				}
				self.loadWall(wall, function(wall) {
					wall.wallContainer.show();
				});
			}
    }
}