var LSWallgroup_global_countdown = function() {
    
}
LSHelpers.extend(LSWallgroup_global_countdown, LSWallgroup_default);

LSWallgroup_global_countdown.prototype.initTabs = function() {
  var self = this;
  for (var key in self.walls) {
    if (self.walls.hasOwnProperty(key)) {
    wall = self.walls[key];
    wall.wallContainer.append($ls(self.wallContainerTpl(wall)));
        // load livestory wall inside the container
        self.loadWall(wall, function(wall) {
            wall.wallContainer.show();
        });
    }
  }

  self.refreshTimer();

  self.mainContainer.off('ls-countdown-end').on('ls-countdown-end', function() {
    setTimeout(function() {
      self.refreshWallgroup();
    }, 1000);
  });
}

LSWallgroup_global_countdown.prototype.refreshWallgroup = function() {
  var self = this;

  var url = self.getUrl();
  $ls.ajaxSetup({ cache: true });
  $ls.getJSON(url, function(data) {
    var should_refresh = false;

    should_refresh = should_refresh || _walls_changed(data);
    should_refresh = should_refresh || _schedules_changed(data);
    
    if (should_refresh) {
      data.wallgroup_id = self.wallgroup._id; //inject wallgorup id, since not in API response
      self.mainContainer.find('.fpls-wallgroup-container').remove();
      self.walls = {};
      self.tabs = {};
      self.parseJSON(data);
    }
  });

  function _walls_changed(data) {
    var current_ids = Object.keys(self.walls).join(',');
    var new_ids = data.walls.map(function(wall) { return wall.wall_id }).join(',');
    return (current_ids != new_ids);
  }

  function _schedules_changed(data) {
    var current_schedules = Object.keys(self.walls).map(function(wall_id) { 
      var wall = self.walls[wall_id];
      return wall.schedule && wall.schedule.schedule_end_date 
    }).join(',');
    var new_schedules = data.walls.map(function(wall) { return wall.schedule && wall.schedule.schedule_end_date }).join(',');
    return (current_schedules != new_schedules);
  }
}

LSWallgroup_global_countdown.prototype.refreshTimer = function() {
  var self = this;
  clearTimeout(self.__refreshTimer);
  self.__refreshTimer = setTimeout(function() {
    self.refreshWallgroup();
    self.refreshTimer();
  }, 1000 * 5);
}
