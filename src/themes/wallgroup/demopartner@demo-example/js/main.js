var LSWallgroup_demopartner_demo_example_2 = function() {
    this.wallContainerTpl = LSTemplates.wallgroup_demopartner_demo_example_2.tab;
}
LSHelpers.extend(LSWallgroup_demopartner_demo_example_2, LSWallgroup_default);

LSWallgroup_demopartner_demo_example_2.prototype.initTabs = function() {
    var self = this;
    for (var key in self.walls) {
    	if (self.walls.hasOwnProperty(key)) {
	    	wall = self.walls[key];
            if (!this.isSSR || wall.wallContainer.find('.fpls-wallgroup-content').length < 1) {
                wall.wallContainer.append($ls(self.wallContainerTpl(wall)));
                // load livestory wall inside the container
            }
            self.loadWall(wall, function(wall) {
                wall.wallContainer.show();
            });
		}
    }

    console.log('Wallgroup example');

    // Added custom title to walls
    var labelTitle = self.wallgroup.uiLabels && self.wallgroup.uiLabels.wallgroupTitle || 'Title not found';

	self.mainContainer.find('.fpls-wall-container-title').html('<h3>' + labelTitle + '</h3>');

}